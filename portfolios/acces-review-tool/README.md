# Access Review Tool (ART)

This blueprint creates an ec2 instance to deploy ART.

https://bitbucket.org/ua_sia/accessreviewtool



## Parameters

* 
## Outputs

* 


## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
