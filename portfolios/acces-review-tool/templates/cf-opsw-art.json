{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "Application Review Tool OpsWorks Stack",
  "Metadata" : {
    "AWS::CloudFormation::Interface" : {
      "ParameterGroups" : [
        {
          "Label" : {"default": "Application Information"},
          "Parameters" : [
            "EnvAppName",
            "AppSlug",
            "EnvSlug"
          ]
        },
        {
          "Label" : {"default": "Load Balancer Settings"},
          "Parameters" : [
            "SSLCertName",
            "LBSubnetA",
            "LBSubnetB"
          ]
        },

        {
          "Label" : {"default": "Instance Settings"},
          "Parameters" : [
            "InstanceType",
            "KeyName",
            "VPCID",
            "AppSubnetA",
            "AppSubnetB",
            "CustomCookbooksBucket",
            "CustomCookbooksSource",
            "DockerRepoAccessKey",
            "DockerRepoSecretKey"
          ]
        },
        {
          "Label" : {"default": "Application Settings"},
          "Parameters" : [
            "AppDockerImage",
            "AppDockerHostname",
            "AppDBUser",
            "AppDBPass",
            "AppHRUser",
            "AppHRPass",
            "AppEDSUser",
            "AppEDSPass",
            "AppAccessKey",
            "AppSecretKey"
          ]
        },
        {
          "Label" : {"default": "Tags"},
          "Parameters" : [
            "TagOwner",
            "TagNetid",
            "TagProjectName"
          ]
        }
      ],
      "ParameterLabels" : {
        "EnvAppName":             {"default": "Application Name:"},
        "AppSlug":                {"default": "Application Slug:"},
        "EnvSlug":                {"default": "Environment Slug:"},
        "LBSubnetA":              {"default": "Load Balancer Subnet A:"},
        "LBSubnetB":              {"default": "Load Balancer Subnet B:"},
        "AppSubnetA":             {"default": "App Instance Subnet A:"},
        "AppSubnetB":             {"default": "App Instance Subnet B:"},

        "DockerRepoAccessKey":    {"default": "Docker Repo Access Key ID:"},
        "DockerRepoSecretKey":    {"default": "Docker Repo Secret Access Key:"},

        "AppDockerImage":         {"default": "Docker Image"},
        "AppDockerHostname":      {"default": "Docker Internal Hostname"},
        "AppDBUser":              {"default": "DB Username"},
        "AppDBPass":              {"default": "DB Password"},
        "AppHRUser":		  {"default": "HR IB Username"},
	"AppHRPass":		  {"default": "HR IB Pass"},
        "AppAccessKey":           {"default": "AWS Access Key"},
        "AppSecretKey":           {"default": "AWS Secret Key"},
        "AppEDSUser":             {"default": "EDS Username"},
        "AppEDSPass":             {"default": "EDS Password"}

      }
    }
  },
  "Parameters": {
    "EnvAppName": {
      "MinLength": "3",
      "Type": "String",
      "Description": "Full Application name, ie 'Kuali Financials'"
    },
    "EnvSlug": {
      "MinLength": "2",
      "Type": "String",
      "Description": "Short environment slug, ie 'dev'. Lowercase letters, numbers and dashes only",
      "AllowedPattern" : "[a-z0-9]*"
    },
    "AppSlug": {
      "MinLength": "3",
      "Type": "String",
      "Description": "Short application slug, ie 'kfs'. Lowercase letters, numbers and dashes only",
      "AllowedPattern" : "[a-z0-9-]*"
    },
    

    "VPCID" : {
      "Description" : "Target VPC",
      "Type" : "AWS::EC2::VPC::Id"
    },
    "AppSubnetA" : {
      "Description" : "Application Subnet for Zone A",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    "AppSubnetB" : {
      "Description" : "Application Subnet for Zone B",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    "SSLCertName" : {
      "Description" : "SSL Certificate Name as uploaded",
      "Type" : "String"
    },
    "LBSubnetA" : {
      "Description" : "Load Balancer Subnet for Zone A",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    "LBSubnetB" : {
      "Description" : "Load Balancer Subnet for Zone B",
      "Type" : "AWS::EC2::Subnet::Id"
    },
    "KeyName" : {
      "Description" : "Amazon EC2 Key Pair",
      "Type" : "AWS::EC2::KeyPair::KeyName"
    },
    "CustomCookbooksSource": {
      "Description" : "URL to S3 cookbooks, ie 'https://s3.amazonaws.com/edu-arizona-pilots-eas/cookbooks.tar.gz'",
      "Default": "https://s3-us-west-2.amazonaws.com/edu-arizona-pilots-eas/sia-art/docker-cookbooks.tar.gz",
      "Type": "String"
    },
    "CustomCookbooksBucket" : {
      "Description" : "S3 Bucket containing the custom cookbooks. Just the bucket name, not a full arn.",
      "Type" : "String"
    },
    "InstanceType" : {
      "Description" : "EC2 Instance Type",
      "Type" : "String",
      "Default" : "t2.micro",
      "AllowedValues" : ["t2.micro", "m3.small", "m3.medium"]
    },
    "DockerRepoAccessKey" : {
      "Description" : "AWS Access Key for permissions to the Docker Repository that holds the image.",
      "Type" : "String"
    },
    "DockerRepoSecretKey" : {
      "Description" : "AWS Secret Key for permissions to the Docker Repository that holds the image.",
      "Type" : "String",
      "NoEcho" : "true"
    },

    "AppDockerImage" : {
      "Description" : "Docker Image, i.e.: easksd/kfs6:dev",
      "Type" : "String"
    },
    "AppDockerHostname" : {
      "Description" : "Docker Internal Hostname, i.e.: app.arizona.edu",
      "Type" : "String"
    },
    "AppDBUser" : {
      "Description" : "Database Username",
      "Type" : "String"
    },
    "AppDBPass" : {
      "Description" : "Database Password",
      "Type" : "String",
      "NoEcho" : "true"
    },
    "AppHRUser" : {
      "Description" : "HR IB Username",
      "Type" : "String"
    },
    "AppHRPass" : {
      "Description" : "HR IB Password",
      "Type" : "String",
      "NoEcho" : "true"
    },
    "AppEDSUser" : {
      "Description" : "EDS Username",
      "Type" : "String"
    },
    "AppEDSPass" : {
      "Description" : "EDS Password",
      "Type" : "String",
      "NoEcho" : "true"
    },
    "AppSecretKey" : {
      "Description" : "AWS Secret Key for s3 files",
      "Type" : "String",
      "NoEcho" : "true"
    },
    "AppAccessKey" : {
      "Description" : "AWS Access Key for s3 files",
      "Type" : "String",
      "NoEcho" : "true"
    },
   
    "TagOwner" : {
      "Description" : "Name of the Owner of this resource (Full Name)",
      "Type" : "String"
    },
    "TagNetid" : {
      "Description" : "UA NetID of responsible person",
      "Type" : "String"
    },
    "TagProjectName" : {
      "Description" : "Name of the Project this is for, or JIRA ticket for more info",
      "Type" : "String"
    }
    
  },
  "Mappings" : {
    "AccountTemplateBucketMap" : {
      "998687558142": { "DefaultZone": "https://s3.amazonaws.com/cloudformation-templates.aws-pilots.arizona.edu" },
      "760232551367": { "DefaultZone": "https://s3.amazonaws.com/cloudformation-templates.aws.arizona.edu" }
    }
  },
  "Resources": {
    "EnvApplicationLayer": {
      "Type": "AWS::OpsWorks::Layer",
      "Properties": {
        "Name": { "Ref": "EnvAppName" },
        "Shortname": { "Fn::Join": [ "-", [ { "Ref": "AppSlug" }, { "Ref": "EnvSlug" }, "instance-" ] ] },
        "Type": "custom",
        "AutoAssignElasticIps": "false",
        "AutoAssignPublicIps": "false",
        "CustomRecipes": {
          "Setup":      [ "ecs-docker::docker_install" ],
          "Deploy":     [ "ecs-docker::docker_login_ecr", "ecs-docker::docker_pull_deploy" ]
        },
        "CustomSecurityGroupIds": [ { "Ref": "InstanceSecurityGroup" } ],
        "EnableAutoHealing": "false",
        "StackId": { "Ref": "EnvStack" }
      }
    },
    "EnvApplicationLayerLoadBalancerAttachment": {
      "Type": "AWS::OpsWorks::ElasticLoadBalancerAttachment",
      "Properties": {
        "ElasticLoadBalancerName": { "Ref": "EnvApplicationLayerLoadBalancer" },
        "LayerId": { "Ref": "EnvApplicationLayer" }
      }
    },
    "ArtApp": {
      "Type": "AWS::OpsWorks::App",
      "Properties": {
        "AppSource": { "Type": "other" },
        "Name": { "Ref": "EnvAppName" },
        "Shortname": { "Ref": "AppSlug" },
        "StackId": { "Ref": "EnvStack" },
        "Type": "other",
        "Environment" : [
            { "Key": "layer",             "Value": { "Ref": "EnvAppName" } },
            { "Key": "docker_repository_access_key", "Value": { "Ref": "DockerRepoAccessKey" } },
            { "Key": "docker_repository_secret_key", "Secure": true, "Value": { "Ref": "DockerRepoSecretKey" } },
            { "Key": "docker_container",  "Value": { "Ref": "AppDockerImage" } },
            { "Key": "docker_ports",      "Value": "0.0.0.0:80:80" },
            { "Key": "docker_run_cmd",    "Value": "/usr/local/bin/startup.sh" },
            { "Key": "docker_hostname",   "Value": { "Ref": "AppDockerHostname" } },
            { "Key": "PHP_dbUser",        "Value": { "Ref": "AppDBUser" } },
            { "Key": "PHP_dbPass",        "Secure": true, "Value": { "Ref": "AppDBPass" } },
	    { "Key": "PHP_HRIBUser",      "Value": { "Ref": "AppHRUser" } },
            { "Key": "PHP_HRIBPass",      "Secure": true, "Value": { "Ref": "AppHRPass" } },
            { "Key": "PHP_edsUser",       "Value": { "Ref": "AppEDSUser" } },
            { "Key": "PHP_edsPass",       "Secure": true, "Value": { "Ref": "AppEDSPass" } },
            { "Key": "PHP_awsAccessKey",  "Secure": true, "Value": { "Ref": "AppAccessKey" } },
            { "Key": "PHP_awsSecretKey",  "Secure": true, "Value": { "Ref": "AppSecretKey" } }
        ]
      }
    },
    "EnvStack": {
      "Type": "AWS::OpsWorks::Stack",
      "Properties": {
        "Name": { "Fn::Join": [" ", [{ "Ref": "EnvAppName" }, { "Ref": "EnvSlug" }] ] },
        "ConfigurationManager": {
          "Name": "Chef",
          "Version": "12"
        },
        "CustomCookbooksSource": {
          "Type": "s3",
          "Url": { "Ref": "CustomCookbooksSource" }
        },
        "ServiceRoleArn": {"Fn::GetAtt" : ["EnvServiceRole", "Arn"] },
        "DefaultInstanceProfileArn": {"Fn::GetAtt" : ["EnvInstanceProfile", "Arn"] },
        "DefaultOs": "Amazon Linux 2016.03",
        "DefaultSshKeyName": { "Ref": "KeyName" },
        "DefaultRootDeviceType": "ebs",
        "DefaultSubnetId": { "Ref": "AppSubnetA" },
        "HostnameTheme": "Layer_Dependent",
        "UseCustomCookbooks": "true",
        "VpcId": { "Ref": "VPCID" }
      }
    },
    "EnvApplicationLayerLoadBalancer": {
      "Type": "AWS::ElasticLoadBalancing::LoadBalancer",
      "Properties": {
        "Scheme": "internet-facing",
        "ConnectionSettings": { "IdleTimeout": "3600" },
        "HealthCheck": {
          "HealthyThreshold": "3",
          "Interval": "60",
          "Target": "TCP:80",
          "Timeout": "10",
          "UnhealthyThreshold": "3"
        },
        "LBCookieStickinessPolicy": [
          {
            "CookieExpirationPeriod": "28800",
            "PolicyName": "DefaultSessionTimeout"
          }
        ],
        "Listeners": [
          {
            "LoadBalancerPort": "80",
            "Protocol": "HTTP",
            "InstancePort": "80",
            "InstanceProtocol": "HTTP",
            "PolicyNames": [ "DefaultSessionTimeout" ]
          },
          {
            "LoadBalancerPort": "443",
            "Protocol": "HTTPS",
            "InstancePort": "80",
            "InstanceProtocol": "HTTP",
            "PolicyNames": [ "DefaultSessionTimeout" ],
            "SSLCertificateId": {
              "Fn::Join": [ "", 
                [ "arn:aws:iam::", { "Ref": "AWS::AccountId" }, ":server-certificate/", { "Ref": "SSLCertName" } ] 
              ]
            }
          }
        ],
        "LoadBalancerName": { 
          "Fn::Join": [ "-", [ { "Ref": "AppSlug" }, { "Ref": "EnvSlug" }, "app-lb" ] ]
        },
        "SecurityGroups": [ { "Ref": "LoadBalancerSecurityGroup" } ],
        "Subnets": [
          { "Ref": "LBSubnetA" },
          { "Ref": "LBSubnetB" }
        ],
        "Tags" :  [
              { "Key": "Name",
                "Value": { "Fn::Join": [ "-", 
                        [{ "Ref": "AppSlug" }, { "Ref": "EnvSlug" }, "-lb" ] ] } },
              { "Key": "owner", "Value": { "Ref": "TagOwner" } },
              { "Key": "netid", "Value": { "Ref": "TagNetid" } },
              { "Key": "projectname", "Value": { "Ref": "TagProjectName" } }
               ]
      }
    },
    "InstanceSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "Allow Load Balancer and SSH to client host",
        "VpcId" : {"Ref" : "VPCID"},
        "SecurityGroupIngress" : [
            {
              "IpProtocol" : "tcp",
              "FromPort" : "22",
              "ToPort" : "22",
              "CidrIp" : "0.0.0.0/0"
            },
            {
              "IpProtocol" : "tcp",
              "FromPort" : "80",
              "ToPort" : "80",
              "SourceSecurityGroupId" :  {"Ref" : "LoadBalancerSecurityGroup"}
            }
            ],
        "Tags" :  [
              { "Key": "Name",
                "Value": { "Fn::Join": [ "-", 
                        [{ "Ref": "AppSlug" }, { "Ref": "EnvSlug" }, "instance-sg" ] ] } },
              { "Key": "owner", "Value": { "Ref": "TagOwner" } },
              { "Key": "netid", "Value": { "Ref": "TagNetid" } },
              { "Key": "projectname", "Value": { "Ref": "TagProjectName" } }
               ]
      }
    },
    "LoadBalancerSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "Allow web traffic to the Load Balancer",
        "VpcId" : {"Ref" : "VPCID"},
        "SecurityGroupIngress" : [
            { "IpProtocol" : "tcp", "FromPort" : "80", "ToPort" : "80", "CidrIp" : "0.0.0.0/0" },
            { "IpProtocol" : "tcp", "FromPort" : "443", "ToPort" : "443", "CidrIp" : "0.0.0.0/0" }
            ],
        "Tags" :  [ 
              { "Key": "Name",
                "Value": { "Fn::Join": [ "-", 
                        [{ "Ref": "AppSlug" }, { "Ref": "EnvSlug" }, "load-balancer-sg" ] ] } },
              { "Key": "owner", "Value": { "Ref": "TagOwner" } },
              { "Key": "netid", "Value": { "Ref": "TagNetid" } },
              { "Key": "projectname", "Value": { "Ref": "TagProjectName" } }
              ]
      }
    },
    "EnvServiceRole": {
       "Type": "AWS::IAM::Role",
       "Properties": {
          "AssumeRolePolicyDocument": {
             "Version" : "2012-10-17",
             "Statement": [ {
                "Effect": "Allow",
                "Principal": {
                  "Service": [ "opsworks.amazonaws.com" ]
                },
                "Action": [ "sts:AssumeRole" ]
             } ]
          },
          "Path": "/",
          "Policies": [ 
            {
              "PolicyName": "opsworks-service",
              "PolicyDocument": {
                "Statement": [
                {
                    "Action": [
                        "ec2:*",
                        "iam:PassRole",
                        "cloudwatch:GetMetricStatistics",
                        "elasticloadbalancing:*",
                        "rds:*"
                    ],
                    "Effect": "Allow",
                    "Resource": [
                        "*"
                    ]
                }
                ]
              }
            }
          ]
       }
    },
    "EnvInstanceRole": {
       "Type": "AWS::IAM::Role",
       "Properties": {
          "AssumeRolePolicyDocument": {
             "Version" : "2012-10-17",
             "Statement": [ {
                "Effect": "Allow",
                "Principal": {
                   "Service": [ "ec2.amazonaws.com" ]
                },
                "Action": [ "sts:AssumeRole" ]
             } ]
          },
          "Path": "/",
          "Policies": [ ]
       }
    },
    "EnvInstanceProfile": {
       "Type": "AWS::IAM::InstanceProfile",
       "Properties": {
          "Path": "/",
          "Roles": [ { "Ref": "EnvInstanceRole"} ]
       }
    },
    "EnvInstanceS3Policy" : {
       "Type": "AWS::IAM::Policy",
       "Properties": {
          "PolicyName": { "Fn::Join": [ "-", [ { "Ref": "AppSlug" }, { "Ref": "EnvSlug" }, "instancepolicy" ] ] },
          "PolicyDocument": {
             "Version" : "2012-10-17",
             "Statement": [
                {
                    "Sid": "Stmt1452033379000",
                    "Effect": "Allow",
                    "Action": [
                        "s3:GetObject",
                        "s3:ListBucket"
                    ],
                    "Resource": [
                        { "Fn::Join": [ "", [ "arn:aws:s3:::", { "Ref": "CustomCookbooksBucket" }, "/*" ] ] }
                    ]
                },
                {
                    "Sid": "Stmt1458096595000",
                    "Effect": "Allow",
                    "Action": [
                        "ecr:GetAuthorizationToken",
                        "ecr:GetDownloadUrlForLayer",
                        "ecr:GetManifest",
                        "ecr:GetRepositoryPolicy",
                        "ecr:ListImages"
                    ],
                    "Resource": [
                        "*"
                    ]
                }
             ]
          },
          "Roles": [ { "Ref": "EnvInstanceRole"} ]
       }
    }

  }
}
