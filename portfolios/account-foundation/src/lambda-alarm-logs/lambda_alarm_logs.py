#
# This program should be called from an SNS topic which gets events from CloudWatch
# alarms. This program looks up additional log information about what caused the
# alarm to fire and adds those descriptions to a new SNS topic.
#
#

import os
import subprocess
import datetime
import dateutil.tz
import dateutil.parser
import logging
import json
import boto3

cwl = boto3.client('logs')
sns = boto3.client('sns')

# Convert a datetime object into milleseconds from the unix epoch
def unix_time_millis(dt):
    epoch = datetime.datetime.fromtimestamp(0, dateutil.tz.gettz('UTC') )
    return int( (dt - epoch).total_seconds() * 1000.0 )

def lambda_handler(event, context):
    rootlogger = logging.getLogger()
    rootlogger.setLevel(logging.INFO)
    logging.info("-" * 100)
    logging.info(json.dumps(event, indent=2))
    
    if "Records" in event:
        event = event["Records"][0]

    # Unpack the message data from this event
    messageText = event["Sns"]["Message"]
    message = json.loads(messageText)
    
    # Get the timestamp of this event
    stateChangeTime = message["StateChangeTime"]
    dt = dateutil.parser.parse(stateChangeTime)
    timestamp = unix_time_millis(dt)

    # How far back in time does this event cover
    period = int( message["Trigger"]["Period"] )
    count = int( message["Trigger"]["EvaluationPeriods"] )
    # Go back more than a single period just to make sure we get some events
    offset = int( period * count * 3 * 1000 )

    # Get the filter for this metric
    filterData = cwl.describe_metric_filters(
        metricName = message["Trigger"]["MetricName"],
        metricNamespace = message["Trigger"]["Namespace"]
    )
    metricFilter = filterData["metricFilters"][0]

    logGroupName = metricFilter["logGroupName"]
    filterPattern = metricFilter["filterPattern"]
    
    # Get log events within the time range
    logEventData = cwl.filter_log_events(
        logGroupName = logGroupName,
        filterPattern = filterPattern,
        startTime = timestamp - offset,
        endTime = timestamp
    )
    
    # Get log messages from found events
    eventMessages = ""
    for e in logEventData["events"]:
        eventMessage = json.loads(e["message"])
        eventTime = datetime.datetime.fromtimestamp(e['timestamp']/1000, dateutil.tz.gettz('UTC') )
        eventMessages += str(eventTime) + ": "
        if "errorMessage" in eventMessage:
            # when the errorMessage is less than 50 characters in length, assume it's generic and provide additional attributes for context
            if len(eventMessage["errorMessage"]) <= 50:
                errorMessage = eventMessage["errorMessage"]
                arn = eventMessage["userIdentity"]["arn"]
                eventSource = eventMessage["eventSource"]
                eventName = eventMessage["eventName"]
                eventMessages += f"User: {arn} received error: \"{errorMessage}\" attempting action: {eventName} on: {eventSource}\n"

                if "requestParameters" in eventMessage:
                    requestParams = json.dumps(eventMessage["requestParameters"], indent=2)
                    if not requestParams == "":
                        eventMessages += f"{requestParams}\n"
                
                if "additionalEventData" in eventMessage:
                    additionalEventData = json.dumps(eventMessage["additionalEventData"], indent=2)
                    eventMessages += f"{additionalEventData}\n"
                
                eventMessages += "\n"

            else:
                eventMessages += eventMessage["errorMessage"] + "\n\n"

        else:
            arn = eventMessage["userIdentity"]["arn"]
            eventName = eventMessage["eventName"]
            eventMessages += f"{arn} {eventName}\n"
            if "requestParameters" in eventMessage:
                requestParams = json.dumps(eventMessage["requestParameters"], indent=2)
                eventMessages += f"{requestParams}\n"
                
            if "additionalEventData" in eventMessage:
                additionalEventData = json.dumps(eventMessage["additionalEventData"], indent=2)
                eventMessages += f"{additionalEventData}\n"

            eventMessages += "\n"
    
    # logging.info( json.dumps(eventMessage, indent=2))
    logging.info(eventMessages)

    # Send message to security topic
    subject = event["Sns"]["Subject"]
    alertMessageText  = f"You are receiving this email because your Amazon CloudWatch Alarm '{subject}' region has entered the ALARM state.\n\n"
    alertMessageText += eventMessages
    
    # The SNS Topic ARN must be passe in as a Lambda Environment variable. Set in the CloudWatch template.
    topicArn = os.environ['AlertSNSTopic']
    
    sns.publish(
        TopicArn = topicArn,
        Subject = subject,
        Message = alertMessageText
    )

    