"""
Find AWS AMI IDs 

This program takes a list of AMI names and looks up the AMI ID for each one
in each reagion. The results are compile into a data structure and output
as JSON.


"""
import json
import urllib
import boto3
import logging
import re
import time
import datetime
from dateutil import parser
from botocore.vendored import requests

SUCCESS = "SUCCESS"
FAILED = "FAILED"

logger = logging.getLogger()
logger.setLevel(logging.INFO)

ami_names = {
  "amazon-linux":   "amzn-ami-hvm-*-x86_64-gp2",
  "ecs-optimized":  "amzn-ami-*-amazon-ecs-optimized",
  "windows-2016":   "Windows_Server-2016-English-Full-Base-*",
  "windows-2012":   "Windows_Server-2012-R2_RTM-English-64Bit-Base-*"
}

def handler(type, region):

    # add 'profile_name' to Session constructor if running locally
    aws_session = boto3.Session(profile_name="erp")
    # aws_session = boto3.Session()
    ec2 = aws_session.client('ec2', region_name=region)
    
    ami_name = ami_names[type]
    image_data = ec2.describe_images(
                    Filters=[{
                      'Name': "name",
                      'Values': [ami_name]
                    }]
                  )

    # Loop through all the AMIs returned and find the newest one that isn't a release candidate
    newest_date = parser.parse("2000-01-01T00:00:00.000Z")
    newest_ami = {}
    for img in image_data["Images"]:
      # Additional filtering depending on image type
      if type == "amazon-linux":
        # make sure we skip any release candidates
        if ".rc" in img['Name']:
          logger.debug("Skipping " + img['Name'])
          continue
      
      if type == "windows-2012" or type == "windows-2016":
        # skip anything that doesn't end in a date. This pulls off the last 10 characters
        # which we want to look like "2017.02.01"
        tail = img['Name'][-10:]
        pat = r'[0-9]{4}\.[0-9]{2}\.[0-9]{2}'
        if not re.search(pat, tail):
          logger.debug("Skipping " + img['Name'])
          continue
      
      image_date = parser.parse(img["CreationDate"])
      if image_date > newest_date:
        newest_ami = img
        newest_date = image_date

    ami_id = newest_ami['ImageId']

    logger.debug('AMI: ' + json.dumps(ami_id, indent=2))
 
    responseData = {
        "id": ami_id
    }

    print(json.dumps(responseData, indent=2))


handler("amazon-linux", "us-west-2")