"""
Find AMI ID for a given type and region.

This lambda function is called as a CloudFormation custom resource in order 
to look up the current AMI ID for a type of EC2 instance in a given region.

Sample Event:


{
    "StackId": "arn:aws:cloudformation:us-west-2:722748364533:stack/fischerm-amifinder/ea893530-a2f4-11e7-b313-503a90a9c4d1",
    "ResponseURL": "http://pre-signed-S3-url-for-response",
    "ResourceProperties": {
        "Type": "amazon-linux",
        "Region": "us-west-2",
        "ServiceToken": "arn:aws:lambda:us-west-2:722748364533:function:fischerm-cfn-ami-ids"
    },
    "RequestType": "Delete",
    "ServiceToken": "arn:aws:lambda:us-west-2:722748364533:function:fischerm-cfn-ami-ids",
    "ResourceType": "Custom::AMILookup",
    "PhysicalResourceId": "arn:aws:uits.arizona.edu:fischerm:amiid",
    "RequestId": "70198b64-eb95-4626-954b-8bfa9557d4dd",
    "LogicalResourceId": "AMILookup"
}

"""
import json
import urllib
import boto3
import logging
import re
import time
import datetime
from dateutil import parser
from botocore.vendored import requests

SUCCESS = "SUCCESS"
FAILED = "FAILED"

logger = logging.getLogger()
logger.setLevel(logging.INFO)

ami_names = {
  "amazon-linux":   "amzn-ami-hvm-*-x86_64-gp2",
  "ecs-optimized":  "amzn-ami-*-amazon-ecs-optimized",
  "windows-2016":   "Windows_Server-2016-English-Full-Base-*",
  "windows-2012":   "Windows_Server-2012-R2_RTM-English-64Bit-Base-*"
}

def handler(event, context):
    logger.debug('Received event: ' + json.dumps(event, indent=2))
    
    props = event['ResourceProperties']
    
    region = props['Region']
    type = props['Type']

    # add 'profile_name' to Session constructor if running locally
    # aws_session = boto3.Session(profile_name="erp")
    aws_session = boto3.Session()
    ec2 = aws_session.client('ec2', region_name=region)
    
    ami_name = ami_names[type]
    image_data = ec2.describe_images(
                    Filters=[{
                      'Name': "name",
                      'Values': [ami_name]
                    }]
                  )

    # Loop through all the AMIs returned and find the newest one that isn't a release candidate
    newest_date = parser.parse("2000-01-01T00:00:00.000Z")
    newest_ami = {}
    for img in image_data["Images"]:
      # Additional filtering depending on image type
      if type == "amazon-linux":
        # make sure we skip any release candidates
        if ".rc" in img['Name']:
          logger.debug("Skipping " + img['Name'])
          continue
      
      if type == "windows-2012" or type == "windows-2016":
        # skip anything that doesn't end in a date. This pulls off the last 10 characters
        # which we want to look like "2017.02.01"
        tail = img['Name'][-10:]
        pat = r'[0-9]{4}\.[0-9]{2}\.[0-9]{2}'
        if not re.search(pat, tail):
          logger.debug("Skipping " + img['Name'])
          continue
      
      image_date = parser.parse(img["CreationDate"])
      if image_date > newest_date:
        newest_ami = img
        newest_date = image_date

    ami_id = newest_ami['ImageId']

    logger.debug('AMI: ' + json.dumps(ami_id, indent=2))
 
    responseData = {
        "id": ami_id
    }

    send(event, context, SUCCESS, responseData, "arn:aws:uits.arizona.edu:fischerm:amiid")


#  Copyright 2016 Amazon Web Services, Inc. or its affiliates. All Rights Reserved.
#  This file is licensed to you under the AWS Customer Agreement (the "License").
#  You may not use this file except in compliance with the License.
#  A copy of the License is located at http://aws.amazon.com/agreement/ .
#  This file is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or implied.
#  See the License for the specific language governing permissions and limitations under the License.

def send(event, context, responseStatus, responseData, physicalResourceId):
    responseUrl = event['ResponseURL']

    print(responseUrl)

    responseBody = {}
    responseBody['Status'] = responseStatus
    responseBody['Reason'] = 'See the details in CloudWatch Log Stream: ' + context.log_stream_name
    responseBody['PhysicalResourceId'] = physicalResourceId or context.log_stream_name
    responseBody['StackId'] = event['StackId']
    responseBody['RequestId'] = event['RequestId']
    responseBody['LogicalResourceId'] = event['LogicalResourceId']
    responseBody['Data'] = responseData

    json_responseBody = json.dumps(responseBody)
   
    print("Response body:\n" + json_responseBody)

    headers = {
        'content-type' : '', 
        'content-length' : str(len(json_responseBody))
    }
    
    try:
        response = requests.put(responseUrl,
                                data=json_responseBody,
                                headers=headers)
        print("Status code: " + response.reason)
    except Exception as e:
        print("send(..) failed executing requests.put(..): " + str(e))
