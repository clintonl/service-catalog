"""
UITS EC2 Route53 Entries

This script attempts to create a Route53 entry for new EC2 instances 
under certain circumstances.

This function should be invoked by a CloudWatch Event Rule.
  Event Source: ec2.amazonaws.com
  Event Name: RunInstances

"""
from __future__ import print_function
import json
import boto3
import logging
import time
import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)

aws_session = boto3.Session()

ec2 = aws_session.client('ec2', region_name='us-west-2')
opw = aws_session.client('opsworks', region_name='us-east-1')
r53 = aws_session.client('route53')

# create_route53_entry_for_instance
#
# Look at the tag data for the given instance, and figure out what DNS names
# need to be set up
def create_route53_entry_for_instance(instance):

    # Grab the EC2 Tags
    tagData = ec2.describe_tags(
      Filters=[
        {
          'Name': 'resource-id',
          'Values': [ instance['instanceId'] ]
        },
      ],
    )
    
    # Transform Amazon's crazy tag data structure into a simple map
    tags = {}
    for i, t in enumerate(tagData['Tags']):
      tags[t['Key']] = t['Value']
    
    # Check on some potential other tags for a prefix to use for this DNS record
    dnsPrefix = ""
    if 'opsworks:instance' in tags:
      dnsPrefix = tags['opsworks:instance'] + "-"
    
    dnsName = ""
    if 'privateroute53record' in tags:
      ipAddressType = 'private'
      dnsName = dnsPrefix + tags['privateroute53record']
      ip = get_ip_of_instance( 'privateIpAddress', instance )
      if ip is not None:
        update_route53_record(dnsName, ip)

    if 'publicroute53record' in tags:
      ipAddressType = 'private'
      dnsName = dnsPrefix + tags['publicroute53record']
      ip = get_ip_of_instance( 'publicIpAddress', instance )
      if ip is not None:
        update_route53_record(dnsName, ip)

    if dnsName == "":
      # If we didn't find a dnsName, then wait a bit and call ourselves to try again.
      # This will eventually hit the Lambda fn timeout, so set something reasonable, like 60s.
      logger.error("No route53record tags found, sleeping and trying again...")
      time.sleep(10)
      create_route53_entry_for_instance( instance )

    return


# update_route53_record
#
# Perform the actual API call to create a new Route53 Record Set.
def update_route53_record(name, ip):
    zone = get_hosted_zone_for_name(name)
    if zone == None:
      logger.error("Could not find hosted zone for {0}".format(name))
      return

    response = r53.change_resource_record_sets(
      HostedZoneId = zone['Id'],
      ChangeBatch = {
        'Changes': [
          {
            'Action': 'UPSERT',
            'ResourceRecordSet': {
              'Name': name,
              'Type': 'A',
              'TTL': 300,
              'ResourceRecords': [ { 'Value': ip } ]
            }
          },
        ]
      }
    )
    logger.info("Set {0} as A record to {1}".format(name, ip))


# get_hosted_zone_for_name
#
# Based on a full DNS string, devolve the name and find the best Route53 hosted zone
# that exists in this account.
def get_hosted_zone_for_name(dnsName):

    # Make sure the dnsName ends in a period
    if dnsName[-1:] != ".":
      dnsName += "."

    # We have a dnsName, deconstruct it to figure out if we have a Route53 Hosted Zone that matches
    # This will take a string like "foo.bar.arizona.edu." and turn it into an array of possible
    # zones to lookup like: ['foo.bar.arizona.edu.', 'bar.arizona.edu.', 'arizona.edu.', 'edu.']
    # We then start with the longest match and see if this account has a hosted zone that matches.
    nameAtoms = dnsName.split('.')
    zoneNames = []
    for i, v in enumerate(nameAtoms):
      zoneNames.append( ".".join(nameAtoms[i:]) )

    # Get all the Route53 Hosted Zones
    # Init a dictionary to hold hostedZone data
    hostedZoneResponse = r53.list_hosted_zones_by_name()
    hostedZones = {}
    for i, z in enumerate(hostedZoneResponse['HostedZones']):
      hostedZones[z['Name']] = z

    for i, n in enumerate(zoneNames):
      logger.info("Looking up {0}".format(n))
      if n in hostedZones:
        logger.info("Found {0}".format(n))
        return hostedZones[n]

    # If we don't find anything, return None
    return None


# get_ip_of_instance
#
# Look through the instance network interfaces data structure for an
# IP address of the given type (privateIpAddress or publicIpAddress)
def get_ip_of_instance( addressType, instance ):
    if "networkInterfaceSet" in instance:
      if "items" in instance["networkInterfaceSet"] and len(instance["networkInterfaceSet"]["items"]) > 0:
        for i, n in enumerate( instance["networkInterfaceSet"]["items"] ):
          logger.debug('Network Interface: ' + json.dumps(n, indent=2))
          if addressType in n:
            return n[addressType]
      
      else:
        logger.error("No network items found for instance.")
        return None
    
    else:
      logger.error("No network data found for instance.")
      return None
    
    logger.error("No interface found contiaining {0}".format(addressType))
    return None


# Main handler.
def lambda_handler(event, context):
    # logger.debug('Received event: ' + json.dumps(event, indent=2))
    
    # Get the EC2 Instance ID
    if ("responseElements" in event['detail'] 
      and "instancesSet"   in event['detail']['responseElements']
      and "items"          in event['detail']['responseElements']['instancesSet']
      and len(event['detail']['responseElements']['instancesSet']['items']) == 1):
        instanceData = event['detail']['responseElements']['instancesSet']['items'][0]
        # logger.debug(json.dumps(instanceData, indent=2))

    else:
      logger.error("Could not get instance data")
      logger.error('Received event: ' + json.dumps(event, indent=2))
      return
    
    create_route53_entry_for_instance( instanceData )
      
