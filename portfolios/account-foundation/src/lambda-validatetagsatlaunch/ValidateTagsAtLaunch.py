#load dependencies
import json
import boto3
import re
import time
ec2_client = boto3.client('ec2')
sns_client = boto3.client('sns')
cf_client = boto3.client('cloudformation')
dynamo_client = boto3.client('dynamodb')

# Verify required tags are present, and have valid values
def find_violation(current_tags, required_tags, instance_identifier):
    instance_summary = ''
    instance_detail = ''
    violation = ''
    problem_found = ''
    nametag = 'no Name tag specified'
    #create dict to store all present and required tags for instance
    tag_dict = {}
    for present_tag in current_tags['Tags']:
        tag_dict[present_tag['Key']]=present_tag['Value']
    for required_tag,required_values in required_tags.iteritems():
        required_tag = required_tag.encode('ascii','ignore')
        if not tag_dict.has_key(required_tag):
            tag_dict[required_tag]=''
    #look for required tags and valid values
    for rtag,rvalues in required_tags.iteritems():
        tag_present = False
        for tag in current_tags['Tags']:
            if tag['Key'] == 'Name' and tag['Value'] != '':
                nametag = tag['Value']
            if tag['Key'] == rtag:
                value_match = False
                tag_present = True
                rvaluesplit = rvalues.split(',')
                for rvalue in rvaluesplit:
                    if tag['Value'] == rvalue:
                        value_match = True
                    if tag['Value'] != '':
                        if rvalue == '*':
                            value_match = True
                #note invalid values
                if value_match == False:
                    tag_dict[tag['Key']]=' Tag ' + tag['Key'] + ' has an invalid value of ' + tag['Value'] + '. Valid value(s) include: ' + required_tags[rtag] + '.\n'
                    problem_found = 'yes'
        #note missing tags
        if tag_present != True:
            tag_dict[str(rtag)]='Missing required tag: ' + str(rtag) + '.\n'
            problem_found = 'yes'
    #exit if tags present and valid
    if problem_found == '':
        return None
    #send email if tags missing or invalid
    else:
        instance_summary = 'Instance ' + instance_identifier + ' (' + nametag + ') summary:\n ---------------- \n'
        instance_detail = 'Instance ' + instance_identifier + ' (' + nametag + ') available details:\n ---------------- \n'
        #violation = violation + 'Instance ' + instance_identifier + ' will be terminated.\n ---------------- \n'
        #list missing/invalid tags in summary section of email       
        for tag_item in tag_dict:    
            if 'Missing required tag' in tag_dict[tag_item] or 'has an invalid value' in tag_dict[tag_item]: 
                instance_summary += tag_dict[tag_item]              
        #list present tags in details section of email
        for tag_item in tag_dict:
            if 'Missing required tag' not in tag_dict[tag_item] and 'has an invalid value' not in tag_dict[tag_item]: 
                instance_detail += tag_item + ': ' + tag_dict[tag_item] + '\n'
        instance_summary += '\n'
        instance_detail += '\n'                
    return [instance_summary, instance_detail]

#Grab the tags from the newly created instance    
def grab_tags(instance_identifier):
    response = ec2_client.describe_tags(
        Filters=[
            {
                'Name': 'resource-id',
                'Values': [
                    instance_identifier,
                ]
            },
        ],
    )
    return response

#look for SNS topic name in cloudformation stack
def find_snstopic():
    foundstack = cf_client.describe_stacks(
            StackName='fdn-logging'
    )    
    if foundstack:
        stackouts = foundstack['Stacks'][0]['Outputs']
        if stackouts:
            for outitem in stackouts:
                if outitem['OutputKey'] == 'rSecurityAlarmTopic':
                    sns_value = outitem['OutputValue']
    return sns_value

#send an SNS message alerting sysadmin of failed launch    
def publish_message(problem_instances, sns_value):
    message = sns_client.publish(
        TopicArn=sns_value,
        Message=problem_instances,
        Subject='New AWS EC2 instance launched with missing or invalid tags'
    )
    
#query the Dynamo DB table containing the required tags
def query_db():
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('RequiredTags')
    #query the table
    query = table.get_item(
        Key={
            'DataSet': 'Ec2Instance'
        }
    )
    query_response = query['Item']       
    defined_tags = query_response['TagToValue']
    return defined_tags

#respond to an EC2 instance launched CloudWatch event by checking instance tags
def lambda_handler(event, context):
    
    #sleep for 30 seconds to allow time for instance tags to populate
    time.sleep(30)

    #read in the required tags/values from Dynamo DB
    required_tags = query_db()

    #find the SNS topic to use to notify account owners of improperly tagged instances
    sns_value = find_snstopic()

    #grab the principal ID of the user launching the instance
    principal_arn = ''
    principal_text = ''
    principal_arn = event['detail']['userIdentity']['arn']
    if principal_arn != '':
        p = re.compile('arn:aws:[a-z]{3}:[a-zA-Z0-9]*:[0-9]*:')
        principal_name = p.sub("", principal_arn)
        principal_text = ' by ' + principal_name
    
    #grab the account number the event occurred in
    acct_number = event['detail']['responseElements']['ownerId']

    #grab the EC2 instance ID from the CloudWatch event
    problem_summary = ''
    problem_detail = ''
    created_instances = event['detail']['responseElements']['instancesSet']['items']
    for created_instance in created_instances:
        instance_identifier = created_instance['instanceId']

        #call the grab_tags function to get tag info from launched instance
        current_tags = grab_tags(instance_identifier)
    
        #call the find_violation function to look for issues with tags
        evaluation = find_violation(current_tags, required_tags, instance_identifier)
    
        #if tags are awry, terminate the instance (COMMENTED OUT) and notify account owner
        if evaluation:
            #term = ec2_client.terminate_instances(
            #    InstanceIds=[
            #        instance_identifier
            #    ]
            #)
            problem_summary += evaluation[0]
            problem_detail += evaluation[1]
    
    #call the publish_message function to email sysadmin if tags are awry
    if problem_summary:
        problem_instances = 'This email is being sent from an automated UITS process due to the following instance(s) recently launched' + principal_text + ' in account ' + acct_number + ' without required tags or tag values.\n\n' + problem_summary + 'Please resolve the tagging issue(s) on above instance(s). In the future, improperly tagged instances will be terminated at first launch. Instance details appear below for your convenience.\n\n' + problem_detail
        publish_message(problem_instances, sns_value)