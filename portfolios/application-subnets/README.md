# Generic Application Subnet Deployment Template

The purpose of this CloudFormation template is to facilitate deployment of Application Subnets in the AWS (production) environments.

## Use:
1. Check out this repository, so you have a local copy to work from.
2. In the AWS Console, select **CloudFormation**.
3. Select **Create Stack**.
4. Under **Choose a template**, select *Upload a template to Amazon S3*, then browse to locate this template (`generic-app-subnets.json`). Select **Next**.
5. Enter the following information in the resulting form:
    * **Stack name**: This should be unique and identifiable to the application using this subnet.  eg. `ProdNKEE`
    * **AppName**: Short name of the application.  eg. `ProdNetKernel-EnterpriseEdition`
    * **NetworkACLPrivate**: Look up and enter the appropriate ACL ID for the Private Network of this VPC.  eg. `acl-cadce9af`
    * **RouteTablePrivate**: Look up and enter the appropriate Route Table ID for the Private Network of this VPC.  eg. `rtb-5dd5ec38`
    * **SubnetPrivateAcidr**: Desired Private subnet for Zone A. Note that the smallest subnet possible in AWS is /28 (16 addresses).  eg. `10.221.92.0/28`
    * **SubnetPrivateBcidr**: Desired Private subnet for Zone B. Note that the smallest subnet possible in AWS is /28 (16 addresses).  eg. `10.221.92.16/28`
    * **VPCID**: From the drop-down list, select the appropriate VPC corresponding to all of the above.  eg. `uits-eas-production` or `uits-eas-development`
6. Select **Next**. This will take you to the **Options** screen, which can be skipped.
7. Select **Next**. This will take you to the **Review** screen to confirm the requested settings before selecting **Create** to actually create the subnets.

From there you can monitor the creation of the new CloudFormation Stack for this new set of private subnets.  