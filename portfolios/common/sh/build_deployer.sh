#!/bin/bash
#*******************************************************************************
#
# TITLE......: build_deployer.sh
# PARAMETER..:
#    /INPUTS.:
#
#*******************************************************************************

#*******************************************************************************
#
# function: validate_arguments
#
# This function parses the arguments from the command-line flags and makes sure
# that all necessary and required parameters have been passed to the program.
# If it encounters an issue, it aborts the program.
#
#
# INPUTS:  
#       -s (required) = CF Stack Name to Create
#       -u (optional) = Update CloudFormation Stack"
#
# RETURNS: none
#
#*******************************************************************************
function validate_arguments
{
# -- Get Account Name
AWS_ACCOUNT=`aws iam list-account-aliases --output text | awk '{print $2}'`

# -- If CF_STACK_NAME is not set then -s was not passed and we need a stack name
if [ -z "$CF_STACK_NAME" ]; then
  print_usage
  error_abort "A CloudFormation Stack Name was not passed, use -s"
fi

# -- this the NetID of the person most familiar with the resources being created, will default to kellehs for now
TAG_CONTACT_NETID="kellehs"

# -- Role ARN will be different based on what account we are in
# -- Eventually we need to query this from AWS instead of hard coding
# -- The CF Template is bigger than 51,200 characters so we need to call it from
# -- an S3 bucket.  The template URL variable will hold the location of the S3 bucket
TEMPLATE_FILE="file://~/git/service-catalog/portfolios/common/templates/deployer.yaml"
case ${AWS_ACCOUNT} in
  ua-uits-peoplesoft-nonprod)
    TAG_TICKET_NUMBER="CLOUD-15"
    TAG_SERVICE="Uaccess PeopleSoft"
    TAG_ACCOUNT_NUMBER="Uaccess PeopleSoft"
    TAG_SUBACCOUNT="Uaccess PeopleSoft"
    SG_CF_NAME="PeopleSoftSG"
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    KEY_PAIR_NAME="peoplesoft-keypair"
    TAG_ENV="dev"
    HOSTED_ZONE_NAME="ps-nonprod-aws.arizona.edu"
    ;;
  ua-uits-peoplesoft-prod)
    TAG_TICKET_NUMBER="CLOUD-15"
    TAG_SERVICE="Uaccess PeopleSoft"
    TAG_ACCOUNT_NUMBER="Uaccess PeopleSoft"
    TAG_SUBACCOUNT="Uaccess PeopleSoft"
    SG_CF_NAME="PeopleSoftSG"
    ROLE_ARN="arn:aws:iam::242275737326:role/fdn-iam-CloudFormationAdminDeployerRole-IP2KC5OSU7DS"
    KEY_PAIR_NAME="peoplesoft-keypair"
    TAG_ENV="prd"
    HOSTED_ZONE_NAME="ps-prod-aws.arizona.edu"
    ;;
  ua-uits-kuali-nonprod)
    TAG_TICKET_NUMBER="UAFAWS-16"
    TAG_SERVICE="Uaccess Kuali"
    TAG_ACCOUNT_NUMBER="Uaccess Kuali"
    TAG_SUBACCOUNT="Uaccess Kuali"
    SG_CF_NAME="Kuali-DbSg"
    ROLE_ARN="arn:aws:iam::397167497055:role/fdn-iam-CloudFormationAdminDeployerRole-12AJX3KHD35CR"
    KEY_PAIR_NAME="kuali-nonprod-keypair"
    TAG_ENV="dev"
    HOSTED_ZONE_NAME="ua-uits-kuali-nonprod.arizona.edu"
    ;;
  ua-uits-kuali-prod)
    TAG_TICKET_NUMBER="UAFAWS-16"
    TAG_SERVICE="Uaccess Kuali"
    TAG_ACCOUNT_NUMBER="Uaccess Kuali"
    TAG_SUBACCOUNT="Uaccess Kuali"
    SG_CF_NAME="Kuali-DbSg"
    ROLE_ARN="arn:aws:iam::740525297805:role/fdn-CloudFormationAdminDeployer"
    KEY_PAIR_NAME="kuali-prod-keypair"
    TAG_ENV="prd"
    HOSTED_ZONE_NAME="kuali-prod-aws.arizona.edu"
    ;;
  *)
    error_abort "Could not determine Role ARN based on the account (${AWS_ACCOUNT})"
    ;;
esac
}

#Default AZ to us-west-2a
AVAILIBILITY_ZONE="us-west-2a"

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage
{
echo "Usage: build_efs_volume.sh"
echo "Options:"
echo "       -s (required) = CF Stack Name to Create"
echo "       -u (optional) = Update CloudFormation Stack"
}

#*******************************************************************************
#
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
typeset MSG=$1

echo
echo "ERROR: ${MSG}"
echo "aborting . . ."
echo
exit 255
}

#*******************************************************************************
#
# function: check_if_env_exists
#
# This funtion will check if the CloudFromation Tempalte exists.  If so, it will
# evaluate the DESTROY_IF_EXISTS vairable.  If Y(es) then the CloudFormation 
# Template will be destroyed, if N(o) then the program will stop with an error
# stating that the environment is already running.
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function check_if_env_exists
{
#-- describe the CloudFormation Template stored in CF_STACK_NAME
# -- if the snapshot file was not populated with data then we have an issue and have to abort
# -- look for the strin arn:aws:cloudformation in the output, if it's there the environment exists
aws cloudformation describe-stacks --stack-name ${CF_STACK_NAME} --output text 2>&1 |grep -q "arn:aws:cloudformation" &> /dev/null

# -- if the grep finds the string it will return a 0, otherwise it will return a 1
if [ $? == 0 ]; then 
  ENV_EXISTS="Y"
else  
  ENV_EXISTS="N"
fi

# -- if the env exists check to see if this is an update
# -- if not updating then through an error and exit
if [ "${ENV_EXISTS}" == "Y" ]; then
  if [ "${UPDATE_CF}" == "N" ]; then
    print_usage
    echo
    error_abort "The CloudFormation Template ${CF_STACK_NAME} already exists. Pass a -u if you'd like to update it."
  fi
fi

# -- if the env does not exist check to see if this is an update
# -- if updating then throw an error and exit
if [ "${ENV_EXISTS}" == "N" ]; then
  if [ "${UPDATE_CF}" == "Y" ]; then
    print_usage
    echo
    error_abort "The CloudFormation Template ${CF_STACK_NAME} does not exist and a -u was passed."
  fi
fi

}

#*******************************************************************************
#
# function: build_environment
#
# This function will build the environment. 
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function build_environment
{

read -d '' JSON_STRING <<ENDJSON
[
 { "ParameterKey": "SecurityGroupCloudFormationName","ParameterValue": "${SG_CF_NAME}"}, 
 { "ParameterKey": "HostName","ParameterValue": "${CF_STACK_NAME}"}, 
 { "ParameterKey": "KeyName","ParameterValue": "${KEY_PAIR_NAME}"}, 
 { "ParameterKey": "AvailabilityZone","ParameterValue": "${AVAILIBILITY_ZONE}"}, 
 { "ParameterKey": "HostedZoneName","ParameterValue": "${HOSTED_ZONE_NAME}"}, 
 { "ParameterKey": "ServiceTag","ParameterValue": "${TAG_SERVICE}" },
 { "ParameterKey": "EnvironmentTag","ParameterValue": "${TAG_ENV}"}, 
 { "ParameterKey": "ContactNetIdTag","ParameterValue": "${TAG_CONTACT_NETID}" },
 { "ParameterKey": "AccountNumberTag","ParameterValue": "${TAG_ACCOUNT_NUMBER}" },
 { "ParameterKey": "SubAccountTag","ParameterValue": "${TAG_SUBACCOUNT}" },
 { "ParameterKey": "TicketNumberTag","ParameterValue": "${TAG_TICKET_NUMBER}" },
 { "ParameterKey": "CreatedByTag","ParameterValue": "${TAG_CONTACT_NETID}" }
]
ENDJSON

if [ "${UPDATE_CF}" == "N" ]; then

  aws cloudformation create-stack \
    --region us-west-2 \
    --stack-name "${CF_STACK_NAME}"  \
    --capabilities "CAPABILITY_IAM" \
    --template-body "${TEMPLATE_FILE}" \
    --parameters "${JSON_STRING}" \
    --role-arn "${ROLE_ARN}" \
    --disable-rollback

else

  aws cloudformation update-stack \
    --region us-west-2 \
    --stack-name "${CF_STACK_NAME}"  \
    --capabilities "CAPABILITY_IAM" \
    --template-body "${TEMPLATE_FILE}" \
    --parameters "${JSON_STRING}" \
    --role-arn "${ROLE_ARN}"
fi
}

#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
# -- set environment
#set_environment
START_TS=`${DATE}`
UPDATE_CF="N"

# -- parse command-line options
while getopts :s:u arguments
do
  case $arguments in
    s)
       CF_STACK_NAME=${OPTARG,,}
       ;;
    u)
       UPDATE_CF="Y"
       ;;
   \?)
       print_usage
       echo
       error_abort "option -${OPTARG} is not a valid argument"
       exit 255
  esac
done

# validate input parameters
validate_arguments

# -- check to see if the environment is running
# -- if so if the DESTROY_IF_EXISTS = Y then destroy the environment first
# -- if not, then exit program with error
check_if_env_exists

# -- if we reach this point we can build the environment
build_environment
