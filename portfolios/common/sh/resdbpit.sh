#
# TITLE......: resdbpit.sh
# LOCATION...:
# PARAMETER..:
#    /INPUTS.:
#
# USAGE......: resdbpit.sh 
#              [-s SRCDB] 
#              [-t TARGSID] 
#              [-d Database Name]
#              [-P Point in Time Date (Format 2017-11-26T18:56:40MST)]
# OUTPUT.....:
# REQUIRES ..: jq
#
#   jq is a lightweight and flexible command-line JSON processor
#   installed using the following ...
#   wget -qP /usr/local/bin https://stedolan.github.io/jq/download/linux64/jq && chmod +x /usr/local/bin/jq
#
# DESCRIPTION:
#   Clone AWS RDS 
#   kfs3stg => kfs3sbi
#
#   http://docs.aws.amazon.com/AmazonRDS/latest/APIReference/API_RestoreDBInstanceToPointInTime.html
#
#   date -u returns UTC
#   aws rds restore-db-instance-to-point-in-time
#       --source-db-instance-identifier kfstg
#   	--target-db-instance-identifier kfsbi
#   	--restore-time 2017-07-21T15:40:00.000
#   	--option-group-name "peoplesoft-oracle-ee-12-1"
#

#CMD_CONFIG=/Users/ehughes/scripts/cmd_config.sh

# control output from AWS CLI 
CLIOUT=json
#export AWS_DEFAULT_PROFILE=kuali-nonprod

#*******************************************************************************
# function: describe_db_instance
#
# This function returns information about a provisioned RDS instance
#
# INPUTS:  first argument - instance identifier
#
# RETURNS: 
#
#*******************************************************************************
function describe_db_instance
{
  JSON=`aws rds describe-db-instances --db-instance-identifier $1 --output=$CLIOUT 2>&1` 
  if [ $? != 0 ]; then
    return 1
  fi ;
  return 0
}

#*******************************************************************************
#
# function: get_logdate
#
# This function returns the current date in YYYY/MM/DD HH24:MI:SS format, used
# in making the the entries in the log file readable.
#
# INPUTS:  none
#
# RETURNS: current date in YYYY/MM/DD HH24:MI:SS format
#
#*******************************************************************************
function get_logdate
{
  DT=`date '+%Y/%m/%d %T'`
  echo ${DT}
}    

#*******************************************************************************
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
  typeset MSG=$1

  echo
  echo "ERROR: ${MSG}"
  echo "aborting . . ."
  echo
  exit 255
}

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage
{
  echo "Usage: resdbpit.sh"
  echo "       [-s source SID] [-t target SID]"
  echo "Options:"
  echo "       -s = source database"
  echo "       -t = target database"
}

#*******************************************************************************
#
# function: validate_arguments
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function validate_arguments
{
  # -- make sure both the source and target databases are specified
  if [ "${TRGSID}" = "" ]; then
    error_abort "target database instance must be specified"
  fi

  if [ "${SRCSID}" = "" ]; then
    error_abort "source database instance must be specified"
  fi

  # name:
  # cannot end with a hyphen
  if [ "${TRGSID: -1}" == "-" ] ||
    # contains no special characters
    ! [[ ${TRGSID:0:1} =~ ^[a-zA-Z]+$ ]] ||
    # does not contain 2 consecutive hyphens
    [[ ${TRGSID} = *"--"* ]] ; then
    error_abort "target name must begin with an alphabetic character, cannot end with a hyphen or contain two consecutive hyphens"
  fi

  # less than 63 in length 
  if [ ${#TRGSID} -ge 63 ] ; then
    error_abort "target name must contain from 1 to 63 alphanumeric characters or hyphens"
  fi

  # -- make sure the source db and target db are not the same
  if [ "${SRCSID}" = "${TRGSID}" ]; then
    error_abort "source and target instances are the same"
  fi

  # -- Validate Point in Time Date format if one was passed
  if ! [ -z $PIT_DATE ]; then
    date +"$PIT_FORMAT" -d "$PIT_DATE" 2>&1 > /dev/null 
    if ! [ "$?" = "0" ]; then
      error_abort "Point in Time refresh date format invalid should look like 2017-11-26T18:56:40MST ($PIT_DATE)"
    fi
  fi
}

#*******************************************************************************
#
# function: get_instance_status
#
# INPUTS:  first argument - JSON block
#
# RETURNS: none
#
#*******************************************************************************
function get_instance_status
{
  DBINSTANCESTATUS=`echo $1 |jq --raw-output .DBInstances[].DBInstanceStatus`
  TARGETDBNAME=`echo $1 |jq --raw-output .DBInstances[].DBName`
}

#*******************************************************************************
#
# function: get_src_endpoint
#
# INPUTS:  first argument - JSON block
#
# RETURNS: none
#
#*******************************************************************************
function get_endpoint
{
  DBENDPOINT=`echo $1 |jq --raw-output .DBInstance.Endpoint.Address`
}

#*******************************************************************************
#
# function: get_vpc_security_group_ids
#
# INPUTS:  first argument - JSON block
#
# RETURNS: none
#
#*******************************************************************************
function get_vpc_security_group_ids
{
  VPCSECURITYGROUPID=`echo $1 |jq --raw-output .DBInstances[].VpcSecurityGroups[].VpcSecurityGroupId`
}

#*******************************************************************************
#
# function: get_port
#
# INPUTS:  first argument - JSON block
#
# RETURNS: none
#
#*******************************************************************************
function get_port
{
  PORT=`echo $1 |jq --raw-output .DBInstance.Endpoint.Port`
}

#*******************************************************************************
#
# function: parse_msg
#
# INPUTS:  first argument - 
#
# RETURNS: none
#
#*******************************************************************************   
function parse_msg
{
  ERRMSG=`echo "$1"|sed '/^$/d'`
}

#*******************************************************************************
#
# function: is_instance_available
#
# Building an RDS instance and can take longer than 30 mins causing the -
#   Waiter DBInstanceAvailable failed: Max attempts exceeded
#
# By default, the "aws rds wait db-instance-available" polls every 30 seconds
# for a maximum of 60 tries (30 mins).
# Therefore, we shall use the following function to test the availability
# of an RDS instance
#
# INPUTS: none
#
# RETURNS: none
#
#*******************************************************************************
function is_instance_available()
{
  cnt=0
  while [ $cnt -lt 6 ]
  do
    CLI=`aws rds wait db-instance-available --db-instance-identifier $TRGSID 2>&1`

    if [ $? = 0 ]; then
      return 0
      break
    fi
    cnt=$(( cnt+1 ))
  done
  return 1
}          

#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
# Initialize Variables
PIT_FORMAT="%Y-%m-%dT%H:%M:%S%Z"

while getopts :s:t:d:P: arguments; do
  case $arguments in
    s) 
      SRCSID=${OPTARG}
      ;;
    t) 
      TRGSID=${OPTARG}
      ;;
    d) 
      SOURCEDBNAME=${OPTARG^^}
      ;;
    P)
      PIT_DATE=${OPTARG}
      ;;
   \?)
     print_usage
     echo
     error_abort "option -${OPTARG} is not a valid argument"
     exit 255
  esac
done


validate_arguments

# source instance must exist
describe_db_instance $SRCSID

if ! [ $? = 0 ]; then
  error_abort "$ERRMSG" 
fi

JSONSRC="$JSON"

# abort if target instance exists
describe_db_instance $TRGSID

if [ $? = 0 ] ; then
  error_abort "An error occurred when calling the DescribeDBInstances operation: DBInstance $TRGSID already exists"
fi
# get source instance properties 
INSTANCECREATETIME=$(echo $JSONSRC |jq --raw-output .DBInstances[].InstanceCreateTime)
DBSUBNETGROUPNAME=$(echo $JSONSRC |jq --raw-output .DBInstances[].DBSubnetGroup.DBSubnetGroupName)
VPCSECURITYGROUPID=$(echo $JSONSRC |jq --raw-output .DBInstances[].VpcSecurityGroups[].VpcSecurityGroupId)
TARGETDBNAME=$(echo $JSONSRC |jq --raw-output .DBInstances[].DBName)

# need to validate Point in Time date if one was passed
# first check will make sure the PIT_DATE >= the Creation Date of the Instance
# next check will be to make sure the PIT_DATE <= the Latest Restorable Time, If not wait and then try again
if ! [ -z $PIT_DATE ]; then
  #Check to make sure the PIT_DATE is >= the Instance Creation Date
  #Convert dates into seconds so we can compare
  PIT_DATE_SEC=$(date -d "$PIT_DATE" +"%s")
  INST_CREATE_SEC=$(date -d "$INSTANCECREATETIME" +"%s")
  if [ "$PIT_DATE_SEC" -lt "$INST_CREATE_SEC" ]; then
    error_abort "Point in Time date passed ($PIT_DATE) is less than Instance Create Date ($INSTANCECREATETIME)"
  fi
  #Now check that the PIT_DATE is no greater than 10 minutes of the Latest Restorable Time
  #and the PIT_DATE is <= the Latest Restoreable Time
  while true
  do
    LATEST_RESTOREABLE_TIME=$(aws rds describe-db-instances --db-instance-identifier $SRCSID --output text --query 'DBInstances[*].LatestRestorableTime')
    LATEST_RESTOREABLE_SEC=$(date -d "$LATEST_RESTOREABLE_TIME" +"%s")
    PIT_LATEST_DIFF_SEC=$(expr $PIT_DATE_SEC - $LATEST_RESTOREABLE_SEC)
    #Check to make sure the PIT_DATE is not greater then 10 minutes of the Latest Restoreable Time
    if [ "$PIT_LATEST_DIFF_SEC" -ge "600" ]; then
      error_abort "The Point in Time date passed ($PIT_DATE) is greater than 10 minutes of the Latest Restorable Time ($LATEST_RESTOREABLE_TIME)"
    fi
    #Now check to make sure the PIT_DATE <= Latest Restorable Time
    if [ $PIT_DATE_SEC -le $LATEST_RESTOREABLE_SEC ]; then
      USE_THIS_DATE=$PIT_DATE
      break
    fi
    sleep 5
  done
else
  USE_THIS_DATE=$(aws rds describe-db-instances --db-instance-identifier $SRCSID --output text --query 'DBInstances[*].LatestRestorableTime')
fi

#If SOURCEDBNAME is not populated then use TARGETDBNAME for he DBNAME
if [ -z "$SOURCEDBNAME" ]; then
  SOURCEDBNAME=$TARGETDBNAME
fi

printf "[$(get_logdate)] >> ********************************************************************************\n"
printf "[$(get_logdate)] >> ********************************************************************************\n"
printf "[$(get_logdate)] >> STARTING:  Restore of [$SRCSID] \n"
printf "[$(get_logdate)] >> --------------------------------------------------------------------------------\n"

CLI=`aws rds restore-db-instance-to-point-in-time \
     --source-db-instance-identifier $SRCSID \
     --target-db-instance-identifier $TRGSID \
     --restore-time $USE_THIS_DATE \
     --db-name $SOURCEDBNAME \
     --db-subnet-group-name $DBSUBNETGROUPNAME --output=$CLIOUT 2>&1`

if [ $? != 0 ]; then
  # restore-db-instance-to-point-in-time failed to build target instance
  error_abort "$CLI"
fi

# create RDS instance complete
if ! is_instance_available; then
  # Waiter DBInstanceAvailable failed: Max attempts exceeded
  error_abort "Waiter DBInstanceAvailable failed: Max attempts exceeded"
fi

printf "[$(get_logdate)] >> restore-db-instance-to-point-in-time: [$SRCSID] complete \n"  

# modify db-instance
# aws rds modify-db-instance --db-instance-identifier kfsbi \
#  --vpc-security-group-ids sg-a10f4ad8 sg-5e0f4a27 sg-040f4a7d
MODCLI=`aws rds modify-db-instance \
         --db-instance-identifier $TRGSID  \
         --vpc-security-group-ids $VPCSECURITYGROUPID \
         --apply-immediately \
         --output=$CLIOUT 2>&1`

if [ $? != 0 ]; then
  error_abort "$ERRMSG"
fi

if ! is_instance_available; then
  # Waiter DBInstanceAvailable failed: Max attempts exceeded
  error_abort "Waiter DBInstanceAvailable failed: Max attempts exceeded during modify for Security Groups"
fi

get_endpoint "$MODCLI"
printf "[$(get_logdate)] >> modify-db-instance: [$TRGSID] complete \n"  
get_port "$MODCLI"
printf "Endpoint: $DBENDPOINT:$PORT\n"
