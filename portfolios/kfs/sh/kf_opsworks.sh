#!/bin/bash
#*******************************************************************************
#
# TITLE......: kf_opsworks.sh
# PARAMETER..:
#    /INPUTS.:
#              -p  Pillar (kf)
#              -e  Environment (SUP)
#              -s  Source Environment (PRD)
#              -d  Destroy the CloudFormation Stack first if it exists
#              -D  Only Destroy the CloudFormation Stack if it exists
#              -m  Mode A=App Only, B=Both App and Database
#
#*******************************************************************************

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage
{
echo "Usage: kf_sup_opsworks.sh"
echo "Options:"
echo "       -p (required) = Pillar valid values are KF"
echo "       -e (required) = Environment including but not limited to SUP"
echo "       -s (optional) = Source Environment, populate this if refreshing from a different environment (ie PRD)"
echo "       -d (optional) = Destroy the Environment first if it is already running, then build it"
echo "       -D (optional) = Destroy the Environment if it is already running, and then stop"
echo "       -m  Mode A=App Only, B=Both App and Database"
}

#*******************************************************************************
#
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
typeset MSG=$1

echo
echo "ERROR: ${MSG}"
echo "aborting . . ."
echo
exit 255
}


#*******************************************************************************
#
# function: check_if_env_exists
#
# This funtion will check if the CloudFromation Tempalte exists.  If so, it will
# evaluate the DESTROY_IF_EXISTS vairable.  If Y(es) then the CloudFormation
# Template will be destroyed, if N(o) then the program will stop with an error
# stating that the environment is already running.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function check_if_env_exists
{
# -- describe the CloudFormation Template stored in STACK_NAME
# -- if the snapshot file was not populated with data then we have an issue and have to abort
# -- look for the strin arn:aws:cloudformation in the output, if it's there the environment exists
aws cloudformation describe-stacks --stack-name ${STACK_NAME} 2>&1 |grep -q "arn:aws:cloudformation" &> /dev/null

# -- if the grep finds the string it will return a 0, otherwise it will return a 1
if [ $? == 0 ]; then
  ENV_EXISTS="Y"
else
  ENV_EXISTS="N"
fi

# -- if the env exists and -d or -D (destroy env) was passed then destroy the environment
if [ "${ENV_EXISTS}" == "Y" ]; then
  if [ "${DESTROY_IF_EXISTS}" == "Y" ]; then
    destroy_environment
  elif [ "${DESTROY_IF_EXISTS}" == "N" ]; then
    print_usage
    echo
    error_abort "The CloudFormation Template ${STACK_NAME} already exists and a \"-D\" or \"-d\" was not passed."
  fi
fi

# --if -D was passed then we only want to destroy the environment and be done
if [ "${DESTROY_ONLY}" == "Y" ]; then
  if [ "${ENV_EXISTS}" == "Y" ]; then
    echo "CloudFormation Stack ${STACK_NAME} has been successfully destroyed.  All done."
  else
    echo "CloudFormation Stack ${STACK_NAME} did not exist, so there is nothing to destroy."
  fi
  exit
fi
}

#*******************************************************************************
#
# function: destroy_environment
#
# This function will destroy the environment.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function destroy_environment
{

# -- run pre-refresh process on targert DB before deleting CloudFormation Delete
# -- this will retain important info for environment refreshes
OUTPUT=$(sqlplus -s /@AWSPSTLS << EOF-SQL
set feed off timing off heading off pagesize 0
begin
  aws_refresh.pre_refresh('${UPPER_PILLAR}','${UPPER_ENV}','N');
end;
/
EOF-SQL
)

# -- if the there was an error exit with error
# -- this will return 0 if there is an ORA- error and 1 if there is not
echo $OUTPUT | egrep "ORA-" > /dev/null
if [ "$?" == "0" ]; then
  error_abort "Error running aws_refesh.pre_refresh-($OUTPUT)"
fi

# -- issue the delete stack command
aws cloudformation delete-stack \
   --region us-west-2 \
   --stack-name "${STACK_NAME}"  \
   --role-arn "${ROLE_ARN}"

# -- if there was an error then abort
if [ $? -ne 0 ]; then
  error_abort "Problems deleting CloudFormation Stack ${STACK_NAME}."
fi

# -- if the MODE is for Both App and DB then destroy the DB too (in parallel)
if [ "$MODE" = "B" ]; then
  aws cloudformation delete-stack \
   --region us-west-2 \
   --stack-name "${STACK_NAME_DB}"  \
   --role-arn "${ROLE_ARN}"
fi

# -- if there was an error then abort
if [ $? -ne 0 ]; then
  error_abort "Problems deleting CloudFormation Stack ${STACK_NAME_DB}."
fi

# -- wait for delete to be complete, it will check every 30 seconds for an hour
# -- if delete does not complete by then it will throw an error
aws cloudformation wait stack-delete-complete\
   --region us-west-2 \
   --stack-name "${STACK_NAME}"

# -- wait for DB delete to be complete, it will check every 30 seconds for an hour
# -- if delete does not complete by then it will throw an error
if [ "$MODE" = "B" ]; then
  aws cloudformation wait stack-delete-complete\
   --region us-west-2 \
   --stack-name "${STACK_NAME_DB}"
fi

}

#*******************************************************************************
#
# function: validate_arguments
#
# This function parses the arguments from the command-line flags and makes sure
# that all necessary and required parameters have been passed to the program.
# If it encounters an issue, it aborts the program.
#
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function validate_arguments
{
# -- make sure pillar is specified and is KF
pillar_list="KF"
good_pillar="N"
for curr_pillar in $pillar_list; do
  if [ "${UPPER_PILLAR}" == "$curr_pillar" ]; then
    good_pillar="Y"
  fi
done

if [ $good_pillar = "N" ]; then
  error_abort "Either a pillar wasn't passed or was invalid please use -p with the value KF (${UPPER_PILLAR})"
fi
LOWER_PILLAR=${UPPER_PILLAR,,}

# -- make sure an Environment was passed
if [ "${UPPER_ENV}" = "" ]; then
  error_abort "An environmnet was not passed, use -e and include the enviornment name i.e. DEV, TST, STG etc."
fi
LOWER_ENV=${UPPER_ENV,,}

# -- if the source environment was not passed then default to the target environment
if [ -z ${UPPER_SOURCE_ENV} ]; then
  UPPER_SOURCE_ENV=${UPPER_ENV}
fi
LOWER_SOURCE_ENV=${UPPER_SOURCE_ENV,,}

# -- Validate MODE, if one was not passed then default to B for Both
if [ "$MODE" = "" ]; then
  MODE="B"
fi
if [ "$MODE" != "A" ] && [ "$MODE" != "B" ]; then
  error_abort "Mode passed it not valid.  Must be A for just the Appliation or B for Both App and DB ($MODE)"
fi

# -- default DESTROY_IF_EXISTS to N if it was not passed via the -D of -d arguments
# -- this parameter will be used to determine if we will destroy the environmnet
# -- before we build it.  This will be the case if we are refreshing an environment.
if [ -z ${DESTROY_IF_EXISTS} ]; then
  DESTROY_IF_EXISTS="N"
fi

#TODO Populate with production or support environment values before running

#############################
# Environment Configuration #
#############################
# Environment slug -- change to 'sup' if deploying the sup environment instead
ENV_SLUG=$LOWER_ENV
# ARN of the Foundational CloudFormation deployer role to use
ROLE_ARN="arn:aws:iam::740525297805:role/fdn-CloudFormationAdminDeployer"
# Hostname of the database to connect to
DB_HOSTNAME="${LOWER_PILLAR}${LOWER_ENV}.csqvaa9kcdo8.us-west-2.rds.amazonaws.com"
SOURCE_DB_SERVICE_NAME="${LOWER_PILLAR}${LOWER_SOURCE_ENV}"
# Service name of the database to connect to
DB_SERVICE_NAME="${LOWER_PILLAR}${LOWER_ENV}"

# HTTPS URL to the Chef Cookbooks previously uploaded into S3
# --First get cookbook name from a parameter store
KFS_COOKBOOK=`aws ssm get-parameter --name Cookbook${UPPER_ENV} --with-decryption --output text --query 'Parameter.[Value]'`
CUSTOM_COOKBOOKS_S3_BUCKET="https://s3-us-west-2.amazonaws.com/kfs-prod-cloudformation-deployment/${KFS_COOKBOOK}"

# Name of the S3 bucket containing the fincials/classes and financials/security directories with binary deployment artifacts
#   (usually this will be the same bucket that the above HTTPS URL also uses)
S3_RESOURCE_BUCKET="kfs-prod-cloudformation-deployment"
# Hosted Zone ID of the Route 53 Hosted Zone to use (this is on the "Hosted zones" console page; it is NOT the full ARN)
HOSTED_ZONE_ID="Z3TXM9I3B7P8MC"
# Name of the Route 53 Hosted Zone to use (i.e. kuali-prod-aws.arizona.edu)
HOSTED_ZONE_NAME="kuali-prod-aws.arizona.edu"
# VPC ID of the VPC to create the application stack in
VPC_ID="vpc-c6c639a0"
# Subnet ID of Private Subnet A
PRIVATE_SUBNET_A="subnet-f31b38ba"
# Subnet ID of Private Subnet B
PRIVATE_SUBNET_B="subnet-910901f6"
#ARN of *.uaccess.arizona.edu
SSL_CERT_ARN="arn:aws:acm:us-west-2:740525297805:certificate/38158055-4f24-4761-bd91-0c726b235975"
# Name of the EC2 keypair to use for root ssh access to the provisioned EC2 instance
EC2_KEYPAIR_NAME="kuali-prod-keypair"
# File mount path prefixed with a colon to the EFS File system ID (for example, kfs:fs-...)
EFS_MOUNT_INFO="kfs:fs-67a227ce"
# Security group ID of the EFS target security group (this is an output from the EFS CloudFormation stack)
EFS_TARGET_SECURITY_GROUP="sg-6c005810"
#B2B Site hostname to connect to
if [ "$UPPER_ENV" = "PRD" ]; then
  B2B_SITE="solutions.sciquest.com"
  EDS_HOST="ldaps://eds.iam.arizona.edu"
else
  B2B_SITE="usertest.sciquest.com"
  EDS_HOST="ldaps://eds.test.iam.arizona.edu"
fi

########
# Tags #
########
# See the overall current tagging strategy here: https://confluence.arizona.edu/display/ECS/Cloud+Tagging+Policy
# Enter your NetID for the 'createdby' tag value
TAG_CREATEDBY="met"

#########################################################
# Credentials and other Secret Deployment Configuration #
#########################################################
# IAM Access Key for the Route 53 registerer stack, created as part of the provisioning instructions
DEPLOYMENT_ROUTE53_REGISTER_ID=`aws ssm get-parameter --name DeploymentRoute53RegistererAccessKeyID --with-decryption --output text --query 'Parameter.[Value]'`
# IAM Secret Key for the Route 53 registerer stack, created as part of the provisioning instructions
DEPLOYMENT_ROUTE53_REGISTER_KEY=`aws ssm get-parameter --name DeploymentRoute53RegistererAccessKey --with-decryption --output text --query 'Parameter.[Value]'`
# IAM Access credentials for Docker repository access. See the Stache secret 'aws/ua-erp/docker-registry-access'
DOCKER_ACCESS_KEY=`aws ssm get-parameter --name AppDockerAccessKey --with-decryption --output text --query 'Parameter.[Value]'`
DOCKER_SECRET_KEY=`aws ssm get-parameter --name AppDockerSecretKey --with-decryption --output text --query 'Parameter.[Value]'`

##########################################################
# Credentials and other Secret Application Configuration #
##########################################################
#Determine parameter names based on if this is production or non-production
if [ "$UPPER_ENV" = "PRD" ]; then
  DB_USERNAME_PARAM="AppDBUsername"
  DB_PASSWORD_PARAM="AppDBPassword"
  RICE_DB_PASSWORD_PARAM="RiceDBPassword"
  RICE_KEYSTORE_PW_PARAM="AppKeystorePassword"
  ENCRYPTION_KEY_PARAM="AppKeystoreEncryptionKey"
  B2B_PO_USERNAME_PARAM="AppB2BPurchaseOrderUsername"
  B2B_PO_PASSWORD_PARAM="AppB2BPurchaseOrderPassword"
  B2B_SHOP_USERNAME_PARAM="AppB2BShoppingUsername"
  B2B_SHOP_PASSWORD_PARAM="AppB2BShoppingPassword"
  EDS_USERNAME_PARAM="AppLdapUsername"
  EDS_PASSWORD_PARAM="AppLdapPassword"
else
  DB_USERNAME_PARAM="AppDBUsernameNP"
  DB_PASSWORD_PARAM="AppDBPasswordNP"
  RICE_DB_PASSWORD_PARAM="RiceDBPasswordNP"
  RICE_KEYSTORE_PW_PARAM="AppKeystorePasswordNP"
  ENCRYPTION_KEY_PARAM="AppKeystoreEncryptionKeyNP"
  B2B_PO_USERNAME_PARAM="AppB2BPurchaseOrderUsernameNP"
  B2B_PO_PASSWORD_PARAM="AppB2BPurchaseOrderPasswordNP"
  B2B_SHOP_USERNAME_PARAM="AppB2BShoppingUsernameNP"
  B2B_SHOP_PASSWORD_PARAM="AppB2BShoppingPasswordNP"
  EDS_USERNAME_PARAM="AppLdapUsernameNP"
  EDS_PASSWORD_PARAM="AppLdapPasswordNP"
fi

# Database credentials to access the 'kulowner' schema
DB_USERNAME=`aws ssm get-parameter --name $DB_USERNAME_PARAM --with-decryption --output text --query 'Parameter.[Value]'`
DB_PASSWORD=`aws ssm get-parameter --name $DB_PASSWORD_PARAM --with-decryption --output text --query 'Parameter.[Value]'`
RICE_DB_PASSWORD=`aws ssm get-parameter --name $RICE_DB_PASSWORD_PARAM --with-decryption --output text --query 'Parameter.[Value]'`
# Password to unlock the binary rice.keystore file
RICE_KEYSTORE_PW=`aws ssm get-parameter --name $RICE_KEYSTORE_PW_PARAM --with-decryption --output text --query 'Parameter.[Value]'`
# Encryption key to the rice.keystore file
ENCRYPTION_KEY=`aws ssm get-parameter --name $ENCRYPTION_KEY_PARAM --with-decryption --output text --query 'Parameter.[Value]'`
# Credentials to use when interacting with B2B for POs
B2B_PO_USERNAME=`aws ssm get-parameter --name $B2B_PO_USERNAME_PARAM --with-decryption --output text --query 'Parameter.[Value]'`
B2B_PO_PASSWORD=`aws ssm get-parameter --name $B2B_PO_PASSWORD_PARAM --with-decryption --output text --query 'Parameter.[Value]'`
# Credentials to use when interacting with B2B for shop catalogs
B2B_SHOP_USERNAME=`aws ssm get-parameter --name $B2B_SHOP_USERNAME_PARAM --with-decryption --output text --query 'Parameter.[Value]'`
B2B_SHOP_PASSWORD=`aws ssm get-parameter --name $B2B_SHOP_PASSWORD_PARAM --with-decryption --output text --query 'Parameter.[Value]'`
# Credentials to use when interacting with EDS for LDAP federation
EDS_USERNAME=`aws ssm get-parameter --name $EDS_USERNAME_PARAM --with-decryption --output text --query 'Parameter.[Value]'`
EDS_PASSWORD=`aws ssm get-parameter --name $EDS_PASSWORD_PARAM --with-decryption --output text --query 'Parameter.[Value]'`
# License key for New Relic monitoring
NEW_RELIC_LICENSE_KEY=`aws ssm get-parameter --name AppNewRelicKey --with-decryption --output text --query 'Parameter.[Value]'`
# IAM credentials of the SMTP IAM user
SMTP_USERNAME=`aws ssm get-parameter --name AppSMTPUsername --with-decryption --output text --query 'Parameter.[Value]'`
SMTP_PASSWORD=`aws ssm get-parameter --name AppSMTPPassword --with-decryption --output text --query 'Parameter.[Value]'`

# Calculated values -- these shouldn't need to change
STACK_NAME="kfs7-$ENV_SLUG"
STACK_NAME_DB="${STACK_NAME}-DB"

#Per Heather via email 03/08/2018
#MEM-19288 Grab Docker Tags from an SMS parameter store
#KFS_DOCKER_TAG_NAME="7.20170323-ua-release44"
#RICE_DOCKER_TAG_NAME=$ENV_SLUG
KFS_DOCKER_TAG_NAME=`aws ssm get-parameter --name DockerTagKFS${UPPER_ENV} --with-decryption --output text --query 'Parameter.[Value]'`
RICE_DOCKER_TAG_NAME=`aws ssm get-parameter --name DockerTagRice${UPPER_ENV} --with-decryption --output text --query 'Parameter.[Value]'`
RHUBARB_DOCKER_TAG_NAME=`aws ssm get-parameter --name DockerTagRhubarb${UPPER_ENV} --with-decryption --output text --query 'Parameter.[Value]'`

# -- check to see if the environment is running
# -- if so if the DESTROY_IF_EXISTS = Y then destroy the environment first
# -- if not, then exit program with error
check_if_env_exists

#######################
#Popultate DB_INSTANCE and DB_LATEST_SNAPSHOT Start
#######################
# -- find latest snapshot, this will return a snapshot name, we will test to see if that snapshot exists
# -- in the next step
DB_LATEST_SNAPSHOT=`aws rds describe-db-snapshots --db-instance-identifier $SOURCE_DB_SERVICE_NAME --output text 2>&1 | sort -k10 -r | awk "NR==1{print}" | awk '{print $6}'`

# -- if the variable was not populated then default it to something
if [ -z ${DB_LATEST_SNAPSHOT} ]; then
  DB_LATEST_SNAPSHOT="~~none~~"
fi

# -- now check to see that the DB_LATEST_SNAPSHOT actually exists
aws rds describe-db-snapshots --db-instance-identifier $SOURCE_DB_SERVICE_NAME --output text 2>&1 | grep ${DB_LATEST_SNAPSHOT} &> /dev/null
if ! [ "$?" == "0" ]; then
  error_abort "An RDS Snapshot does not exist for the DB Instance ${DB_SERVICE_NAME}"
fi

# -- populate Database Instance Type
if [ "$ENV_SLUG" = "prd" ]; then
  DB_INST_CLASS="db.m4.xlarge"
else
  DB_INST_CLASS="db.m4.large"
fi

# -- If this is SUP then we don't want to take a final snapshot when the DB is deleted
DB_FINAL_SNAPSHOT="Y"
if [ "$UPPER_ENV" = "SUP" ]; then
  DB_FINAL_SNAPSHOT="N"
fi

# Application Short Name - currently just used for url building -- change to 'financialsup' if deploying the sup environment
case $UPPER_ENV in
  PRD)
    APPLICATION="financials"
    ;;
  SUP)
    APPLICATION="financialsup"
    ;;
  *)
    error_abort "Could not determine APPLICATION based on the environment passed (${UPPER_ENV})"
    ;;
esac

APP_BASE_URL="https://$APPLICATION.uaccess.arizona.edu"
RICE_BASE_URL="https://rice-$APPLICATION.uaccess.arizona.edu"
#Added 06/15/2018 for release 46
EMAIL_FOR_SNS="katt-automation@list.arizona.edu"

}

#*******************************************************************************
#
# function: build_environment
#
# This function will build the environment.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function build_environment
{

# -- Before building need to make sure the CloudWatch Log group does not exist
# -- It's normally destroyed with the CF stack is removed but sometimes it lingers
LOG_GROUP_NAME="kfs7-${ENV_SLUG}-application"
CLI=`aws logs describe-log-groups \
     --log-group-name-prefix ${LOG_GROUP_NAME} \
     --output text 2>&1`

# -- If error running cli command then throw an error and stop
if ! [ $? = 0 ]; then
  error_abort "Error describing CloudWatch Log Group ($CLI)"
fi

# -- If the CLI variable is populated that means the Log Group exists
# -- and needs to be removed before creating the CF Stack
if [ "${CLI}" != "" ]; then
  CLI=$(aws logs delete-log-group --log-group-name ${LOG_GROUP_NAME} 2>&1)
  if ! [ $? = 0 ]; then
    error_abort "Error deleting CloudWatch Log Group ${LOG_GROUP_NAME} ($CLI)"
  fi
fi

# -- if the MODE = B (for both app and DB) the DB needs to be created first
if [ "$MODE" = "B" ]; then
read -d '' JSON_STRING <<ENDJSON
[
  { "ParameterKey": "EnvSlug",      "ParameterValue": "$ENV_SLUG" },
  { "ParameterKey": "AppDBServicename", "ParameterValue": "$DB_SERVICE_NAME" },
  { "ParameterKey": "DBSnapshotID",  "ParameterValue": "$DB_LATEST_SNAPSHOT" },
  { "ParameterKey": "DBInstClass",  "ParameterValue": "$DB_INST_CLASS" },
  { "ParameterKey": "DBFinalSnapshot",  "ParameterValue": "$DB_FINAL_SNAPSHOT" },
  { "ParameterKey": "TagService",     "ParameterValue": "UAccess Financials" },
  { "ParameterKey": "TagName",     "ParameterValue": "kfs7-$ENV_SLUG" },
  { "ParameterKey": "TagEnvironment",     "ParameterValue": "$ENV_SLUG" },
  { "ParameterKey": "TagCreatedBy",     "ParameterValue": "$TAG_CREATEDBY" },
  { "ParameterKey": "TagContactNetId",     "ParameterValue": "fischerm" },
  { "ParameterKey": "TagAccountNumber",     "ParameterValue": "UAccess Financials" },
  { "ParameterKey": "TagTicketNumber",     "ParameterValue": "UAF-3905" },
  { "ParameterKey": "TagResourceFunction",     "ParameterValue": "KFS 7 $ENV_SLUG Application Stack" }
]
ENDJSON

  aws cloudformation create-stack \
    --region us-west-2 \
    --stack-name $STACK_NAME_DB  \
    --capabilities "CAPABILITY_IAM" \
    --template-body "file://~/git/service-catalog/portfolios/kfs/templates/kfs_rds_instance.yaml" \
    --role-arn $ROLE_ARN \
    --on-failure DO_NOTHING \
    --parameters "${JSON_STRING}" \
    --tags \
        Key=service,Value="UAccess Financials" \
        Key=Name,Value="$ENV_SLUG-CloudFormation-stack" \
        Key=environment,Value="$ENV_SLUG" \
        Key=createdby,Value="$TAG_CREATEDBY" \
        Key=contactnetid,Value="fischerm" \
        Key=accountnumber,Value="UAccess Financials" \
        Key=subaccount,Value="UAccess Financials" \
        Key=resourcefunction,Value="KFS 7 $ENV_SLUG Application Stack" \

  if [ "$?" != "0" ]; then
    error_abort "Error creating DB ${STACK_NAME_DB}"
  fi

  #--Wait for DB to be built
  cnt=0
  db_built="N"
  while [ $cnt -lt 6 ]
  do
    OUTPUT=$(aws cloudformation wait stack-create-complete \
      --region us-west-2 \
      --stack-name "$STACK_NAME_DB")

    if [ "$?" = "0" ]; then
      db_built="Y"
      break
    fi
    cnt=$(( cnt+1 ))
  done

  if [ "$db_built" != "Y" ]; then
    error_abort "Error waiting to create DB ${OUTPUT}"
  fi

  # -- Run Flowdown Logic if this a refresh
  # -- If the UPPER_ENV and UPPER_SOURCE_ENV do not match then we need to run flowdown scripts
  if [ "$UPPER_SOURCE_ENV" != "$UPPER_ENV" ]; then

    # -- Run the general post refresh script to populate DB users and DB Links
    # -- run pre-refresh process on targert DB before deleting CloudFormation Delete
    # -- this will retain important info for environment refreshes
    OUTPUT=$(sqlplus -s /@AWSPSTLS << EOF-SQL
    begin
      aws_refresh.post_refresh('${UPPER_PILLAR}','${UPPER_ENV}','N','N');
    end;
    /
    exit
EOF-SQL
            )

    # -- if the there was an error exit with error
    # -- this will return 0 if there is an ORA- error and 1 if there is not
    echo $OUTPUT | grep ORA- >> /dev/null
    if [ "$?" == "0" ]; then
      error_abort "Error running aws_refesh.post_refresh-($OUTPUT)"
    fi

    # -- This assumes that a local repo already exists on the server running this
    # -- an attempt to perform a pull from that repo in the master branch will be done first
    REPO="kfs-db-flowdown"
    MASTER="master"
    GIT_HOME="/home/ec2-user/git/${REPO}"
    OUTPUT=$(git -C $GIT_HOME checkout $MASTER 2>&1)
    OUTPUT=$(git -C $GIT_HOME pull 2>&1)

    # -- If this is sup then run the following scripts
    # -- GIT_HOME/scripts/kfs7-KFS-sup-flowdown.sql
    # -- GIT_HOME/scripts/rice2.5-sup-env-flowdown.sql
    if [ "$UPPER_ENV" = "SUP" ]; then
      OUTPUT=$(sqlplus -s /@AWSKFSUP << EOF-SQL
               @${GIT_HOME}/scripts/kfs7-KFS-sup-flowdown.sql
               @${GIT_HOME}/scripts/rice2.5-sup-env-flowdown.sql
               COMMIT;
               EXIT
EOF-SQL
              )

      # -- If there is an error running the scripts
      echo $OUTPUT | grep ORA- >> /dev/null
      if [ "$?" = "0" ]; then
        #In the future add logic to post to a Slack Channel
        echo "Error running flowdown ${OUTPUT}"
      fi
    fi
  fi

fi

# -- create all the other applciation components outside the DB
read -d '' JSON_STRING <<ENDJSON
[
  { "ParameterKey": "DeploymentRoute53RegistererAccessKeyID",   "ParameterValue": "$DEPLOYMENT_ROUTE53_REGISTER_ID" },
  { "ParameterKey": "DeploymentRoute53RegistererAccessKey",   "ParameterValue": "$DEPLOYMENT_ROUTE53_REGISTER_KEY" },
  { "ParameterKey": "HostnameA",   "ParameterValue": "${ENV_SLUG}1" },
  { "ParameterKey": "HostnameB",   "ParameterValue": "${ENV_SLUG}2" },
  { "ParameterKey": "HostnameC",   "ParameterValue": "${ENV_SLUG}3" },
  { "ParameterKey": "HostnameD",   "ParameterValue": "${ENV_SLUG}4" },
  { "ParameterKey": "HostedZoneId",   "ParameterValue": "$HOSTED_ZONE_ID" },
  { "ParameterKey": "EnvAppName",   "ParameterValue": "KFS" },
  { "ParameterKey": "AppSlug",      "ParameterValue": "kfs7" },
  { "ParameterKey": "EnvSlug",      "ParameterValue": "$ENV_SLUG" },
  { "ParameterKey": "VPCID",        "ParameterValue": "$VPC_ID" },
  { "ParameterKey": "AppSubnetA",   "ParameterValue": "$PRIVATE_SUBNET_A" },
  { "ParameterKey": "AppSubnetB",   "ParameterValue": "$PRIVATE_SUBNET_B" },
  { "ParameterKey": "HostedZoneName", "ParameterValue": "$HOSTED_ZONE_NAME" },
  { "ParameterKey": "SSLCertARN",  "ParameterValue": "$SSL_CERT_ARN" },
  { "ParameterKey": "LBSubnetA",    "ParameterValue": "$PRIVATE_SUBNET_A" },
  { "ParameterKey": "LBSubnetB",    "ParameterValue": "$PRIVATE_SUBNET_B" },
  { "ParameterKey": "KeyName",      "ParameterValue": "$EC2_KEYPAIR_NAME" },
  { "ParameterKey": "CustomCookbooksSource",  "ParameterValue": "$CUSTOM_COOKBOOKS_S3_BUCKET" },
  { "ParameterKey": "KFSInstanceType", "ParameterValue": "m5.xlarge" },
  { "ParameterKey": "RiceInstanceType", "ParameterValue": "c5.2xlarge" },
  { "ParameterKey": "AppEFSMountInfo", "ParameterValue": "$EFS_MOUNT_INFO" },
  { "ParameterKey": "EFSTargetSecurityGroup", "ParameterValue": "$EFS_TARGET_SECURITY_GROUP" },
  { "ParameterKey": "AppDockerImage", "ParameterValue": "760232551367.dkr.ecr.us-west-2.amazonaws.com/kuali/kfs7:${KFS_DOCKER_TAG_NAME}" },
  { "ParameterKey": "RhubarbDockerImage", "ParameterValue": "760232551367.dkr.ecr.us-west-2.amazonaws.com/kuali/rhubarb:${RHUBARB_DOCKER_TAG_NAME}" },
  { "ParameterKey": "AppDockerAccessKey", "ParameterValue": "$DOCKER_ACCESS_KEY" },
  { "ParameterKey": "AppDockerSecretKey", "ParameterValue": "$DOCKER_SECRET_KEY" },
  { "ParameterKey": "AppBaseURL",   "ParameterValue": "$APP_BASE_URL" },
  { "ParameterKey": "AppDBHostname", "ParameterValue": "$DB_HOSTNAME" },
  { "ParameterKey": "AppDBServicename", "ParameterValue": "$DB_SERVICE_NAME" },
  { "ParameterKey": "AppDBUsername",  "ParameterValue": "$DB_USERNAME" },
  { "ParameterKey": "AppDBPassword",  "ParameterValue": "$DB_PASSWORD" },
  { "ParameterKey": "AppKeystorePassword", "ParameterValue": "$RICE_KEYSTORE_PW" },
  { "ParameterKey": "AppKeystoreEncryptionKey",  "ParameterValue": "$ENCRYPTION_KEY" },
  { "ParameterKey": "AppB2BPurchaseOrderUsername", "ParameterValue": "$B2B_PO_USERNAME" },
  { "ParameterKey": "AppB2BPurchaseOrderPassword", "ParameterValue": "$B2B_PO_PASSWORD" },
  { "ParameterKey": "AppB2BShoppingUsername",  "ParameterValue": "$B2B_SHOP_USERNAME" },
  { "ParameterKey": "AppB2BShoppingPassword",  "ParameterValue": "$B2B_SHOP_PASSWORD" },
  { "ParameterKey": "AppB2BSelectSiteHostname",  "ParameterValue": "$B2B_SITE" },
  { "ParameterKey": "AppLdapUsername",  "ParameterValue": "$EDS_USERNAME" },
  { "ParameterKey": "AppLdapPassword",  "ParameterValue": "$EDS_PASSWORD" },
  { "ParameterKey": "AppLdapHostname",  "ParameterValue": "$EDS_HOST" },
  { "ParameterKey": "AppNewRelicKey",  "ParameterValue": "$NEW_RELIC_LICENSE_KEY" },
  { "ParameterKey": "AppS3ResourceBucket",  "ParameterValue": "$S3_RESOURCE_BUCKET" },
  { "ParameterKey": "RiceDockerImage", "ParameterValue": "760232551367.dkr.ecr.us-west-2.amazonaws.com/kuali/rice:${RICE_DOCKER_TAG_NAME}" },
  { "ParameterKey": "RiceDBUsername",  "ParameterValue": "rice" },
  { "ParameterKey": "RiceDBPassword",  "ParameterValue": "$RICE_DB_PASSWORD" },
  { "ParameterKey": "RiceBaseURL",   "ParameterValue": "$RICE_BASE_URL" },
  { "ParameterKey": "TagService",     "ParameterValue": "UAccess Financials" },
  { "ParameterKey": "TagName",     "ParameterValue": "kfs7-$ENV_SLUG" },
  { "ParameterKey": "TagEnvironment",     "ParameterValue": "$ENV_SLUG" },
  { "ParameterKey": "TagCreatedBy",     "ParameterValue": "$TAG_CREATEDBY" },
  { "ParameterKey": "TagContactNetId",     "ParameterValue": "fischerm" },
  { "ParameterKey": "TagAccountNumber",     "ParameterValue": "UAccess Financials" },
  { "ParameterKey": "TagSubAccount",     "ParameterValue": "UAccess Financials" },
  { "ParameterKey": "TagResourceFunction",     "ParameterValue": "KFS 7 $ENV_SLUG Application Stack" },
  { "ParameterKey": "TagTicketNumber",     "ParameterValue": "UAF-3905" },
  { "ParameterKey": "AppSMTPUsername",  "ParameterValue": "$SMTP_USERNAME" },
  { "ParameterKey": "AppSMTPPassword",  "ParameterValue": "$SMTP_PASSWORD" },
  { "ParameterKey": "EmailForSNSSubscription",  "ParameterValue": "$EMAIL_FOR_SNS" }
]
ENDJSON

#template body has breached the AWS limit of 50kb to use as a file from
# the cli, so need to push to S3
aws s3 cp ~/git/service-catalog/portfolios/kfs/templates/kfs7_opsworks.yaml s3://$S3_RESOURCE_BUCKET/kfs7_opsworks.yaml

aws cloudformation create-stack \
    --region us-west-2 \
    --stack-name $STACK_NAME  \
    --capabilities "CAPABILITY_IAM" \
    --template-url "https://s3-us-west-2.amazonaws.com/$S3_RESOURCE_BUCKET/kfs7_opsworks.yaml" \
    --role-arn $ROLE_ARN \
    --on-failure DO_NOTHING \
    --parameters "${JSON_STRING}" \
    --tags \
        Key=service,Value="UAccess Financials" \
        Key=resourcename,Value="$ENV_SLUG-CloudFormation-stack" \
        Key=environment,Value="$ENV_SLUG" \
        Key=createdby,Value="$TAG_CREATEDBY" \
        Key=contactnetid,Value="fischerm" \
        Key=accountnumber,Value="UAccess Financials" \
        Key=subaccount,Value="UAccess Financials" \
        Key=resourcefunction,Value="KFS 7 $ENV_SLUG Application Stack" \

aws cloudformation wait stack-create-complete \
    --region us-west-2 \
    --stack-name "$STACK_NAME"
}


#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
# -- set environment
#set_environment
START_TS=`${DATE}`

# -- parse command-line options
while getopts :p:e:s:Ddm: arguments
do
  case $arguments in
    p)
       UPPER_PILLAR=${OPTARG^^}
       ;;
    e)
       UPPER_ENV=${OPTARG^^}
       ;;
    s)
       UPPER_SOURCE_ENV=${OPTARG^^}
       ;;
    d)
       DESTROY_IF_EXISTS="Y"
       DESTROY_ONLY="N"
       ;;
    D)
       DESTROY_IF_EXISTS="Y"
       DESTROY_ONLY="Y"
       ;;
    m)
       MODE=${OPTARG^^}
       ;;
   \?)
       print_usage

       echo
       error_abort "option -${OPTARG} is not a valid argument"
       exit 255
  esac
done

# validate input parameters
validate_arguments

# -- if we reach this point we can build the environment
build_environment

