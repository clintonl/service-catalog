#!/bin/bash
 
#TODO Populate this value with production account information
# Enter the CloudFormation stack name of the Route 53 hosted zone for KFS
ROUTE_53_STACK_NAME="kuali-prod-route53"
# ARN of the Foundational CloudFormation deployer role to use
ROLE_ARN="arn:aws:iam::740525297805:role/fdn-CloudFormationAdminDeployer"
 
#TODO Enter deployer's NetID here
TAG_CREATED_BY="kellehs"
 
#TODO Modify region if necessary
REGION="us-west-2"
 
# These values do not need to be modified
STACK_NAME="kfs-route53-self-registerer-user"
REQUIRED_STACK_PREFIX="kfs"
SERVICE_NAME="UAccess Financials"
SERVICE_SLUG=$STACK_NAME
TAG_SERVICE=$SERVICE_NAME
TAG_NAME=$STACK_NAME
TAG_ENVIRONMENT="prd"
TAG_CONTACT_NETID="quikkian"
TAG_ACCOUNT_NUMBER=$SERVICE_NAME
TAG_SUB_ACCOUNT=$SERVICE_NAME
TAG_TICKET_NUMBER="UAFAWS-238"
TAG_RESOURCE_FUNCTION="KFS Route 53 Self-Registerer User"
 
read -d '' JSON_STRING <<ENDJSON
[
  { "ParameterKey": "Route53StackName", "ParameterValue": "$ROUTE_53_STACK_NAME" },
  { "ParameterKey": "StackPrefix", "ParameterValue": "$REQUIRED_STACK_PREFIX" }
]
ENDJSON
 
aws cloudformation create-stack \
    --region $REGION \
    --stack-name $STACK_NAME  \
    --capabilities "CAPABILITY_IAM" \
    --template-body "file://~/git/service-catalog/portfolios/kfs/templates/kfs_route53_register_user.yaml" \
    --role-arn $ROLE_ARN \
    --on-failure DO_NOTHING \
    --parameters "${JSON_STRING}" \
    --tags \
        Key=service,Value="$TAG_SERVICE" \
        Key=Name,Value="$STACK_NAME-CloudFormation-stack" \
        Key=environment,Value="$TAG_ENVIRONMENT" \
        Key=createdby,Value="$TAG_CREATED_BY" \
        Key=contactnetid,Value="$TAG_CONTACT_NETID" \
        Key=accountnumber,Value="$TAG_ACCOUNT_NUMBER" \
        Key=subaccount,Value="$TAG_SUB_ACCOUNT" \
        Key=resourcefunction,Value="$TAG_RESOURCE_FUNCTION CloudFormation Stack"
                   
aws cloudformation wait stack-create-complete \
    --region $REGION \
    --stack-name $STACK_NAME
