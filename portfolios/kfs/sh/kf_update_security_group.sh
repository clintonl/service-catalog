#!/bin/bash
# -- Get Account Name
AWS_ACCOUNT=`aws iam list-account-aliases --output text | awk '{print $2}'`

case $AWS_ACCOUNT in
  ua-uits-kuali-nonprod)
    ROLE_ARN="arn:aws:iam::397167497055:role/fdn-iam-CloudFormationAdminDeployerRole-12AJX3KHD35CR"
    ;;
  ua-uits-kuali-prod)
    ROLE_ARN="arn:aws:iam::740525297805:role/fdn-CloudFormationAdminDeployer"
    ;;
  *)
    echo "Could not determine Role ARN based on the AWS Account (${AWS_ACCOUNT})"
    exit 1
    ;;
esac

aws cloudformation update-stack \
   --region us-west-2 \
   --stack-name "Kuali-DbSg"  \
   --capabilities "CAPABILITY_IAM" \
   --template-body "file://~/git/service-catalog/portfolios/kfs/templates/kfs_security_groups.yaml" \
   --role-arn "${ROLE_ARN}"
