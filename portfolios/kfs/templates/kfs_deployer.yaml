---
# App Deployer CloudFormation Deployment
# -----------------------------------------
# 
# This CloudFormation template creates an IAM user who is allowed to
# create certain CloudFormation stacks and pass a role along to run
# the actual deployment.

AWSTemplateFormatVersion: '2010-09-09'
Description: CloudFormation Deployer

# Parameters
# ----------
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:
  # BucketNamePath is the full path name to the bucket.
  DeployerRoleName:
    Type: String
    Description: Name of the role to use for deployment

  # IAMUserName is the username that will be used for this IAM user.   
  StackPrefix:
    Type: String
    Description: Required Prefix for all Stack Names. ie. "kfs", or "elm" etc.

# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Web UI.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: Configuration
      Parameters:
      - DeployerRoleName
      - StackPrefix
    ParameterLabels:
      DeployerRoleName:
        default: 'Deployment Role:'
      StackPrefix:
        default: 'Required Stack Prefix :'

# Resources
# ---------
#
# These are all of the resources deployed by this template.
#        
Resources:
  # #### DeploymentUser
  #
  # 
  DeploymentUser:
    Type: AWS::IAM::User
    Properties:
      Path: "/"

  KfsDeployerPolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties:
      ManagedPolicyName: !Sub "${StackPrefix}-KFSDeployerDeploymentAccess"
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
        - Sid: StmtAllowPassRole
          Effect: Allow
          Action:
          - iam:PassRole
          Resource:
          - !Sub "arn:aws:iam::${AWS::AccountId}:role/${DeployerRoleName}"
        - Sid: StmtCloudformation
          Effect: Allow
          Action:
          - cloudformation:Describe*
          - cloudformation:EstimateTemplateCost
          - cloudformation:GetStackPolicy
          - cloudformation:GetTemplate
          - cloudformation:GetTemplateSummary
          - cloudformation:List*
          - cloudformation:PreviewStackUpdate
          - cloudformation:ValidateTemplate
          Resource:
          - "*"
        - Sid: StmtCloudformationDelete
          Effect: Allow
          Action:
          - cloudformation:CreateStack
          - cloudformation:DeleteStack
          Resource:
          - !Sub "arn:aws:cloudformation:${AWS::Region}:${AWS::AccountId}:stack/${StackPrefix}*/*"
        - Sid: StmtAllowOpsworksDescribeInstances
          Effect: Allow
          Action:
          - opsworks:DescribeInstances
          - opsworks:DescribeLayers
          - opsworks:DescribeStacks
          - opsworks:CreateInstance
          - opsworks:StartInstance
          - opsworks:StopInstance
          - opsworks:DeleteInstance
          Resource:
          - "*"
        - Sid: StmtAllowS3PutAndGetInKfsBuckets
          Effect: Allow
          Action:
          - s3:PutObject
          - s3:GetObject
          Resource:
          - "arn:aws:s3:::kfs-cloudformation-deployment/*"
          - "arn:aws:s3:::kfs-build-artifacts/*"
        - Sid: StmtRdsDenyDeleteOnRestrictedDatabase
          Effect: Deny
          Action:
          - rds:DeleteDBInstance
          Resource:
          - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:db:kfs3imp"
          # This is because even though the database instances match the wildcard kf*,
          # the aws cli of 'describe-db-instances' attempts to describe ALL instances
          # which must be explicitly allowed (even though, in actuality, all the desired 
          # instances match kf*, the permissions do not go deep enough to see this).
        - Sid: StmtRdsDescribeDBInstancesAllowAll
          Effect: Allow
          Action:
          - rds:DescribeDBInstances
          Resource:
          - "*"
        - Sid: StmtRdsAllowForKFInstances
          Effect: Allow
          Action:
          - rds:DescribeDBSnapshots
          - rds:RestoreDBInstanceFromDBSnapshot
          - rds:ModifyDBInstance
          - rds:CreateDBSnapshot
          - rds:DeleteDBInstance
          - rds:AddTagsToResource
          - rds:RebootDBInstance
          - rds:ListTagsForResource
          - rds:StopDBInstance
          - rds:StartDBInstance
          Resource:
          - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:db:kf*"
          - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:og:ua-oracle-ee-12-1"
          - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:pg:kuali-oracle-12-1"
          - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:snapshot:kf*"
          - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:snapshot:rds:kf*"
          - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:subgrp:rds-private-subnet-group"
        - Sid: StmtAllowRdsSnapshotDeletesForTstToDevRestoreCleanup
          Effect: Allow
          Action:
          - rds:DeleteDBSnapshot
          Resource:
          - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:snapshot:kfs3tst-restore-snapshot-*"
          - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:snapshot:kfs6tst-restore-snapshot-*"
          - !Sub "arn:aws:rds:${AWS::Region}:${AWS::AccountId}:snapshot:kfs7tst-restore-snapshot-*"
      Users:
        - !Ref DeploymentUser     
# Outputs
# ---------
#
# Output values that can be viewed from the AWS CloudFormation console.
# 
Outputs:
  DeploymentUser:
    Value: !Ref DeploymentUser

