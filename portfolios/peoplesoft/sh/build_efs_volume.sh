#              -D  Destroy the CloudFormation Template first, if it exists
#!/bin/bash
#*******************************************************************************
#
# TITLE......: build_efs_volume.sh
# PARAMETER..:
#    /INPUTS.:
#              -p  Pillar (HR,EL,SA)
#              -u  Update CF (don't create)
#
#*******************************************************************************

#*******************************************************************************
#
# function: validate_arguments
#
# This function parses the arguments from the command-line flags and makes sure
# that all necessary and required parameters have been passed to the program.
# If it encounters an issue, it aborts the program.
#
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function validate_arguments
{
# -- Get Account Name
AWS_ACCOUNT=`aws iam list-account-aliases|awk '{print $2}'`

# -- make sure pillar is specified and is either EL, HR, or SA
pillar_list="EL HR SA"
good_pillar="N"
for curr_pillar in $pillar_list; do
  if [ "${UPPER_PILLAR}" == "$curr_pillar" ]; then
    good_pillar="Y"
  fi
done

if [ $good_pillar = "N" ]; then
  error_abort "Either a pillar wasn't passed or was invalid please use -p with the values EL, HR, or SA (${UPPER_PILLAR})" 
fi 
LOWER_PILLAR=${UPPER_PILLAR,,}

#  -- create Service Tag based on pillar
case ${UPPER_PILLAR} in
  EL)
    TAG_SERVICE="Uaccess Learning"
    ;;
  HR)
    TAG_SERVICE="Uaccess Employee"
    ;;
  SA)
    TAG_SERVICE="Uaccess Student"
    ;;
  *)
    error_abort "Could not determine Service Tag based on the pillar passed (${UPPER_PILLAR})"
    ;;
esac

# -- Various other tag elements
TAG_TICKET_NUMBER="CLOUD-15"
TAG_RESOURCE_FUNCTION="EFS Mount for ${UPPER_PILLAR}"

# -- accountnumber and subaccount tags will be based on Pillar
case ${UPPER_PILLAR} in
  EL)
    TAG_ACCOUNT_NUMBER="Human Resources Systems"
    TAG_SUBACCOUNT="Uaccess Learning"

    ;;
  HR)
    TAG_ACCOUNT_NUMBER="Human Resources Systems"
    TAG_SUBACCOUNT="Uaccess Employee"
    ;;
  SA)
    TAG_ACCOUNT_NUMBER="Student Systems"
    TAG_SUBACCOUNT="Uaccess Student"
    ;;
  *)
    error_abort "Could not determine Account and Subaccount Tag based on the pillar passed (${UPPER_PILLAR})"
    ;;
esac

# -- this the NetID of the person most familiar with the resources being created, will default to kellehs for now
TAG_CONTACT_NETID="kellehs"

# -- CloudFormation Stack Name will be PeopleSoft+UPPER_PILLAR+UPPER_ENV
CF_STACK_NAME="PeopleSoft${UPPER_PILLAR}-EFS"

# -- Role ARN will be different based on what account we are in
# -- Eventually we need to query this from AWS instead of hard coding
# -- The CF Template is bigger than 51,200 characters so we need to call it from
# -- an S3 bucket.  The template URL variable will hold the location of the S3 bucket
case ${AWS_ACCOUNT} in
  ua-uits-peoplesoft-nonprod)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    #TEMPLATE_URL='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    ;;
  ua-uits-peoplesoft-prod)
    ROLE_ARN="arn:aws:iam::242275737326:role/fdn-iam-CloudFormationAdminDeployerRole-IP2KC5OSU7DS"
    #TEMPLATE_URL='http://peoplesoft-prod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    ;;
  *)
    error_abort "Could not determine Role ARN based on the account (${AWS_ACCOUNT})"
    ;;
esac

}

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage
{
echo "Usage: build_efs_volume.sh"
echo "Options:"
echo "       -p (required) = Pillar valid values are HR, SA, EL"
echo "       -u (optional) = Update CloudFormation Stack"
}

#*******************************************************************************
#
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
typeset MSG=$1

echo
echo "ERROR: ${MSG}"
echo "aborting . . ."
echo
exit 255
}

#*******************************************************************************
#
# function: check_if_env_exists
#
# This funtion will check if the CloudFromation Tempalte exists.  If so, it will
# evaluate the DESTROY_IF_EXISTS vairable.  If Y(es) then the CloudFormation 
# Template will be destroyed, if N(o) then the program will stop with an error
# stating that the environment is already running.
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function check_if_env_exists
{
#-- describe the CloudFormation Template stored in CF_STACK_NAME
# -- if the snapshot file was not populated with data then we have an issue and have to abort
# -- look for the strin arn:aws:cloudformation in the output, if it's there the environment exists
aws cloudformation describe-stacks --stack-name ${CF_STACK_NAME} 2>&1 |grep -q "arn:aws:cloudformation" &> /dev/null

# -- if the grep finds the string it will return a 0, otherwise it will return a 1
if [ $? == 0 ]; then 
  ENV_EXISTS="Y"
else  
  ENV_EXISTS="N"
fi

# -- if the env exists check to see if this is an update
# -- if not updating then through an error and exit
if [ "${ENV_EXISTS}" == "Y" ]; then
  if [ "${UPDATE_CF}" == "N" ]; then
    print_usage
    echo
    error_abort "The CloudFormation Template ${CF_STACK_NAME} already exists. Pass a -u if you'd like to update it."
  fi
fi

# -- if the env does not exist check to see if this is an update
# -- if updating then throw an error and exit
if [ "${ENV_EXISTS}" == "N" ]; then
  if [ "${UPDATE_CF}" == "Y" ]; then
    print_usage
    echo
    error_abort "The CloudFormation Template ${CF_STACK_NAME} does not exist and a -u was passed."
  fi
fi

}

#*******************************************************************************
#
# function: build_environment
#
# This function will build the environment. 
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function build_environment
{

read -d '' JSON_STRING <<ENDJSON
[
 { "ParameterKey": "EnvAppName","ParameterValue": "${TAG_SERVICE}"}, 
 { "ParameterKey": "AppSlug","ParameterValue": "${LOWER_PILLAR}" }, 
 { "ParameterKey": "TagService","ParameterValue": "${TAG_SERVICE}" },
 { "ParameterKey": "TagContactNetId","ParameterValue": "${TAG_CONTACT_NETID}" },
 { "ParameterKey": "TagAccountNumber","ParameterValue": "${TAG_ACCOUNT_NUMBER}" },
 { "ParameterKey": "TagSubAccount","ParameterValue": "${TAG_SUBACCOUNT}" },
 { "ParameterKey": "TagTicketNumber","ParameterValue": "${TAG_TICKET_NUMBER}" },
 { "ParameterKey": "TagResourceFunction","ParameterValue": "${TAG_RESOURCE_FUNCTION}" }
]
ENDJSON

if [ "${UPDATE_CF}" == "N" ]; then

  aws cloudformation create-stack \
    --region us-west-2 \
    --stack-name "${CF_STACK_NAME}"  \
    --capabilities "CAPABILITY_IAM" \
    --template-body "file://~/git/service-catalog/portfolios/peoplesoft/templates/efs_volume.yaml" \
    --parameters "${JSON_STRING}" \
    --role-arn "${ROLE_ARN}" \
    --disable-rollback

else

  aws cloudformation update-stack \
    --region us-west-2 \
    --stack-name "${CF_STACK_NAME}"  \
    --capabilities "CAPABILITY_IAM" \
    --template-body "file://~/git/service-catalog/portfolios/peoplesoft/templates/efs_volume.yaml" \
    --parameters "${JSON_STRING}" \
    --role-arn "${ROLE_ARN}"
fi
}

#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
# -- set environment
#set_environment
START_TS=`${DATE}`
UPDATE_CF="N"

# -- parse command-line options
while getopts :p:u arguments
do
  case $arguments in
    p)
       UPPER_PILLAR=${OPTARG^^}
       ;;
    u)
       UPDATE_CF="Y"
       ;;
   \?)
       print_usage
       echo
       error_abort "option -${OPTARG} is not a valid argument"
       exit 255
  esac
done

# validate input parameters
validate_arguments


# -- check to see if the environment is running
# -- if so if the DESTROY_IF_EXISTS = Y then destroy the environment first
# -- if not, then exit program with error
check_if_env_exists

# -- if we reach this point we can build the environment
build_environment
