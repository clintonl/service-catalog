#!/bin/bash
#*******************************************************************************
#
# TITLE......: build_ps_env.sh
# PARAMETER..:
#    /INPUTS.:
#              -p  Pillar (HR,EL,SA) (req)
#              -e  Environment (DEV, TST, STG, etc) (req)
#              -s  Source Environment 
#              -d  Destroy the CloudFormation Template first, if it exists and rebuild
#              -D  Destroy the CloudFormation Template first, if it exists and stop
#              -z  Set up like production
#              -Z  Beef up non production environment (a bigger env for major testing)
#              -o  Obscure data if this is a refresh
#              -P  Point in time refresh (if left blank the --use-latest-restorable-time will be used)
#                  Example format: 2017-11-26T18:56:40MST (YYYY-MM-DDTHH:MI:SSZONE)
#                  Bash date format date +"%Y-%m-%dT%H:%M:%S%Z" - 2017-11-26T18:56:40MST
#              -m  Mode - Use "a" to just apply this to the app/web layer and leave the DB alone
#
#*******************************************************************************

#*******************************************************************************
#
# function: validate_arguments
#
# This function parses the arguments from the command-line flags and makes sure
# that all necessary and required parameters have been passed to the program.
# If it encounters an issue, it aborts the program.
#
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function validate_arguments
{
# -- make sure pillar is specified and is either EL, HR, or SA
pillar_list="EL HR SA"
good_pillar="N"
for curr_pillar in $pillar_list; do
  if [ "${UPPER_PILLAR}" == "$curr_pillar" ]; then
    good_pillar="Y"
  fi
done

if [ $good_pillar = "N" ]; then
  error_abort "Either a pillar wasn't passed or was invalid please use -p with the values EL, HR, or SA (${UPPER_PILLAR})" 
fi 
LOWER_PILLAR=${UPPER_PILLAR,,}

#Make sure SET_UP_LIKE_PRD is set
if [ "${UPPER_ENV}" == "PRD" ]; then
  SET_UP_LIKE_PRD="Y"
fi

# -- Set the Slack Channel name to send refresh messages
REFRESH_SLACK_CHANNEL="${LOWER_PILLAR}-refresh"

#  -- create Service Tag based on pillar
case ${UPPER_PILLAR} in
  EL)
    TAG_SERVICE="Uaccess Learning"
    ;;
  HR)
    TAG_SERVICE="Uaccess Employee"
    ;;
  SA)
    TAG_SERVICE="Uaccess Student"
    ;;
  *)
    error_abort "Could not determine Service Tag based on the pillar passed (${UPPER_PILLAR})"
    ;;
esac

# -- accountnumber and subaccount tags will be based on Pillar
case ${UPPER_PILLAR} in
  EL)
    TAG_ACCOUNT_NUMBER="1192621"
    TAG_SUBACCOUNT="Uaccess Learning"
    ;;
  HR)
    TAG_ACCOUNT_NUMBER="1192621"
    TAG_SUBACCOUNT="Uaccess Employee"
    ;;
  SA)
    TAG_ACCOUNT_NUMBER="Student Systems"
    TAG_SUBACCOUNT="Uaccess Student"
    ;;
  *)
    error_abort "Could not determine Account and Subaccount Tag based on the pillar passed (${UPPER_PILLAR})"
    ;;
esac

# -- this the NetID of the person most familiar with the resources being created, will default to kellehs for now
TAG_CONTACT_NETID="kellehs"

# -- make sure an Environment was passed
if [ "${UPPER_ENV}" = "" ]; then
  error_abort "An environmnet was not passed, use -e and include the enviornment name i.e. DEV, TST, STG etc."
fi 
LOWER_ENV=${UPPER_ENV,,}

# -- if the source environment was not passed then default to the target environment
if [ -z ${UPPER_SOURCE_ENV} ]; then
  UPPER_SOURCE_ENV=${UPPER_ENV}
fi
LOWER_SOURCE_ENV=${UPPER_SOURCE_ENV,,}

# -- Validate MODE, if one was not passed then default to B for Both
if [ "$MODE" = "" ]; then
  MODE="B"
fi

if [ "$MODE" != "A" ] && [ "$MODE" != "B" ]; then
  error_abort "Mode passed it not valid.  Must be A for just the Appliation or B for Both App and DB ($MODE)"
fi


# -- if the Env and Source Env do not match then we need to set the RUN_POST_REFRESH flag to Y
# -- Do not run post_refresh scripts for PRD environments
# -- otherwise it will be set to N
if [ "${UPPER_ENV}" == "${UPPER_SOURCE_ENV}" ] || [ "${UPPER_ENV}" == "PRD" ]; then
  RUN_POST_REFRESH="N"
  #Also set the Point in Time Refresh flag to N as well.  Point in Time refreshes only make sense
  #when the source and target are different
  PIT_REFRESH="N"
else
  RUN_POST_REFRESH="Y"
fi

# -- Set SOURCE and TARGET DB Instance Variables
SOURCE_DB_INSTANCE="peoplesoft-${LOWER_PILLAR}${LOWER_SOURCE_ENV}"
TARGET_DB_INSTANCE="peoplesoft-${LOWER_PILLAR}${LOWER_ENV}"

# -- default DESTROY_IF_EXISTS to N if it was not passed via the -D argument
# -- this parameter will be used to determine if we will destroy the environmnet
# -- before we build it.  This will be the case if we are refreshing an environment.
if [ -z ${DESTROY_IF_EXISTS} ]; then
  DESTROY_IF_EXISTS="N"
fi

# -- CloudFormation Stack Name will be PeopleSoft+UPPER_PILLAR+UPPER_ENV
CF_STACK_NAME="PeopleSoft${UPPER_PILLAR}${UPPER_ENV}-ECS"
CF_STACK_NAME_DB="PeopleSoft${UPPER_PILLAR}${UPPER_ENV}-DB"

# -- Role ARN will be different based on what account we are in
# -- Eventually we need to query this from AWS instead of hard coding
# -- The CF Template is bigger than 51,200 characters so we need to call it from
# -- an S3 bucket.  The template URL variable will hold the location of the S3 bucket
case ${UPPER_ENV} in
  DMO)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    TEMPLATE_URL='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'
    ;;
  DMA)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    TEMPLATE_URL='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'
    ;;
  DMB)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    TEMPLATE_URL='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'
    ;;
  DEV)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    TEMPLATE_URL='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'
    ;;
  TST)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    TEMPLATE_URL='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'
    ;;
  TEA)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    TEMPLATE_URL='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'
    ;;
  TEB)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    TEMPLATE_URL='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'
    ;;
  TEC)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    TEMPLATE_URL='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'
    ;;
  TRN)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    TEMPLATE_URL='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'
    ;;
  STG)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    TEMPLATE_URL='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-nonprod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'
    ;;
  SUP)
    ROLE_ARN="arn:aws:iam::242275737326:role/fdn-iam-CloudFormationAdminDeployerRole-IP2KC5OSU7DS"
    TEMPLATE_URL='http://peoplesoft-prod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-prod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'
    ;;
  RPT)
    ROLE_ARN="arn:aws:iam::242275737326:role/fdn-iam-CloudFormationAdminDeployerRole-IP2KC5OSU7DS"
    TEMPLATE_URL='http://peoplesoft-prod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-prod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'
    ;;
  PRD)
    ROLE_ARN="arn:aws:iam::242275737326:role/fdn-iam-CloudFormationAdminDeployerRole-IP2KC5OSU7DS"
    TEMPLATE_URL='http://peoplesoft-prod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs.yaml'
    TEMPLATE_URL_DB='http://peoplesoft-prod-s3-arizona-edu.s3.amazonaws.com/cf_templates/ps_env_ecs_db.yaml'
    ;;
  *)
    error_abort "Could not determine App Bit Bucket Branch based on the target environment passed (${UPPER_ENV})"
    ;;
esac

# -- Before checking on the environment check to see if this is a Point in Time refresh
# -- At this point we need to check to ensure if a date was passed that it follows a proper date format
if [ "$PIT_REFRESH" = "Y" ]; then
  #If the PIT_DATE is not blank then validate the date format
  if ! [ -z $PIT_DATE ]; then
    date +"$PIT_FORMAT" -d "$PIT_DATE" 2>&1 > /dev/null
    if ! [ "$?" = "0" ]; then
      error_abort "Point in Time refresh date format invalid should look like 2017-11-26T18:56:40MST ($PIT_DATE)"
    fi
  fi
fi


# -- check to see if the environment is running
# -- if so if the DESTROY_IF_EXISTS = Y then destroy the environment first
# -- if not, then exit program with error
check_if_env_exists

#######################
#######################
# -- make sure the Pillar/Environment exists
# -- find the most recent RDS snapshot for the Pillar/Source Env passed
# -- find latest snapshot, this will return a snapshot name, we will test to see if that snapshot exists
# -- in the next step
DB_LATEST_SNAPSHOT=`aws rds describe-db-snapshots --db-instance-identifier "${SOURCE_DB_INSTANCE}" --output text 2>&1 | sort -k10 -r | awk "NR==1{print}" | awk '{print $6}'`

# -- if the variable was not populated then default it to something
if [ -z ${DB_LATEST_SNAPSHOT} ]; then
  DB_LATEST_SNAPSHOT="~~none~~"
fi

# -- now check to see that the DB_LATEST_SNAPSHOT actually exists
aws rds describe-db-snapshots --db-instance-identifier "${SOURCE_DB_INSTANCE}" --output text 2>&1 | grep ${DB_LATEST_SNAPSHOT} &> /dev/null
if ! [ "$?" == "0" ]; then
  error_abort "An RDS Snapshot does not exist for the DB Instance ${SOURCE_DB_INSTANCE}"
fi

# -- HostedZoneName will be ps-nonprod-aws.arizona.edu for non PRD and non SUP environments
# -- HostedZoneName will be ps-prod-aws.arizona.edu for PRD and SUP
# -- May want to see if we can describe this from a CLI command
# -- First default the Fully Qualified Domain Name prefix to pillar-env (el-dev)
FQDN_PREFIX="${LOWER_PILLAR}-${LOWER_ENV}"
PUB_FQDN_PREFIX="${LOWER_PILLAR}-${LOWER_ENV}"
case ${UPPER_ENV} in
  DMO)
    HOSTED_ZONE_NAME="ps-nonprod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-nonprod-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="mosaic.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="ps-pub-nonprod-aws.arizona.edu"
    ;;
  DMA)
    HOSTED_ZONE_NAME="ps-nonprod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-nonprod-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="mosaic.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="ps-pub-nonprod-aws.arizona.edu"
    ;;
  DMB)
    HOSTED_ZONE_NAME="ps-nonprod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-nonprod-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="mosaic.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="ps-pub-nonprod-aws.arizona.edu"
    ;;
  DEV)
    HOSTED_ZONE_NAME="ps-nonprod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-nonprod-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="mosaic.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="ps-pub-nonprod-aws.arizona.edu"
    ;;
  TRN)
    HOSTED_ZONE_NAME="ps-nonprod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-nonprod-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="mosaic.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="ps-pub-nonprod-aws.arizona.edu"
    if [ "${UPPER_PILLAR}" == "SA" ]; then
      AUTH_TOKEN_DOMAIN="ps-nonprod-aws.arizona.edu"
      PUB_AUTH_TOKEN_DOMAIN="ps-pub-nonprod-aws.arizona.edu"
    else
      AUTH_TOKEN_DOMAIN="mosaic.arizona.edu"
      PUB_AUTH_TOKEN_DOMAIN="pub.mosaic.arizona.edu"
    fi
    ;;
  TST)
    HOSTED_ZONE_NAME="ps-nonprod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-nonprod-aws.arizona.edu"
    if [ "${UPPER_PILLAR}" == "SA" ]; then
      AUTH_TOKEN_DOMAIN="ps-nonprod-aws.arizona.edu"
      PUB_AUTH_TOKEN_DOMAIN="ps-pub-nonprod-aws.arizona.edu"
    else
      AUTH_TOKEN_DOMAIN="mosaic.arizona.edu"
      PUB_AUTH_TOKEN_DOMAIN="pub.mosaic.arizona.edu"
    fi
    ;;
  TEA)
    HOSTED_ZONE_NAME="ps-nonprod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-nonprod-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="mosaic.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="ps-pub-nonprod-aws.arizona.edu"
    ;;
  TEB)
    HOSTED_ZONE_NAME="ps-nonprod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-nonprod-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="mosaic.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="ps-pub-nonprod-aws.arizona.edu"
    ;;
  TEC)
    HOSTED_ZONE_NAME="ps-nonprod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-nonprod-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="mosaic.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="ps-pub-nonprod-aws.arizona.edu"
    ;;
  STG)
    HOSTED_ZONE_NAME="ps-nonprod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-nonprod-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="mosaic.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="ps-pub-nonprod-aws.arizona.edu"
    ;;
  SUP)
    HOSTED_ZONE_NAME="ps-prod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-prod-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="uaccess.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="schedule.arizona.edu"
    if [ "${UPPER_PILLAR}" == "HR" ]; then
      FQDN_PREFIX="employeesup851"
    elif [ "${UPPER_PILLAR}" == "SA" ]; then
      FQDN_PREFIX="studentsup851"
      PUB_FQDN_PREFIX="uaccesssup"
      #FQDN_PREFIX="sa-sup"
      #PUB_FQDN_PREFIX="sa-sup"
    fi
    ;;
  RPT)
    HOSTED_ZONE_NAME="ps-prod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-prod-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="uaccess.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="schedule.arizona.edu"
    if [ "${UPPER_PILLAR}" == "HR" ]; then
      FQDN_PREFIX="employeesup851"
    elif [ "${UPPER_PILLAR}" == "SA" ]; then
      FQDN_PREFIX="studentrpt"
      PUB_FQDN_PREFIX="uaccesssup"
      #FQDN_PREFIX="sa-rpt"
      #PUB_FQDN_PREFIX="sa-rpt"
    fi
    ;;
  PRD)
    HOSTED_ZONE_NAME="ps-prod-aws.arizona.edu"
    PUB_HOSTED_ZONE_NAME="ps-pub-prod-aws.arizona.edu"
    AUTH_TOKEN_DOMAIN="uaccess.arizona.edu"
    PUB_AUTH_TOKEN_DOMAIN="schedule.arizona.edu"
    if [ "${UPPER_PILLAR}" == "HR" ]; then
      FQDN_PREFIX="employee851"
    elif [ "${UPPER_PILLAR}" == "SA" ]; then
      FQDN_PREFIX="student851"
      PUB_FQDN_PREFIX='uaccess'
      #FQDN_PREFIX="sa-prd"
      #PUB_FQDN_PREFIX="sa-prd"
    else
      FQDN_PREFIX="learning"
    fi
    ;;
  *)
    error_abort "Could not determine a Hosted Zone Name based on the target environment passed (${UPPER_ENV})"
    ;;
esac

# -- Populate the variable TLS12, which will be passed via a Docker Variable into the Web and App container
# -- if set to Y then TLS1.2 parameters will be set on the Web and App otherwise they will not be set
# -- TLS is a ciper for SSL and 1.0 has to be removed, we are going to 1.2 because we are able with the 
# -- version of java we have on the web and app servers
# -- As of 09/17/2017 all non prod environments will be set to Y, production will be set to N
case ${UPPER_ENV} in
  PRD)
    #11/29/2017 changing to Y for all environments
    #TLS12="N"
    TLS12="Y"
    ;;
  *)
    TLS12="Y"
    ;;
esac

# -- Web Listener Policy Name will be ELBSecurityPolicy-2016-08 as of 01/03/2017
# -- May want to see if we can describe this from a CLI command
# -- Changed to ELBSecurityPolicy-TLS-1-2-2017-01 to we elimiate TLS1.0
# -- If TLS12 = Y then set to ELBSecurityPolicy-TLS-1-2-2017-01, otherwise set to ELBSecurityPolicy-2016-08
if [ "$TLS12" = "Y" ]; then
  WEB_ELB_LISTENER_POLICY_NAME="ELBSecurityPolicy-TLS-1-2-2017-01"
else
  WEB_ELB_LISTENER_POLICY_NAME="ELBSecurityPolicy-2016-08"
fi

# -- SSL Cert ARN will be arn:aws:iam::415418166582:server-certificate/ps-nonprod-aws.arizona.edu_2016 for nonprod
# -- and ? for prod
if [ "${HOSTED_ZONE_NAME}" == "ps-nonprod-aws.arizona.edu" ]; then
  if [ "${UPPER_PILLAR}" == "SA" ]; then
    if [ "${UPPER_ENV}" = "TST" ]; then
      #ps-nonprod-aws.arizona.edu
      SSL_CERT_ARN="arn:aws:acm:us-west-2:415418166582:certificate/fbb2267b-5d62-4061-9dff-3b9d2a3b45b3"
      #ps-pub-nonprod-aws.arizona.edu - Need to 
      PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:415418166582:certificate/6f77b8a1-60d1-4751-8c9f-80094d51cc61"
      #pub.mosaic.arizona.edu
      #PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:415418166582:certificate/a3554385-a8ed-44b0-baf8-0627193dcc4b"
    else
      #mosaic.arizona.edu
      SSL_CERT_ARN="arn:aws:acm:us-west-2:415418166582:certificate/aeb6f872-18d9-4d91-ba83-ce06e7e1f526"
      #ps-pub-nonprod-aws.arizona.edu - Need to 
      PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:415418166582:certificate/6f77b8a1-60d1-4751-8c9f-80094d51cc61"
    fi
  else
    #mosaic.arizona.edu
    SSL_CERT_ARN="arn:aws:acm:us-west-2:415418166582:certificate/aeb6f872-18d9-4d91-ba83-ce06e7e1f526"
    #Public cert will match the private cert for PILLAR <> SA
    PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:415418166582:certificate/aeb6f872-18d9-4d91-ba83-ce06e7e1f526"
  fi
elif [ "${HOSTED_ZONE_NAME}" == "ps-prod-aws.arizona.edu" ]; then
  if [ "${UPPER_PILLAR}" == "SA" ]; then
    #ps-prod-aws.arizona.edu
    #SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/18d79228-ea3b-4253-a01b-1d2dd3be7055"
    #ps-pub-prod-aws.arizona.edu
    #PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/6ca4c28f-245e-4618-983a-d5e3d276796f"

    #uaccess.arizona.edu
    SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/d1c9a073-3f2b-4b00-a06c-b037bc50083d"
    #There are two certs for the PROD account
    #uaccess.schedule.arizona.edu - is for prd
    #uaccesssup.schedule.arizona.edu -  is for sup
    if [ "${UPPER_ENV}" = "SUP" ]; then
      #uaccesssup.schedule.arizona.edu
      PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/4210d2ee-b38f-434a-be36-6a4071258fe4"
    elif [ "${UPPER_ENV}" = "PRD" ]; then
      #uaccess.schedule.arizona.edu
      PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/cd9f7a06-fa7d-4a5c-850d-24700e5c77ea"
    fi
  else
    #uaccess.arizona.edu
    SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/d1c9a073-3f2b-4b00-a06c-b037bc50083d"
    PUB_SSL_CERT_ARN="arn:aws:acm:us-west-2:242275737326:certificate/d1c9a073-3f2b-4b00-a06c-b037bc50083d"
  fi
else
  error_abort "Could not determine the SSL Cert ARN based on the Hosted Zone Name (${HOSTED_ZONE_NAME})"
fi

# -- Docker Images
# -- This will change frequently and may be different from Environment to Environment
# -- Will hard code for now and revisit in the future
case ${UPPER_ENV} in
  DMO)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    fi
    ;;
  DMA)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    fi
    ;;
  DMB)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85525"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85525"
    fi
    ;;
  DEV)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    fi
    ;;
  TRN)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    fi
    ;;
  TST)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    fi
    ;;
  TEA)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85525"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85525"
    fi
    ;;
  TEB)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85525"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85525"
    fi
    ;;
  TEC)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85525"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85525"
    fi
    ;;
  STG)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    fi
    ;;
  SUP)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    fi
    ;;
  RPT)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    fi
    ;;
  PRD)
    if [ "$UPPER_PILLAR" = "HR" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "EL" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85524"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85524"
    elif [ "$UPPER_PILLAR" = "SA" ]; then
      APP_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/app-batch:CentOS6-PT85521"
      WEB_DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/web:CentOS6-PT85521"
    fi
    ;;
  *)
    error_abort "Could not determine a Docker Images based on the target environment passed (${UPPER_ENV})"
    ;;
esac

# -- Web Profile Name 
# -- Will be DEV for all non-prod environments and PROD for prod environrments
# -- Will hard code for now and revisit in the future
if [ "${UPPER_ENV}" == "PRD" ] || [ "${UPPER_ENV}" == "SUP" ]; then
  WEB_PROFILE="PROD"
else
  WEB_PROFILE="DEV"
fi

# -- PS Reports Directory
# -- Will be /[pillar]/reports/[env]
PSREPORTS_DIR="/${LOWER_PILLAR}/reports/uaz${LOWER_PILLAR}${LOWER_ENV}"

# -- App or Batch with be BOTH for all non-production environments and BATCH for production environments
if [ "${SET_UP_LIKE_PRD}" == "Y" ]; then
  APP_OR_BATCH="APP"
else
  APP_OR_BATCH="BOTH"
fi

# -- App Template will be small for all non-production environments and large for production
if [ "${SET_UP_LIKE_PRD}" == "Y" ]; then
  # -- MEM-18377 will change template to small from large to small
  APP_TEMPLATE="small"
else
  APP_TEMPLATE="small"
fi

# -- App OPRID will be UAZPRCS and the password as well, need to find a better way to handle creds
if [ "${UPPER_ENV}" == "DMO" ] || [ "${UPPER_ENV}" == "DMA" ] || [ "${UPPER_ENV}" == "DMB" ]; then 
  APP_OPR_ID="PS"
  APP_OPR_ID_PW="PS"
else
  APP_OPR_ID=$(aws ssm get-parameter --name "PeopleSoftUser" --with-decryption --output text --query 'Parameter.[Value]' 2>&1)
  APP_OPR_ID_PW=$(aws ssm get-parameter --name "PeopleSoftUserPW" --with-decryption --output text --query 'Parameter.[Value]' 2>&1)
fi

# -- App Bit Bucket Branch will vary based environment
case ${UPPER_ENV} in
  DMO)
    APP_BIT_BUCKET_BRANCH="demo"
    ;;
  DMA)
    APP_BIT_BUCKET_BRANCH="dma"
    ;;
  DMB)
    APP_BIT_BUCKET_BRANCH="dmb"
    ;;
  DEV)
    APP_BIT_BUCKET_BRANCH="development"
    ;;
  TST)
    APP_BIT_BUCKET_BRANCH="test"
    ;;
  TEA)
    APP_BIT_BUCKET_BRANCH="tea"
    ;;
  TEB)
    APP_BIT_BUCKET_BRANCH="teb"
    ;;
  TEC)
    APP_BIT_BUCKET_BRANCH="tec"
    ;;
  STG)
    APP_BIT_BUCKET_BRANCH="stage"
    ;;
  TRN)
    APP_BIT_BUCKET_BRANCH="trn"
    ;;
  SUP)
    APP_BIT_BUCKET_BRANCH="support"
    ;;
  RPT)
    APP_BIT_BUCKET_BRANCH="rpt"
    ;;
  PRD)
    APP_BIT_BUCKET_BRANCH="production"
    ;;
  *)
    error_abort "Could not determine App Bit Bucket Branch based on the target environment passed (${UPPER_ENV})"
    ;;
esac

# -- SES Server will be sesdev.ps-nonprod-aws.arizona.edu for non prod and sesprd.ps-nonprod-aws.arizona.edu for
# -- SUP and PRD
case ${UPPER_ENV} in
  DMO)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  DMA)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  DMB)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  DEV)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TRN)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TST)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TEA)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TEB)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  TEC)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  STG)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="els.ps-nonprod-aws.arizona.edu"
    fi
    ;;
  SUP)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    fi
    ;;
  RPT)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    fi
    ;;
  PRD)
    if [ $UPPER_PILLAR = "HR" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "EL" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    elif [ $UPPER_PILLAR = "SA" ]; then
      SES_SERVER="sesprd.ps-prod-aws.arizona.edu"
    fi
    ;;
  *)
    error_abort "Could not determine App Bit Bucket Branch based on the target environment passed (${UPPER_ENV})"
    ;;
esac

# -- SES Definitions will vary passed on pillar
case ${UPPER_PILLAR} in
  EL)
    SES_DEFNS="LS_LM_ACT_CI,LS_LM_LEARNING,LS_LM_OBJV,LS_LM_PRG,PTPORTALREGISTRY"
    ;;
  HR)
    SES_DEFNS="HC_BEN_HEALTH_BENEFIT,PTPORTALREGISTRY"
    ;;
  SA)
    SES_DEFNS="PTPORTALREGISTRY"
    ;;
  *)
    error_abort "Could not determine Search Definitions based on the pillar passed (${UPPER_PILLAR})"
    ;;
esac

# -- DB Multi Zone will be true for production and false for everything else
if [ "${SET_UP_LIKE_PRD}" == "Y" ]; then 
  DB_MULTI_ZONE="true"
else
  DB_MULTI_ZONE="false"
fi

# -- DB Inst Class must be at least db.t2.large for encryption to work
case ${UPPER_PILLAR} in
  EL)
    if [ "${SET_UP_LIKE_PRD}" == "Y" ]; then 
      DB_INST_CLASS="db.m4.large"
    elif [ "${SET_UP_LIKE_PRD}" == "T" ]; then 
      DB_INST_CLASS="db.m4.large"
    else
      DB_INST_CLASS="db.t2.large"
    fi
    ;;
  HR)
    if [ "${SET_UP_LIKE_PRD}" == "Y" ]; then 
      DB_INST_CLASS="db.m4.2xlarge"
    else
      #Make SUP and m4.xlarge, other nonprods just use m4.large
      if [ "${UPPER_ENV}" = "SUP" ]; then
        DB_INST_CLASS="db.m4.xlarge"
      else
        if [ "${SET_UP_LIKE_PRD}" == "T" ]; then 
          DB_INST_CLASS="db.m4.xlarge"
        else
          DB_INST_CLASS="db.m4.large"
        fi
      fi
    fi
    ;;
  SA)
    if [ "${SET_UP_LIKE_PRD}" == "Y" ]; then 
      DB_INST_CLASS="db.m4.2xlarge"
    else
      #Make SUP and m4.xlarge, other nonprods just use m4.large
      if [ "${UPPER_ENV}" = "SUP" ] || [ "${UPPER_ENV}" = "RPT" ]; then
        DB_INST_CLASS="db.m4.xlarge"
      else
        if [ "${SET_UP_LIKE_PRD}" == "T" ]; then 
          DB_INST_CLASS="db.m4.xlarge"
        else
          DB_INST_CLASS="db.m4.large"
        fi
      fi
    fi
    ;;
  *)
    error_abort "Could not determine DB Inst Class based on the pillar passed (${UPPER_PILLAR})"
    ;;
esac

# -- DB Storage Type will be gp2 (general purpose) for all DBs for now
DB_STORAGE_TYPE="gp2"

# -- For now HR environments will be behing the VPN so we will not make the ELB public facing
# -- all other environments will be pubic facing
if [ "$UPPER_PILLAR" == "HR" ]; then
  PUBLIC_FACING_ELB="Y"
else
  PUBLIC_FACING_ELB="Y"
fi

# -- PSAWS-54 add a new parameter PSWEB_REQUIRE_DUO.  Will require Duo if this is HR
PSWEB_REQUIRE_DUO="N"
#if [ "$UPPER_PILLAR" = "HR" ]; then
#  if [ "$UPPER_ENV" != "PRD" ]; then
#    PSWEB_REQUIRE_DUO="Y"
#  fi
#fi

# -- MEM-18660 Sender Email Address for app server
APP_SENDER_EMAIL="peoplesoft+${LOWER_PILLAR}${LOWER_ENV}@email.arizona.edu"
# -- For HR Production the sender needs to be UAccessEmployee
if [ "${UPPER_PILLAR}" = "HR" ] && [ "${UPPER_ENV}" = "PRD" ]; then
  APP_SENDER_EMAIL="UAccessEmployee@email.arizona.edu"
fi

# -- This if the AMI for the ECS Instance running in the ECS cluster 
# -- it will change from time to time
# -- Image last updated on 09/17/2017 
# -- Go here to find latest image 
# -- http://docs.aws.amazon.com/AmazonECS/latest/developerguide/ecs-optimized_AMI.html
# -- Updated to ami-decc7fa6 (amzn-ami-2017.09.g-amazon-ecs-optimized) 01/22/2018
ECS_IMAGE_ID=ami-decc7fa6

# -- SAAWS-1 Add logic for the Public Sites
# -- For now we will create a public site for DEV, TST, STG, SUP, PRD
# -- MEM-19784 08/06/2018 - Added TEA, TEB, and TEC to the list
CREATE_PUBLIC_SITE="N"
ENV_LIST="DEV TST STG TEA TEB TEC SUP PRD"
if [ "$UPPER_PILLAR" = "SA" ]; then
  for env in $ENV_LIST
  do
    if [ "$UPPER_ENV" = $env ]; then
      CREATE_PUBLIC_SITE="Y"
    fi
  done
fi

# -- PSAWS-69 find perferred AZ, need to place all nonprod environments in the same
# -- AZ in order to avoid some batch processes from suffering from latency
PREFERRED_AZ=$(aws ec2 describe-availability-zones \
               --output text \
               --region ${REGION} \
               --filters "Name=state,Values=available" \
               --query 'AvailabilityZones[*].ZoneName' | awk '{print $1}')

# -- PSAWS-69 find perferred AZ, need to place all nonprod environments in the same
# -- Not that we have the Preferred AZ, get the Private Subnet that is in that AZ
PREFERRED_PRIVATE_SUBNET=$(aws ec2 describe-subnets --output text --region ${REGION} --filters "Name=availabilityZone,Values=${PREFERRED_AZ}" "Name=tag-key,Values=Name" "Name=tag-value,Values=*Private*" --query 'Subnets[*].[SubnetId]' | awk '{print $1}')

# -- SAAWS-350 list of INAS mak files that need to be run when COBOLs are compiled
INAS_MAK_FILES="inasbl16.mak inasbl17.mak inasbl18.mak"

}

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage
{
echo "Usage: build_ps_env.sh"
echo "Options:"
echo "       -p (required) = Pillar valid values are HR, SA, EL"
echo "       -e (required) = Environment including but not limited to DEV, TST, STG, SUP"
echo "       -s (optional) = Source Environment, populate this if refreshing from a different environment (ie PRD)"
echo "       -d (optional) = Destroy the Environment first if it is already running, then build it"
echo "       -D (optional) = Destroy the Environment if it is already running, and then stop"
echo "       -z (optional) = Set up as though it were production (useful when testing in nonprod)"
echo "       -Z  Beef up non production environment (a bigger env for major testing)"
echo "       -o (optional) = Run Obscure Post Refresh SQL if this is a refresh"
echo "       -P (optional) = Point in Time refresh specify date/time or leave blank for latest refresh date"
echo "                       Example format: 2017-11-26T18:56:40MST"
echo "       -m  Mode - Use "a" to just apply this to the app/web layer and leave the DB alone"
}

#*******************************************************************************
#
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
typeset MSG=$1

# -- If this is an error for a RPT, SUP, or PRD then send a message to the VictorOps channel
if [ "${UPPER_ENV}" = "SUP" ] || [ "${UPPER_ENV}" = "RPT" ] || [ "${UPPER_ENV}" = "PRD" ]; then
  USE_THIS_SLACK_URL=${SLACK_URL_VO}
  SLACK_CHANNEL="ua-errors"
else
  USE_THIS_SLACK_URL=${SLACK_URL}
  SLACK_CHANNEL="met-errors"
fi

# -- Send a message to the #met-errors channel
if ! [ -z "${USE_THIS_SLACK_URL}" ]; then
  SLACK_MSG="Error running build_ps_env_ecs.sh - (${MSG})"
  JSON="{ \"channel\": \"${SLACK_CHANNEL}\", \"text\": \"${SLACK_MSG}\" }"
  OUTPUT=$(curl -s -d "payload=${JSON}" "${USE_THIS_SLACK_URL}" 2>&1)
fi

echo
echo "ERROR: ${MSG}"
echo "aborting . . ."
echo
exit 255
}

#*******************************************************************************
#
# function: destroy_environment
#
# This function will destroy the environment. 
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function destroy_environment
{

# -- create log file
log_file="/tmp/log${TARGET_DB_INSTANCE}.out"

# -- run pre-refresh process on targert DB before deleting CloudFormation Delete
# -- this will retain important info for environment refreshes
sqlplus -s /@AWSPSTLS << EOF-SQL > $log_file 2>&1
set feed off timing off heading off pagesize 0
begin
  aws_refresh.pre_refresh('${UPPER_PILLAR}','${UPPER_ENV}');
end;
/
EOF-SQL

# -- if the there was an error exit with error
# -- this will return 0 if there is an ORA- error and 1 if there is not
egrep "ORA-" $log_file &> /dev/null 2>&1
if [ "$?" == "0" ]; then
  error_abort "Error running aws_refesh.pre_refresh-(`cat $log_file`)"
fi

# -- MEM-19186 put the DB into blackout in OEM before removing the environment
OUTPUT=$(../../common/sh/blackout.sh -t ${TARGET_DB_INSTANCE} 2>&1) 

if ! [ "$?" = "0" ]; then
  echo "WARNING: Issues placing DB into blackout mode ($OUTPUT)."
fi

# -- Get the PointInTimeRefresh parameter from the stack, it it's set to Y then the DB was not created
# -- as part of the stack and will need to be deleted separately
CF_PIT_REFRESH=$(aws cloudformation describe-stacks \
                 --region ${REGION} \
                 --stack-name ${CF_STACK_NAME} \
                 --output text \
                 --query 'Stacks[*].Parameters[?ParameterKey==`PointInTimeRefresh`].ParameterValue' 2>&1)
#--Check to see if the command failed
if ! [ "$?" = "0" ]; then
  error_abort "Error find PointInTimeRefresh parameter in existing CF Stack ($CF_PIT_REFRESH)"
fi
#--If the parameter was not set then default to N
if [ -z $CF_PIT_REFRESH ]; then
  CF_PIT_REFRESH="N"
fi

# -- issue the delete stack command
OUTPUT=$(aws cloudformation delete-stack \
         --region ${REGION} \
         --stack-name ${CF_STACK_NAME}  \
         --role-arn ${ROLE_ARN} 2>&1)

# -- if there was an error then abort
if [ $? -ne 0 ]; then
  error_abort "Problems deleting CloudFormation Stack ${CF_STACK_NAME} ($OUTPUT)"
fi

# -- If MODE = B then we need to delete the DB as well
if [ "$MODE" = "B" ]; then
  # -- While stack is deleting check to see if we need to remove the RDS instance because it was a 
  # -- Point in Time refresh
  if [ "$CF_PIT_REFRESH" = "Y" ]; then
    OUTPUT=$(../../common/sh/deldb.sh -t ${TARGET_DB_INSTANCE} -s 2>&1)
    if ! [ "$?" = "0" ]; then
      error_abort "Encountered issues deleting RDS Instance built based on Point In Time refresh ($OUTPUT)"
    fi
  else
    # -- issue the delete stack command
    OUTPUT=$(aws cloudformation delete-stack \
             --region ${REGION} \
             --stack-name ${CF_STACK_NAME_DB}  \
             --role-arn ${ROLE_ARN} 2>&1)
    # -- wait for delete to be complete, it will check every 30 seconds for an hour
    # -- if delete does not complete by then it will throw an error
    aws cloudformation wait stack-delete-complete \
       --region ${REGION} \
       --stack-name "${CF_STACK_NAME_DB}" 
    if [ "$?" -ne "0" ]; then
      error_abort "Problems waiting for the deleting of Database CloudFormation Stack ${CF_STACK_NAME_DB}."
    fi
  fi
fi

# -- wait for delete to be complete, it will check every 30 seconds for an hour
# -- if delete does not complete by then it will throw an error
aws cloudformation wait stack-delete-complete \
   --region ${REGION} \
   --stack-name "${CF_STACK_NAME}" 

# -- if there was an error then try to delete again
# -- put this in place to attempt to fix the issue with the ECS Cluster
# -- not deleting becuase the EC2 instance has not been terminated yet
if [ "$?" -ne "0" ]; then
  # -- issue the delete stack command again
  aws cloudformation delete-stack \
     --region ${REGION} \
     --stack-name "${CF_STACK_NAME}"  \
     --role-arn "${ROLE_ARN}"

  # -- Wait for stack delete cammand to finish
  aws cloudformation wait stack-delete-complete\
     --region ${REGION} \
     --stack-name "${CF_STACK_NAME}" 

  #Now we can abort if the second delete did not work
  if [ "$?" -ne "0" ]; then
    error_abort "Problems waiting for the deleting of CloudFormation Stack ${CF_STACK_NAME}."
  fi
fi
}

#*******************************************************************************
#
# function: check_if_env_exists
#
# This funtion will check if the CloudFromation Tempalte exists.  If so, it will
# evaluate the DESTROY_IF_EXISTS vairable.  If Y(es) then the CloudFormation 
# Template will be destroyed, if N(o) then the program will stop with an error
# stating that the environment is already running.
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function check_if_env_exists
{
# -- describe the CloudFormation Template stored in CF_STACK_NAME
# -- if the snapshot file was not populated with data then we have an issue and have to abort
# -- look for the string arn:aws:cloudformation in the output, if it's there the environment exists
aws cloudformation describe-stacks --stack-name ${CF_STACK_NAME} --output text 2>&1 |grep -q "arn:aws:cloudformation" &> /dev/null

# -- if the grep finds the string it will return a 0, otherwise it will return a 1
if [ $? == 0 ]; then 
  ENV_EXISTS="Y"
else  
  ENV_EXISTS="N"
fi

# -- if the env exists and -D (destroy env) was passed then destroy the environment
if [ "${ENV_EXISTS}" == "Y" ]; then
  if [ "${DESTROY_IF_EXISTS}" == "Y" ]; then

    # -- Send a message to the to the refresh channel
    if ! [ -z "${SLACK_URL}" ]; then
      SLACK_MSG="${UPPER_PILLAR}${UPPER_ENV} is going offline"
      JSON="{ \"channel\": \"${REFRESH_SLACK_CHANNEL}\", \"text\": \"${SLACK_MSG}\" }"
      OUTPUT=$(curl -s -d "payload=${JSON}" "${SLACK_URL}" 2>&1)
    fi
    destroy_environment
    # -- Send a message to the to the refresh channel
    if ! [ -z "${SLACK_URL}" ]; then
      SLACK_MSG="${UPPER_PILLAR}${UPPER_ENV} has been successfully destroyed"
      JSON="{ \"channel\": \"${REFRESH_SLACK_CHANNEL}\", \"text\": \"${SLACK_MSG}\" }"
      OUTPUT=$(curl -s -d "payload=${JSON}" "${SLACK_URL}" 2>&1)
    fi

  elif [ "${DESTROY_IF_EXISTS}" == "N" ]; then
    print_usage
    echo
    error_abort "The CloudFormation Template ${CF_STACK_NAME} already exists and a \"-D\" or \"-d\" was not passed."
  fi
fi

# --if -D was passed then we only want to destroy the environment and be done
if [ "${DESTROY_ONLY}" == "Y" ]; then
  if [ "${ENV_EXISTS}" == "Y" ]; then
    echo "CloudFormation Stack ${CF_STACK_NAME} has been successfully destroyed.  All done."
  else
    echo "CloudFormation Stack ${CF_STACK_NAME} did not exist, so there is nothing to destroy."
  fi
  exit
fi

}

#*******************************************************************************
#
# function: build_environment
# This function will build the environment. 
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function build_environment
{

# -- Send a message to the to the refresh channel
if ! [ -z "${SLACK_URL}" ]; then
  SLACK_MSG="${UPPER_PILLAR}${UPPER_ENV} is starting the build process"
  JSON="{ \"channel\": \"${REFRESH_SLACK_CHANNEL}\", \"text\": \"${SLACK_MSG}\" }"
  OUTPUT=$(curl -s -d "payload=${JSON}" "${SLACK_URL}" 2>&1)
fi

#Before we build the enviornment reset the FIRST_ONE field in the REFRESH.PS_ENVIORNMENTS
#table so post refresh scripts and ACM scripts will run properly.
# -- create log file
log_file="/tmp/log${TARGET_DB_INSTANCE}.out"

# -- run pre-refresh process on targert DB before deleting CloudFormation Delete
# -- this will retain important info for environment refreshes
sqlplus -s /@AWSPSTLS << EOF-SQL > $log_file 2>&1
set feed off timing off heading off pagesize 0
var v_first_one varchar2;
begin
  :v_first_one := aws_refresh.is_first_one('${UPPER_PILLAR}','${UPPER_ENV}','Y');
end;
/
EOF-SQL

# -- if the there was an error exit with error
# -- this will return 0 if there is an ORA- error and 1 if there is not
egrep "ORA-" $log_file &> /dev/null 2>&1
if [ "$?" == "0" ]; then
  error_abort "Error running aws_refesh.is_first_one ($log_file)"
fi

# -- if MODE = B then we need to build the DB, if not then this is only for the App Layer so don't touch the DB
if [ "$MODE" = "B" ]; then 
  # -- if this is a point in time refresh create the RDS instance here
  # -- will pass a parameter into the CF template that will prevent the DB from begin
  # -- created in the template
  if [ "$PIT_REFRESH" = "Y" ]; then
    TARGET_DB_NAME="${UPPER_PILLAR}${UPPER_ENV}"
    if ! [ -z $PIT_DATE ]; then
      OUTPUT=$(../../common/sh/resdbpit.sh -s $SOURCE_DB_INSTANCE -t $TARGET_DB_INSTANCE -d $TARGET_DB_NAME -P $PIT_DATE 2>&1)
    else
      OUTPUT=$(../../common/sh/resdbpit.sh -s $SOURCE_DB_INSTANCE -t $TARGET_DB_INSTANCE -d $TARGET_DB_NAME 2>&1)
    fi
    #If there was an error stop and report
    if ! [ "$?" = "0" ]; then
      error_abort "Issues creating Point in Time Refresh database DB Inst=$TARGET_DB_INSTANCE ($OUTPUT)"
    fi
  else
    # -- PSAWS-69 If this is not a PIT refresh then we need to build the DB via a CF template
    # -- the DB has been removed from the main CF template and moved into it's own.
    read -d '' JSON_STRING <<ENDJSON
    [
     { "ParameterKey": "PillarLowerCase","ParameterValue": "${LOWER_PILLAR}"}, 
     { "ParameterKey": "EnvironmentLowerCase","ParameterValue": "${LOWER_ENV}" }, 
     { "ParameterKey": "PillarUpperCase","ParameterValue": "${UPPER_PILLAR}" }, 
     { "ParameterKey": "EnvironmentUpperCase","ParameterValue": "${UPPER_ENV}" }, 
     { "ParameterKey": "DBSnapshotID","ParameterValue": "${DB_LATEST_SNAPSHOT}" }, 
     { "ParameterKey": "DBMultiAz","ParameterValue": "${DB_MULTI_ZONE}" }, 
     { "ParameterKey": "DBInstanceClass","ParameterValue": "${DB_INST_CLASS}" }, 
     { "ParameterKey": "DBStorageType","ParameterValue": "${DB_STORAGE_TYPE}" },
     { "ParameterKey": "TagService","ParameterValue": "${TAG_SERVICE}" },
     { "ParameterKey": "TagContactNetid","ParameterValue": "${TAG_CONTACT_NETID}" },
     { "ParameterKey": "TagAccountNumber","ParameterValue": "${TAG_ACCOUNT_NUMBER}" },
     { "ParameterKey": "TagSubAccount","ParameterValue": "${TAG_SUBACCOUNT}" },
     { "ParameterKey": "SetUpLikePrd", "ParameterValue": "${SET_UP_LIKE_PRD}" },
     { "ParameterKey": "PreferredAz", "ParameterValue": "${PREFERRED_AZ}" }
    ]
ENDJSON

    OUTPUT=$(aws cloudformation create-stack \
       --region ${REGION} \
       --stack-name "${CF_STACK_NAME_DB}"  \
       --capabilities "CAPABILITY_IAM" \
       --template-url "${TEMPLATE_URL_DB}" \
       --parameters "${JSON_STRING}" \
       --role-arn "${ROLE_ARN}" \
       --disable-rollback 2>&1)

    #Make sure the CF Stack create command was kicked off without any issues
    if ! [ "$?" = "0" ]; then
      error_abort "Error encountered creating CF Stack ($OUTPUT)"
    fi

    #Make sure the CF Stack wait command finished without any issues
    #sometimes the DB takes longer than an hour to build so we need to wait longer
    cnt=0
    while [ $cnt -lt 6 ]
    do
      OUTPUT=`aws cloudformation wait stack-create-complete --stack-name "${CF_STACK_NAME_DB}" 2>&1`

      if [ $? = 0 ]; then
        break
      fi

      #Increment counter
      cnt=$(( cnt+1 ))

      if [ $cnt -gt 5 ]; then
        error_abort "Error encountered waiting for Database CF Stack to create ($OUTPUT)"
      fi
    done

  fi #End of if this is a PIT Refresh
fi #End of if MODE = B

# -- If this is a refresh (source is different from target) then we need to reset the 
# -- bitbucket branch to reflect the state production-copy if the branch is the dev repo
# -- or production if the branch is in the prd repo
if [ "${RUN_POST_REFRESH}" = "Y" ] && [ "${UPPER_ENV}" != "PRD" ]; then

  # -- Sync the Custom Home Repo first
  if [ "$APP_BIT_BUCKET_BRANCH" = "production" ] || \
     [ "$APP_BIT_BUCKET_BRANCH" = "stage" ]; then
    REPO="${LOWER_PILLAR}-prd"
    MASTER="production"
  else
    REPO="${LOWER_PILLAR}-dev"
    MASTER="production-copy"
  fi
  GIT_HOME="/home/ec2-user/git/${REPO}"

  # -- First make sure master is up to date
  OUTPUT=$(git -C $GIT_HOME checkout $MASTER 2>&1)
  OUTPUT=$(git -C $GIT_HOME pull 2>&1)

  # -- Next, check out environment butbucket branch
  OUTPUT=$(git -C $GIT_HOME checkout $APP_BIT_BUCKET_BRANCH 2>&1)

  # -- Reset to MASTER
  OUTPUT=$(git -C $GIT_HOME reset --hard  $MASTER 2>&1)

  # -- Push changes to bitbucket forced
  OUTPUT=$(git -C $GIT_HOME push --force origin $APP_BIT_BUCKET_BRANCH 2>&1)


  # -- Now sync the App Home
  REPO="peoplesoft-app-home-${LOWER_PILLAR}"
  GIT_HOME="/home/ec2-user/git/${REPO}"
  MASTER="production"

  # -- First make sure master is up to date
  OUTPUT=$(git -C $GIT_HOME checkout $MASTER 2>&1)
  OUTPUT=$(git -C $GIT_HOME pull 2>&1)

  # -- Next, check out environment butbucket branch
  OUTPUT=$(git -C $GIT_HOME checkout $APP_BIT_BUCKET_BRANCH 2>&1)

  # -- Reset to MASTER
  OUTPUT=$(git -C $GIT_HOME reset --hard  $MASTER 2>&1)

  # -- Push changes to bitbucket forced
  OUTPUT=$(git -C $GIT_HOME push --force origin $APP_BIT_BUCKET_BRANCH 2>&1)


  # -- SAAWS-68 - create SASUP environment
  # -- Now run the Post Refresh process because the DB is up
  # -- moved this here and out of the app-batch docker image
  PLSQL_OUTPUT=`sqlplus -s /@AWSPSTLS << EOF-SQL
set feed off timing off heading off pagesize 0
begin
 aws_refresh.post_refresh('${UPPER_PILLAR}','${UPPER_ENV}','${OBSCURE_ON_REFRESH}');
end;
/
EOF-SQL`

  # -- if the there was a syntax error then error_abort
  if [ "$?" -ne "0" ]; then
    error_abort "Error running aws_refesh.post_refresh-${PLSQL_OUTPUT}"
  fi

  # -- if the there was a ORA- error then error_abort
  echo $PLSQL_OUTPUT|egrep "ORA-" 2>&1
  if [ "$?" = "0" ]; then
    error_abort "Error (ORA-) running aws_refesh.post_refresh-${PLSQL_OUTPUT}"
  fi

fi

read -d '' JSON_STRING <<ENDJSON
[
 { "ParameterKey": "PillarLowerCase","ParameterValue": "${LOWER_PILLAR}"}, 
 { "ParameterKey": "EnvironmentLowerCase","ParameterValue": "${LOWER_ENV}" }, 
 { "ParameterKey": "PillarUpperCase","ParameterValue": "${UPPER_PILLAR}" }, 
 { "ParameterKey": "EnvironmentUpperCase","ParameterValue": "${UPPER_ENV}" }, 
 { "ParameterKey": "RunPostRefresh","ParameterValue": "${RUN_POST_REFRESH}" },
 { "ParameterKey": "HostedZoneName","ParameterValue": "${HOSTED_ZONE_NAME}" }, 
 { "ParameterKey": "AuthTokenDomain","ParameterValue": "${AUTH_TOKEN_DOMAIN}" }, 
 { "ParameterKey": "FQDNPrefix","ParameterValue": "${FQDN_PREFIX}" }, 
 { "ParameterKey": "WebELBListenerPolicyNames","ParameterValue": "${WEB_ELB_LISTENER_POLICY_NAME}" }, 
 { "ParameterKey": "WebELBListenerSSLCertID","ParameterValue": "${SSL_CERT_ARN}" }, 
 { "ParameterKey": "WebDockerImage","ParameterValue": "${WEB_DOCKER_IMAGE}" },
 { "ParameterKey": "WebProfileName","ParameterValue": "${WEB_PROFILE}" }, 
 { "ParameterKey": "PsReportsDirecory","ParameterValue": "${PSREPORTS_DIR}" },
 { "ParameterKey": "AppDockerImage","ParameterValue": "${APP_DOCKER_IMAGE}" },
 { "ParameterKey": "AppOrBatch","ParameterValue": "${APP_OR_BATCH}" },
 { "ParameterKey": "AppTemplate","ParameterValue": "${APP_TEMPLATE}" },
 { "ParameterKey": "AppOprId","ParameterValue": "${APP_OPR_ID}" },
 { "ParameterKey": "PSAppOpridPw", "ParameterValue": "${APP_OPR_ID_PW}" },
 { "ParameterKey": "AppBitBucketBranch","ParameterValue": "${APP_BIT_BUCKET_BRANCH}" },
 { "ParameterKey": "AppSesServer","ParameterValue": "${SES_SERVER}" }, 
 { "ParameterKey": "AppSesDefns","ParameterValue": "${SES_DEFNS}" }, 
 { "ParameterKey": "TagService","ParameterValue": "${TAG_SERVICE}" },
 { "ParameterKey": "TagContactNetid","ParameterValue": "${TAG_CONTACT_NETID}" },
 { "ParameterKey": "TagAccountNumber","ParameterValue": "${TAG_ACCOUNT_NUMBER}" },
 { "ParameterKey": "TagSubAccount","ParameterValue": "${TAG_SUBACCOUNT}" },
 { "ParameterKey": "SetUpLikePrd", "ParameterValue": "${SET_UP_LIKE_PRD}" },
 { "ParameterKey": "PublicFacingELB", "ParameterValue": "${PUBLIC_FACING_ELB}" },
 { "ParameterKey": "Tls12", "ParameterValue": "${TLS12}" },
 { "ParameterKey": "ObscureOnRefresh", "ParameterValue": "${OBSCURE_ON_REFRESH}" }, 
 { "ParameterKey": "PsWebRequireDuo", "ParameterValue": "${PSWEB_REQUIRE_DUO}" },
 { "ParameterKey": "PointInTimeRefresh", "ParameterValue": "${PIT_REFRESH}" },
 { "ParameterKey": "AppSenderEmail", "ParameterValue": "${APP_SENDER_EMAIL}" },
 { "ParameterKey": "CreatePublicSite", "ParameterValue": "${CREATE_PUBLIC_SITE}" },
 { "ParameterKey": "PreferredPrivateSubnet", "ParameterValue": "${PREFERRED_PRIVATE_SUBNET}" },
 { "ParameterKey": "PubFQDNPrefix", "ParameterValue": "${PUB_FQDN_PREFIX}" },
 { "ParameterKey": "PubAuthTokenDomain", "ParameterValue": "${PUB_AUTH_TOKEN_DOMAIN}" },
 { "ParameterKey": "PubHostedZoneName", "ParameterValue": "${PUB_HOSTED_ZONE_NAME}" },
 { "ParameterKey": "PubWebELBListenerSSLCertID", "ParameterValue": "${PUB_SSL_CERT_ARN}" },
 { "ParameterKey": "InasMakFiles", "ParameterValue": "${INAS_MAK_FILES}" }
]
ENDJSON

# Removed from parameter list as this will be retrieved via a SSM call in the CF template 
# { "ParameterKey": "EcsImageId", "ParameterValue": "${ECS_IMAGE_ID}" },

OUTPUT=$(aws cloudformation create-stack \
   --region ${REGION} \
   --stack-name "${CF_STACK_NAME}"  \
   --capabilities "CAPABILITY_IAM" \
   --template-url "${TEMPLATE_URL}" \
   --parameters "${JSON_STRING}" \
   --role-arn "${ROLE_ARN}" \
   --disable-rollback 2>&1)

#Make sure the CF Stack create command was kicked off without any issues
if ! [ "$?" = "0" ]; then
  error_abort "Error encountered creating CF Stack ($OUTPUT)"
fi

OUTPUT=`aws cloudformation wait stack-create-complete --stack-name "${CF_STACK_NAME}" 2>&1`

#Make sure the CF Stack wait command finished without any issues
if ! [ $? = "0" ]; then
  error_abort "Error encountered waiting for CF Stack to create ($OUTPUT)"
fi

#Now that the stack is created attach EC2 instance(s) to the Excel to CI Target Group
register_excel_to_ci

# -- MEM-19186 take DB out of blackout in OEM after environment is built
OUTPUT=$(../../common/sh/blackout.sh -t ${TARGET_DB_INSTANCE} -d 2>&1) 

if ! [ "$?" = "0" ]; then
  echo "WARNING: Issues taking DB out of blackout mode ($OUTPUT)."
fi

# -- Sleep for 5 minutes to give the web/app/servers to come online
sleep 5m

# -- Send a message to the to the refresh channel
if ! [ -z "${SLACK_URL}" ]; then
  SLACK_MSG="${UPPER_PILLAR}${UPPER_ENV} build process is complete"
  JSON="{ \"channel\": \"${REFRESH_SLACK_CHANNEL}\", \"text\": \"${SLACK_MSG}\" }"
  OUTPUT=$(curl -s -d "payload=${JSON}" "${SLACK_URL}" 2>&1)
fi

}

#*******************************************************************************
#
# function: register_excel_to_ci
#
# This function will check to see if an elbv2 Target Group exists with the
# lower PillarEnv name along with a ExCI in the name.  If one does exist then
# the EC2 instances that have the upper PillarEnv-ECS at the end of the Tag=Name
# (i.e. HRPRD-ECS) will be registered to that Target Group.
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function register_excel_to_ci
{

#This will find the Excel to CI Target Group and attach 
#instances that include the PillarEnv-ECS (i.e. HRPRD-ECS) at the end
#First find the target group that includes the Lower PillarEnv and ExCI
TARGETGROUP_ARN=`aws elbv2 describe-target-groups --query 'TargetGroups[*].[TargetGroupArn,TargetGroupName]' --output text | grep ${LOWER_PILLAR}${LOWER_ENV} | grep "ExCI" | awk '{print $1}' 2>&1`

#Make sure the describe statement ran properly
if ! [ $? = "0" ]; then
  error_abort "Error encountered describing Target Group ($OUTPUT)"
fi

#If a Target Group ARN was found then find instances to register to it
if ! [ -z $TARGETGROUP_ARN ]; then

  #Find instances that are running (instance-state-code=16) and have Upper PillarEnv-ECS in the Tag Name
  INSTANCES=`aws ec2 describe-instances --filter "Name=tag:Name,Values=*${UPPER_PILLAR}${UPPER_ENV}-ECS" "Name=instance-state-code,Values=16" --query 'Reservations[*].Instances[*].[InstanceId]' --output=text`

  for inst_id in `echo $INSTANCES`; do
    OUTPUT=`aws elbv2 register-targets --target-group-arn $TARGETGROUP_ARN --targets Id=$inst_id 2>&1`
    #Check for errors
    if ! [ $? = "0" ]; then
      error_abort "Error encountered registering EC2 instance to Target Group ($OUTPUT)"
    fi
  done

fi

}

#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
# -- Make sure to be located in the directory the script is run
cd $(dirname $0)

# -- set environment
START_TS=`${DATE}`
SET_UP_LIKE_PRD="N"
OBSCURE_ON_REFRESH="N"
PIT_REFRESH="N"
PIT_FORMAT="%Y-%m-%dT%H:%M:%S%Z"
REGION=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)

# -- Find the SLACK_URL for slack messages
SLACK_URL=$(aws ssm get-parameter --name "SLACK_ALERT_URL" --with-decryption --output text --region $REGION --query 'Parameter.[Value]' 2>&1)
if ! [ "$?" = "0" ]; then
  unset SLACK_URL
fi

# -- Find the SLACK_URL for slack messages that will go to VictorOps
SLACK_URL_VO=$(aws ssm get-parameter --name "SLACK_ALERT_URL_VO" --with-decryption --output text --region $REGION --query 'Parameter.[Value]' 2>&1)
if ! [ "$?" = "0" ]; then
  unset SLACK_URL_VO
fi

# -- parse command-line options
while getopts :p:e:s:DdzZoP:m: arguments
do
  case $arguments in
    p)
       UPPER_PILLAR=${OPTARG^^}
       ;;
    e)
       UPPER_ENV=${OPTARG^^}
       ;;
    s)
       UPPER_SOURCE_ENV=${OPTARG^^}
       ;;
    d) 
       DESTROY_IF_EXISTS="Y"
       DESTROY_ONLY="N"
       ;;
    D)
       DESTROY_IF_EXISTS="Y"
       DESTROY_ONLY="Y"
       ;;
    #If -z is passed that means this will be set up like a PRD environment
    z) 
       SET_UP_LIKE_PRD="Y"
       ;;
    #MEM-19089
    #If -Z is passed that means this will be set up like a beefed up Test environment
    Z) 
       SET_UP_LIKE_PRD="T"
       ;;
    o)
       OBSCURE_ON_REFRESH="Y"
       ;;
    m)
       MODE=${OPTARG^^}
       ;;
    #If -P is passed then we need to do a Point in Time refresh
    P)
       PIT_REFRESH="Y"
       #If the value passed is now blank out the PIT_DATE variable, that will 
       #make the point in time look at the Latest Restoreable Time of the database
       if [ "${OPTARG,,}" = "now" ]; then
         unset PIT_DATE
       else
         PIT_DATE=${OPTARG}
       fi
       ;;
    :)
       error_abort "-P requires a value, either a date in the format \"`date +"%Y-%m-%dT%H:%M:%S%Z"`\"  or the word \"now\" for current date time"
       ;;
   \?)
       print_usage
       echo
       error_abort "option -${OPTARG} is not a valid argument"
  esac
done

# validate input parameters
validate_arguments

# -- check to see if the environment is running
# -- if so if the DESTROY_IF_EXISTS = Y then destroy the environment first
# -- if not, then exit program with error
check_if_env_exists

# -- if we reach this point we can build the environment
build_environment
