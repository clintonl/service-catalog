#!/bin/bash

aws cloudformation create-stack \
   --region us-west-2 \
   --stack-name "PeopleSoftSG"  \
   --template-body "file://~/git/service-catalog/portfolios/peoplesoft/templates/peoplesoft_security_groups.yaml" \
   --role-arn "arn:aws:iam::242275737326:role/fdn-iam-CloudFormationAdminDeployerRole-IP2KC5OSU7DS"
