#!/usr/bin/env bash
#*******************************************************************************
#
# TITLE......: build_roboreg.sh
# PARAMETER..:
#    /INPUTS.:
#              -z  Set up like production
#
#*******************************************************************************

#*******************************************************************************
#
# function: validate_arguments
#
# This function parses the arguments from the command-line flags and makes sure
# that all necessary and required parameters have been passed to the program.
# If it encounters an issue, it aborts the program.
#
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function validate_arguments {

    # -- Get Account Name
    AWS_ACCOUNT=`aws iam list-account-aliases|awk '{print $2}'`

    #  -- Roboreg is only used by Student
    TAG_SERVICE="Uaccess Student"

    TAG_ACCOUNT_NUMBER="Student Systems"
    TAG_SUBACCOUNT="Uaccess Student"

    # -- this the NetID of the person most familiar with the resources being created, will default to kellehs for now
    TAG_CONTACT_NETID="kellehs"

    # -- CloudFormation Stack Name will be PeopleSoft+UPPER_PILLAR+UPPER_ENV
    CF_STACK_NAME="PeopleSoft${UPPER_PILLAR}-ROBOREG"

    # -- Role ARN will be different based on what account we are in
    # -- Eventually we need to query this from AWS instead of hard coding
    # -- The CF Template is bigger than 51,200 characters so we need to call it from
    # -- an S3 bucket.  The template URL variable will hold the location of the S3 bucket
    case ${AWS_ACCOUNT} in
      ua-uits-peoplesoft-nonprod)
        ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
        ;;
      *)
        error_abort "Could not determine Role ARN based on the account (${AWS_ACCOUNT})"
        ;;
    esac

    PROD_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/eas-peoplesoft-roboreg:2018-01-30"
    NON_PROD_DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/eas-peoplesoft-roboreg:latest"

    # -- Set the Docker Image
    case ${AWS_ACCOUNT} in
      ua-uits-peoplesoft-nonprod)
        DOCKER_IMAGE=${NON_PROD_DOCKER_IMAGE}
        ;;
      ua-uits-peoplesoft-prod)
        DOCKER_IMAGE=${PROD_DOCKER_IMAGE}
        ;;
      *)
        error_abort "Could not determine Docker Image  based on the account (${AWS_ACCOUNT})"
        ;;
    esac

    if [ "${SET_UP_LIKE_PRD}" == "Y" ]; then
      DOCKER_IMAGE=${PROD_DOCKER_IMAGE}
    fi

    ########## Robo Reg ##################################
    #  See: SAAWS-8
    #  RoboReg provides access credentials to have the RoboReg agent connect to the RoboReg Service. These are stored
    #  in ec2 parameters.
    ##
    ROBOREG_FICE=$(aws ssm get-parameter --name "RoboRegFICE" --with-decryption --output text --query 'Parameter.[Value]' 2>&1)
    ROBOREG_CINCPASSWD=$(aws ssm get-parameter --name "RoboRegCINCPASSWD" --with-decryption --output text --query 'Parameter.[Value]' 2>&1)
		echo "FICE: ${ROBOREG_FICE}"
		echo "CINCPASSWD: ${ROBOREG_CINCPASSWD}"
}

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage {
    echo "Usage: build_roboreg.sh"
    echo "Options:"
    echo "       -z (optional) = Set up as though it were production (useful when testing in nonprod)"
}

#*******************************************************************************
#
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort {
    typeset MSG=$1

    echo
    echo "ERROR: ${MSG}"
    echo "aborting . . ."
    echo
    exit 255
}

#*******************************************************************************
#
# function: build_roboreg
#
# This function will build the environment.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function build_roboreg
{

read -d '' JSON_STRING <<ENDJSON
[
 { "ParameterKey": "FCIE","ParameterValue": "${ROBOREG_FICE}" },
 { "ParameterKey": "CINCPASSWD","ParameterValue": "${ROBOREG_CINCPASSWD}" },
 { "ParameterKey": "DockerImage","ParameterValue": "${DOCKER_IMAGE}" },
 { "ParameterKey": "SetUpLikePrd", "ParameterValue": "${SET_UP_LIKE_PRD}" },
 { "ParameterKey": "TagService","ParameterValue": "${TAG_SERVICE}" },
 { "ParameterKey": "TagContactNetid","ParameterValue": "${TAG_CONTACT_NETID}" },
 { "ParameterKey": "TagAccountNumber","ParameterValue": "${TAG_ACCOUNT_NUMBER}" },
 { "ParameterKey": "TagSubAccount","ParameterValue": "${TAG_SUBACCOUNT}" }
]
ENDJSON

aws cloudformation create-stack \
   --region us-west-2 \
   --stack-name "${CF_STACK_NAME}"  \
   --capabilities "CAPABILITY_IAM" \
   --template-body "file://../templates/roboreg_ecs.yaml" \
   --parameters "${JSON_STRING}" \
   --role-arn "${ROLE_ARN}" \
   --disable-rollback
}

#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
# -- set environment
#set_environment
START_TS=`${DATE}`
SET_UP_LIKE_PRD="N"

# -- parse command-line options
while getopts :p:z arguments
do
  case $arguments in
    z)
       SET_UP_LIKE_PRD="Y"
       ;;
   \?)
       print_usage
       echo
       error_abort "option -${OPTARG} is not a valid argument"
       exit 255
  esac
done

# validate input parameters
validate_arguments
build_roboreg
