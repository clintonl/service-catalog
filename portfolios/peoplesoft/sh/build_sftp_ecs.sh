#!/bin/bash
#*******************************************************************************
#
# TITLE......: build_sftp_ecs.sh
# PARAMETER..:
#    /INPUTS.:
#              -p  Pillar (HR,EL,SA)
#              -z  Set up like production
#
#*******************************************************************************

#*******************************************************************************
#
# function: validate_arguments
#
# This function parses the arguments from the command-line flags and makes sure
# that all necessary and required parameters have been passed to the program.
# If it encounters an issue, it aborts the program.
#
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function validate_arguments
{
# -- Get Account Name
AWS_ACCOUNT=`aws iam list-account-aliases|awk '{print $2}' 2>&1`

# -- make sure pillar is specified and is either EL, HR, or SA
pillar_list="EL HR SA"
good_pillar="N"
for curr_pillar in $pillar_list; do
  if [ "${UPPER_PILLAR}" == "$curr_pillar" ]; then
    good_pillar="Y"
  fi
done

if [ $good_pillar = "N" ]; then
  error_abort "Either a pillar wasn't passed or was invalid please use -p with the values EL, HR, or SA (${UPPER_PILLAR})" 
fi 
LOWER_PILLAR=${UPPER_PILLAR,,}

#  -- create Service Tag based on pillar
case ${UPPER_PILLAR} in
  EL)
    TAG_SERVICE="Uaccess Learning"
    ;;
  HR)
    TAG_SERVICE="Uaccess Employee"
    ;;
  SA)
    TAG_SERVICE="Uaccess Student"
    ;;
  *)
    error_abort "Could not determine Service Tag based on the pillar passed (${UPPER_PILLAR})"
    ;;
esac

case ${UPPER_PILLAR} in
  EL)
    TAG_ACCOUNT_NUMBER="1192621"
    TAG_SUBACCOUNT="Uaccess Learning"
    ;;
  HR)
    TAG_ACCOUNT_NUMBER="1192621"
    TAG_SUBACCOUNT="Uaccess Employee"
    ;;
  SA)
    TAG_ACCOUNT_NUMBER="Student Systems"
    TAG_SUBACCOUNT="Uaccess Student"
    ;;
  *)
    error_abort "Could not determine Account and Subaccount Tag based on the pillar passed (${UPPER_PILLAR})"
    ;;
esac

# -- this the NetID of the person most familiar with the resources being created, will default to kellehs for now
TAG_CONTACT_NETID="kellehs"

# -- CloudFormation Stack Name will be PeopleSoft+UPPER_PILLAR+UPPER_ENV
CF_STACK_NAME="PeopleSoft${UPPER_PILLAR}-SFTP-ECS"

# -- Role ARN will be different based on what account we are in
# -- Eventually we need to query this from AWS instead of hard coding
# -- The CF Template is bigger than 51,200 characters so we need to call it from
# -- an S3 bucket.  The template URL variable will hold the location of the S3 bucket
case ${AWS_ACCOUNT} in
  ua-uits-peoplesoft-nonprod)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
  # -- Set SFTP_ENV Variable
  if [ "$UPPER_PILLAR" = "EL" ]; then
    SFTP_ENV="ELM"
  else
    SFTP_ENV="${UPPER_PILLAR}_NONPROD"
  fi
    ;;
  ua-uits-peoplesoft-prod)
    ROLE_ARN="arn:aws:iam::242275737326:role/fdn-iam-CloudFormationAdminDeployerRole-IP2KC5OSU7DS"
  # -- Set SFTP_ENV Variable
  if [ "$UPPER_PILLAR" = "EL" ]; then
    SFTP_ENV="ELM"
  else
    SFTP_ENV=$UPPER_PILLAR
  fi
    ;;
  *)
    error_abort "Could not determine Role ARN based on the account (${AWS_ACCOUNT})"
    ;;
esac

# -- Set the Docker Image
case ${AWS_ACCOUNT} in
  ua-uits-peoplesoft-nonprod)
    #Per SAAWS-389 changed tag to 2018-06-25
    DOCKER_IMAGE="415418166582.dkr.ecr.us-west-2.amazonaws.com/eas/sftphub:2018-06-25"
    ;;
  ua-uits-peoplesoft-prod)
    #Per SAAWS-389 changed tag to 2018-06-25
    DOCKER_IMAGE="242275737326.dkr.ecr.us-west-2.amazonaws.com/eas/sftphub:2018-06-25"
    ;;
  *)
    error_abort "Could not determine Docker Image  based on the account (${AWS_ACCOUNT})"
    ;;
esac

# -- Get Elastic IPs and popualte the EIP1 and EIP2 variables.
# -- If one or both parameters do not exist then we will bass "none" in both

# -- First create the parameter name
EIP_PARAM="${UPPER_PILLAR}SFTPEIP1"
EIP1=`aws ssm get-parameter --name $EIP_PARAM --with-decryption --output text --query 'Parameter.[Value]' 2>&1`
# If this command returns a non 0 code then the parameter does not exist
# so we will default to none
if [ $? != 0 ]; then
  echo "$EIP_PARAM does not exist, will not use Elastic IPs for this SFTP Hub"
  EIP1="none"
  EIP2="none"
fi

# -- Now look for the second EIP
# -- First create the parameter name
if ! [ $EIP1 = "none" ]; then
  EIP_PARAM="${UPPER_PILLAR}SFTPEIP2"
  EIP2=`aws ssm get-parameter --name $EIP_PARAM --with-decryption --output text --query 'Parameter.[Value]' 2>&1`
  # If this command returns a non 0 code then the parameter does not exist
  # so we will default to none
  if [ $? != 0 ]; then
    echo "$EIP_PARAM does not exist, will not use Elastic IPs for this SFTP Hub"
    EIP1="none"
    EIP2="none"
  fi
fi

}

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage
{
echo "Usage: build_sftp_ecs.sh"
echo "Options:"
echo "       -p (required) = Pillar valid values are HR, SA, EL"
echo "       -z (optional) = Set up as though it were production (useful when testing in nonprod)"
}

#*******************************************************************************
#
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
typeset MSG=$1

echo
echo "ERROR: ${MSG}"
echo "aborting . . ."
echo
exit 255
}

#*******************************************************************************
#
# function: destroy_environment
#
# This function will destroy the environment. 
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function destroy_environment
{

# -- issue the delete stack command
aws cloudformation delete-stack \
   --region us-west-2 \
   --stack-name "${CF_STACK_NAME}"  \
   --role-arn "${ROLE_ARN}"

# -- if there was an error then abort
if [ $? -ne 0 ]; then
  error_abort "Problems deleting CloudFormation Stack ${CF_STACK_NAME}."
fi

# -- wait for delete to be complete, it will check every 30 seconds for an hour
# -- if delete does not complete by then it will throw an error
aws cloudformation wait stack-delete-complete\
   --region us-west-2 \
   --stack-name "${CF_STACK_NAME}" 

# -- if there was an error then try to delete again
# -- put this in place to attempt to fix the issue with the ECS Cluster
# -- not deleting becuase the EC2 instance has not been terminated yet
if [ $? -ne 0 ]; then
  # -- issue the delete stack command again
  aws cloudformation delete-stack \
     --region us-west-2 \
     --stack-name "${CF_STACK_NAME}"  \
     --role-arn "${ROLE_ARN}"

  # -- Wait for stack delete cammand to finish
  aws cloudformation wait stack-delete-complete\
     --region us-west-2 \
     --stack-name "${CF_STACK_NAME}" 

  #Now we can abort if the second delete did not work
  if [ $? -ne 0 ]; then
    error_abort "Problems waiting for the deleting of CloudFormation Stack ${CF_STACK_NAME}."
  fi

fi

}

#*******************************************************************************
#
# function: check_if_env_exists
#
# This funtion will check if the CloudFromation Tempalte exists.  If so, it will
# evaluate the DESTROY_IF_EXISTS vairable.  If Y(es) then the CloudFormation 
# Template will be destroyed, if N(o) then the program will stop with an error
# stating that the environment is already running.
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function check_if_env_exists
{
aws cloudformation describe-stacks --stack-name ${CF_STACK_NAME} 2>&1 |grep -q "arn:aws:cloudformation" &> /dev/null

# -- if the grep finds the string it will return a 0, otherwise it will return a 1
if [ $? == 0 ]; then 
  ENV_EXISTS="Y"
else  
  ENV_EXISTS="N"
fi

# -- if the env exists and -D (destroy env) was passed then destroy the environment
if [ "${ENV_EXISTS}" == "Y" ]; then
  error_abort "The CloudFormation Template ${CF_STACK_NAME} already exists...aborting."
fi
}

#*******************************************************************************
#
# function: build_sftp_hub
#
# This function will build the environment. 
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function build_sftp_hub
{

read -d '' JSON_STRING <<ENDJSON
[
 { "ParameterKey": "UppercaseSFTPEnv","ParameterValue": "${SFTP_ENV}"}, 
 { "ParameterKey": "LowercasePillar","ParameterValue": "${LOWER_PILLAR}" }, 
 { "ParameterKey": "DockerImage","ParameterValue": "${DOCKER_IMAGE}" }, 
 { "ParameterKey": "SetUpLikePrd", "ParameterValue": "${SET_UP_LIKE_PRD}" },
 { "ParameterKey": "TagService","ParameterValue": "${TAG_SERVICE}" },
 { "ParameterKey": "TagContactNetid","ParameterValue": "${TAG_CONTACT_NETID}" },
 { "ParameterKey": "TagAccountNumber","ParameterValue": "${TAG_ACCOUNT_NUMBER}" },
 { "ParameterKey": "TagSubAccount","ParameterValue": "${TAG_SUBACCOUNT}" },
 { "ParameterKey": "Eip1","ParameterValue": "${EIP1}" },
 { "ParameterKey": "Eip2","ParameterValue": "${EIP2}" }
]
ENDJSON

aws cloudformation create-stack \
   --region us-west-2 \
   --stack-name "${CF_STACK_NAME}"  \
   --capabilities "CAPABILITY_IAM" \
   --template-body "file://../templates/sftp_hub_ecs.yaml" \
   --parameters "${JSON_STRING}" \
   --role-arn "${ROLE_ARN}" \
   --disable-rollback
}

#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
# -- set environment
#set_environment
START_TS=`${DATE}`
SET_UP_LIKE_PRD="N"

# -- parse command-line options
while getopts :p:z arguments
do
  case $arguments in
    p)
       UPPER_PILLAR=${OPTARG^^}
       ;;
    #If -z is passed that means this will be set up like a PRD environment
    z) 
       SET_UP_LIKE_PRD="Y"
       ;;
   \?)
       print_usage
       echo
       error_abort "option -${OPTARG} is not a valid argument"
       exit 255
  esac
done

# validate input parameters
validate_arguments


# -- check to see if the environment is running
# -- if so if the DESTROY_IF_EXISTS = Y then destroy the environment first
# -- if not, then exit program with error
check_if_env_exists

# -- if we reach this point we can build the environment
build_sftp_hub
