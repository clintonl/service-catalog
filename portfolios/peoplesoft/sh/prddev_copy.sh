#!/bin/bash
#*******************************************************************************
#
# TITLE......: prddev_copy.sh
# PARAMETER..:
#    /INPUTS.:
#              -p  Pillar (HR,EL,SA)
#              -e  Environment (PRDDEV)
#
#*******************************************************************************

#*******************************************************************************
#
# function: validate_arguments
#
# This function parses the arguments from the command-line flags and makes sure
# that all necessary and required parameters have been passed to the program.
# If it encounters an issue, it aborts the program.
#
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function validate_arguments
{
# -- make sure pillar is specified and is either EL, HR, or SA
pillar_list="EL HR SA"
good_pillar="N"
for curr_pillar in $pillar_list; do
  if [ "${UPPER_PILLAR}" == "$curr_pillar" ]; then
    good_pillar="Y"
  fi
done

if [ $good_pillar = "N" ]; then
  error_abort "Either a pillar wasn't passed or was invalid please use -p with the values EL, HR, or SA (${UPPER_PILLAR})" 
fi 
LOWER_PILLAR=${UPPER_PILLAR,,}

#  -- create Service Tag based on pillar
case ${UPPER_PILLAR} in
  EL)
    TAG_SERVICE="Uaccess Learning"
    ;;
  HR)
    TAG_SERVICE="Uaccess Employee"
    ;;
  SA)
    TAG_SERVICE="Uaccess Student"
    ;;
  *)
    error_abort "Could not determine Service Tag based on the pillar passed (${UPPER_PILLAR})"
    ;;
esac

# -- accountnumber and subaccount tags will be based on Pillar
case ${UPPER_PILLAR} in
  EL)
    TAG_ACCOUNT_NUMBER="Human Resources Systems"
    TAG_SUBACCOUNT="Uaccess Learning"
    ;;
  HR)
    TAG_ACCOUNT_NUMBER="Human Resources Systems"
    TAG_SUBACCOUNT="Uaccess Employee"
    ;;
  SA)
    TAG_ACCOUNT_NUMBER="Student Systems"
    TAG_SUBACCOUNT="Uaccess Student"
    ;;
  *)
    error_abort "Could not determine Account and Subaccount Tag based on the pillar passed (${UPPER_PILLAR})"
    ;;
esac

# -- this the NetID of the person most familiar with the resources being created, will default to kellehs for now
TAG_CONTACT_NETID="kellehs"

# -- make sure an Environment was passed
if [ "${UPPER_ENV}" = "" ]; then
  error_abort "An environmnet was not passed, use -e and include the enviornment name i.e. DEV, TST, STG etc."
fi 
LOWER_ENV=${UPPER_ENV,,}

# -- make sure source database link was passed
if [ "${SOURCE_DB_LINK}" == "" ]; then
  error_abort "A source database link was not passed, use -d and include the source db linke (i.e. AWSELPRD"
fi

# -- CloudFormation Stack Name will be PeopleSoftRDS+UPPER_PILLAR+UPPER_ENV
CF_STACK_NAME="PeopleSoftRDS${UPPER_PILLAR}${UPPER_ENV}"

# -- Role ARN will be different based on what account we are in
# -- Eventually we need to query this from AWS instead of hard coding
case ${UPPER_ENV} in
  DEV)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    ;;
  PRDDEV)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    ;;
  TST)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    ;;
  STG)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    ;;
  SUP)
    ROLE_ARN="arn:aws:iam::242275737326:role/fdn-iam-CloudFormationAdminDeployerRole-IP2KC5OSU7DS"
    ;;
  PRD)
    ROLE_ARN="arn:aws:iam::242275737326:role/fdn-iam-CloudFormationAdminDeployerRole-IP2KC5OSU7DS"
    ;;
  *)
    error_abort "Could not determine Role ARN based on the target environment passed (${UPPER_ENV})"
    ;;
esac

# -- DB Multi Zone will default to false 
DB_MULTI_ZONE="false"

# -- DB Inst Class must be at least db.t2.large for encryption to work
case ${UPPER_PILLAR} in
  EL)
    DB_INST_CLASS="db.t2.large"
    ;;
  *)
    error_abort "Could not determine DB Inst Class based on the pillar passed (${UPPER_PILLAR})"
    ;;
esac

# -- DB Storage Type will be gp2 (general purpose) for all DBs for now
# -- Will go provisioned with HR and SA
DB_STORAGE_TYPE="gp2"

#######################
#Popultate DB_INSTANCE and Snapshot Variables
#######################
# -- make sure the Pillar/Environment exists
# -- find the most recent RDS snapshot for the Pillar/Source Env passed
DB_INSTANCE="peoplesoft-${LOWER_PILLAR}${LOWER_ENV}"
DB_TNS_NAME="AWS${UPPER_PILLAR}${UPPER_ENV}"

# -- for now, we are going to hard code the snapshot name given.
# -- -after-refresh is the snapshot taken after the MV refresh was complete, but before the MVs were dropped
# -- -after-mvs-gone is the snapshot taken after the MV refresh and the MVs were remove
DB_SNAPSHOT_W_MVS="${DB_INSTANCE}-after-refresh"
DB_SNAPSHOT_WO_MVS="${DB_INSTANCE}-after-mvs-gone"

# -- check to see if -after-refresh snapshot exists
aws rds describe-db-snapshots --db-instance-identifier ${DB_INSTANCE} --db-snapshot-identifier ${DB_SNAPSHOT_W_MVS} | grep ${DB_SNAPSHOT_W_MVS} &> /dev/null

# -- if the snapshot file was not populated with data then we have an issue and have to abort
if ! [ "$?" == "0" ]; then
  error_abort "An RDS Snapshot does not exist for the DB Instance ${DB_INSTANCE} with the snapshot name of ${DB_SNAPSHOT_W_MVS}"
fi

# -- check to see if a snapshot without MVs exists and update the variable DB_SNAPSHOT_WO_MVS_EXISTS
aws rds describe-db-snapshots --db-instance-identifier ${DB_INSTANCE} --db-snapshot-identifier ${DB_SNAPSHOT_WO_MVS} | grep ${DB_SNAPSHOT_WO_MVS} &> /dev/null

# -- if the snapshot file was not populated with data then we have an issue and have to abort
if ! [ "$?" == "0" ]; then
  DB_SNAPSHOT_WO_MVS_EXISTS="N"
else
  DB_SNAPSHOT_WO_MVS_EXISTS="Y"
fi

# -- check to see if the RDS instance is up, if the output from these command contains
# -- "DBInstanceNotFound" then the DB is not up
aws rds describe-db-instances --db-instance-identifier ${DB_INSTANCE} 2>&1 | grep DBInstanceNotFound &> /dev/null

# -- if the grep command returns a non-zero then the DB Intance is up and we need to abort
if ! [ "$?" == "0" ]; then
  error_abort "The RDS instance ${DB_INSTANCE} is already up and running, it should be down before this runs.  Check it out."
fi

}

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage
{
echo "Usage: prddev_copy.sh"
echo "Options:"
echo "       -p (required) = Pillar valid values are HR, SA, EL"
echo "       -e (required) = Environment including but not limited to PRDDEV"
}

#*******************************************************************************
#
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
typeset MSG=$1

echo
echo "ERROR: ${MSG}"
echo "aborting . . ."
echo
exit 255
}

#*******************************************************************************
#
# function: build_rds_instance
#
# Will build and RDS instance using CloudFormation
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function build_rds_instance
{

read -d '' JSON_STRING <<ENDJSON
[
 { "ParameterKey": "PillarLowerCase","ParameterValue": "${LOWER_PILLAR}"}, 
 { "ParameterKey": "EnvironmentLowerCase","ParameterValue": "${LOWER_ENV}" }, 
 { "ParameterKey": "PillarUpperCase","ParameterValue": "${UPPER_PILLAR}" }, 
 { "ParameterKey": "EnvironmentUpperCase","ParameterValue": "${UPPER_ENV}" }, 
 { "ParameterKey": "DBSnapshotID","ParameterValue": "${DB_SNAPSHOT_W_MVS}" }, 
 { "ParameterKey": "DBMultiAz","ParameterValue": "${DB_MULTI_ZONE}" }, 
 { "ParameterKey": "DBInstanceClass","ParameterValue": "${DB_INST_CLASS}" }, 
 { "ParameterKey": "DBStorageType","ParameterValue": "${DB_STORAGE_TYPE}" },
 { "ParameterKey": "TagService","ParameterValue": "${TAG_SERVICE}" },
 { "ParameterKey": "TagContactNetid","ParameterValue": "${TAG_CONTACT_NETID}" },
 { "ParameterKey": "TagAccountNumber","ParameterValue": "${TAG_ACCOUNT_NUMBER}" },
 { "ParameterKey": "TagSubAccount","ParameterValue": "${TAG_SUBACCOUNT}" }
]
ENDJSON

aws cloudformation create-stack \
   --region us-west-2 \
   --stack-name "${CF_STACK_NAME}"  \
   --capabilities "CAPABILITY_IAM" \
   --template-body "file://~/git/service-catalog/portfolios/peoplesoft/templates/peoplesoft_rds_instance.yaml" \
   --parameters "${JSON_STRING}" \
   --role-arn "${ROLE_ARN}" \
   --disable-rollback &> $log_file

if [ $? -ne 0 ]; then
  cat $log_file
  error_abort "Problems waiting for the creating of CloudFormation Stack ${CF_STACK_NAME}."
fi

#   --parameters "file:///tmp/parameters.json" \
# -- wait for create to complete, it will check every 30 seconds for an hour
# -- if create does not complete by then it will throw an error
aws cloudformation wait stack-create-complete \
   --region us-west-2 \
   --stack-name "${CF_STACK_NAME}" &> $log_file

# -- if there was an error then abort
if [ $? -ne 0 ]; then
  cat $log_file
  error_abort "Problems waiting for the creating of CloudFormation Stack ${CF_STACK_NAME}."
fi

}

#*******************************************************************************
#
# function: refresh_mvs
#
# This function will run the MV Refresh process on the target database
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function refresh_mvs
{

# -- run mv_replicate.refresh_mv process on targert DB 
sqlplus -s /@${DB_TNS_NAME} << EOF-SQL > $log_file 2>&1
set feed off timing off heading off pagesize 0
begin
  mv_replicate.refresh_mv('SYSADM','${SOURCE_DB_LINK}');
end;
/
EOF-SQL

# -- if the there was an error exit with error
# -- this will return 0 if there is an ORA- error and 1 if there is not
egrep "ORA-" $log_file &> /dev/null 2>&1
if [ "$?" == "0" ]; then
  echo "Error running mv_replicate.refresh_mv-"
  cat $log_file
  exit 255
fi

}

#*******************************************************************************
#
# function: create_new_snapshot_w_mvs
#
# This function with create a new snapshot after the MV refresh is complete
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function create_new_snapshot_w_mvs
{

# -- first delete snapshot
# -- if we have made it here we know that the snapshot already exists
aws rds delete-db-snapshot --db-snapshot-identifier ${DB_SNAPSHOT_W_MVS} &> $log_file 

if ! [ "${?}" == "0" ]; then
  cat $log_file
  error_abort "There was an error encountered while deleting the snapshot ${DB_SNAPSHOT_W_MVS}"
fi

# -- now create a new version of the snapshot
aws rds create-db-snapshot \
--db-snapshot-identifier ${DB_SNAPSHOT_W_MVS} \
--db-instance-identifier ${DB_INSTANCE} &> $log_file

if ! [ "${?}" == "0" ]; then
  cat $log_file
  error_abort "There was an error encountered while creating the snapshot ${DB_SNAPSHOT_W_MVS}"
fi

# -- wait for the snapshot creation to complete
WAIT_RETURN_CODE=1
until [ "${WAIT_RETURN_CODE}" = "0" ]; do
  aws rds wait db-snapshot-completed \
  --db-snapshot-identifier ${DB_SNAPSHOT_W_MVS} \
  --db-instance-identifier ${DB_INSTANCE} &> $log_file

  WAIT_RETURN_CODE=${?}

  if ! [ "${WAIT_RETURN_CODE}" == "0" ]; then
    # -- if "Max attempts exceeded" exists in the log file we will continue to wait, otherwise we'll throw an error
    # -- if the grep returns 0 then we will just continue to wait, otherwise we'll throw an error
    grep "Max attempts exceeded" $log_file &> /dev/null
    if ! [ "${?}" == "0" ]; then
      cat $log_file
      error_abort "There was an error encountered while waiting for the creation of  the snapshot ${DB_SNAPSHOT_W_MVS}"
    fi
  fi
done

}

#*******************************************************************************
#
# function: remove_mvs
#
# This function with create a new snapshot after the MV refresh is complete
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function remove_mvs
{

# -- run mv_replicate.remove_mv process on targert DB 
sqlplus -s /@${DB_TNS_NAME} << EOF-SQL > $log_file 2>&1
set feed off timing off heading off pagesize 0
begin
  mv_replicate.remove_mv('PS');
  mv_replicate.remove_mv('SYSADM');
end;
/
EOF-SQL

# -- if the there was an error exit with error
# -- this will return 0 if there is an ORA- error and 1 if there is not
egrep "ORA-" $log_file &> /dev/null 2>&1
if [ "$?" == "0" ]; then
  echo "Error running mv_replicate.remove_mv-"
  cat $log_file
  exit 255
fi

}

#*******************************************************************************
#
# function: create_new_snapshot_wo_mvs
#
# This function with create a new snapshot after the MV refresh is complete
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function create_new_snapshot_wo_mvs
{

# -- first delete snapshot if it exists
if [ "${DB_SNAPSHOT_WO_MVS_EXISTS}" == "Y" ]; then
  aws rds delete-db-snapshot --db-snapshot-identifier ${DB_SNAPSHOT_WO_MVS} &> $log_file 

  if ! [ "${?}" == "0" ]; then
    cat $log_file
    error_abort "There was an error encountered while deleting the snapshot ${DB_SNAPSHOT_WO_MVS}"
  fi
fi

# -- now create a new version of the snapshot
aws rds create-db-snapshot \
--db-snapshot-identifier ${DB_SNAPSHOT_WO_MVS} \
--db-instance-identifier ${DB_INSTANCE} &> $log_file

if ! [ "${?}" == "0" ]; then
  cat $log_file
  error_abort "There was an error encountered while creating the snapshot ${DB_SNAPSHOT_WO_MVS}"
fi

# -- wait for the snapshot creation to complete
WAIT_RETURN_CODE=1
until [ "${WAIT_RETURN_CODE}" = "0" ]; do
  aws rds wait db-snapshot-completed \
  --db-snapshot-identifier ${DB_SNAPSHOT_WO_MVS} \
  --db-instance-identifier ${DB_INSTANCE} &> $log_file

  WAIT_RETURN_CODE=${?}

  if ! [ "${WAIT_RETURN_CODE}" == "0" ]; then
    # -- if "Max attempts exceeded" exists in the log file we will continue to wait, otherwise we'll throw an error
    # -- if the grep returns 0 then we will just continue to wait, otherwise we'll throw an error
    grep "Max attempts exceeded" $log_file &> /dev/null
    if ! [ "${?}" == "0" ]; then
      cat $log_file
      error_abort "There was an error encountered while waiting for the creation of  the snapshot ${DB_SNAPSHOT_WO_MVS}"
    fi
  fi
done

}

#*******************************************************************************
#
# function: delete_rds_instance 
#
# This function will delete the CloudFormation template that built this RDS Intance
# 
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function delete_rds_instance
{

# -- issue the delete stack command
aws cloudformation delete-stack \
   --region us-west-2 \
   --stack-name "${CF_STACK_NAME}"  \
   --role-arn "${ROLE_ARN}" &> $log_file

# -- if there was an error then abort
if [ $? -ne 0 ]; then
  cat $log_file
  error_abort "Problems deleting CloudFormation Stack ${CF_STACK_NAME}."
fi

# -- wait for delete to be complete, it will check every 30 seconds for an hour
# -- if delete does not complete by then it will throw an error
aws cloudformation wait stack-delete-complete \
   --region us-west-2 \
   --stack-name "${CF_STACK_NAME}" &> $log_file

# -- if there was an error then abort
if [ $? -ne 0 ]; then
  cat $log_file
  error_abort "Problems waiting for the deleting of CloudFormation Stack ${CF_STACK_NAME}."
fi

}

#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
# -- set environment
#set_environment
START_TS=`${DATE}`

# -- parse command-line options
while getopts :p:e:d: arguments
do
  case $arguments in
    p)
       UPPER_PILLAR=${OPTARG^^}
       ;;
    e)
       UPPER_ENV=${OPTARG^^}
       ;;
    d)
       SOURCE_DB_LINK=${OPTARG^^}
       ;;
   \?)
       print_usage
       echo
       error_abort "option -${OPTARG} is not a valid argument"
       exit 255
  esac
done

# -- log/work file
log_file="/tmp/prddev_log.out"

# -- validate input parameters
validate_arguments

# -- create RDS Instance
build_rds_instance

# -- run refresh
refresh_mvs

# -- create new version of snapshot with MVs
create_new_snapshot_w_mvs

# -- remove mvs now that there is a snapshot of the database after the latest refresh
remove_mvs

# -- create new version of snapshot without MVs
create_new_snapshot_wo_mvs

# -- finally, delete the RDS Intance
delete_rds_instance 

# -- remove log/work file
rm $log_file
