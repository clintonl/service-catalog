#!/bin/bash
#*******************************************************************************
#
# TITLE......: pull_custom_repo.sh
# PARAMETER..:
#     INPUTS.: -b  branch (development, test, stage, or production)
#
# --This script requires that the jq package is installed, used to parse the region
#   yum install -y jq
#
#*******************************************************************************

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage
{
echo "Usage: pull_custom_repo.sh"
echo "Options:"
echo "       -b (required) = branch (development, test, stage, or production)"
}

#*******************************************************************************
#
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
typeset MSG=$1

# -- Send a message to the #met-errors channel
if ! [ -z "$SLACK_URL" ]; then
  SLACK_CHANNEL=$(aws ssm get-parameter --name "SLACK_WARNING_CHANNEL" --with-decryption --output text --region $REGION --query 'Parameter.[Value]' 2>&1)
  SLACK_MSG="Error in $AWS_ACCOUNT running pull_custom_repos.sh - Branch=$BB_BRANCH ($MSG)"
  JSON="{ \"channel\": \"$SLACK_CHANNEL\", \"text\": \"$SLACK_MSG\" }"
  OUTPUT=$(curl -s -d "payload=$JSON" "$SLACK_URL" 2>&1)
fi

echo
echo "ERROR: ${MSG}"
echo "aborting . . ."
echo
exit 255
}

#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
START_TS=`${DATE}`

# -- parse command-line options
while getopts :b: arguments
do
  case $arguments in
    b)
       BB_BRANCH=${OPTARG,,}
       ;;
   \?)
       print_usage
       echo
       error_abort "option -${OPTARG} is not a valid argument"
       exit 255
  esac
done

#Grab region
REGION=$(curl -s http://169.254.169.254/latest/dynamic/instance-identity/document | jq -r .region)

# -- Get Account Name
AWS_ACCOUNT=$(aws iam list-account-aliases --region $REGION --output text --query 'AccountAliases[0]' 2>&1)

# -- Get Slack URL for messages
SLACK_URL=$(aws ssm get-parameter --name "SLACK_ALERT_URL" --with-decryption --output text --region $REGION --query 'Parameter.[Value]' 2>&1)
if ! [ "$?" = "0" ]; then
  unset SLACK_URL
fi

#Create a variable with a list of available Bitbucket Branches
BRANCH_LIST="development test stage support production tea teb tec trn demo gdr fnc dev cnv gld pdv pts pqa pcp pcd sup prd"

#Check to see if input is valid
#If no branch was passed then raise an error
if [ -z $BB_BRANCH ]; then
  error_abort "A branch was not passed, please use -b to pass a branch name"
fi

#Validate the branch passed exists in the BRANCH_LIST
good_branch="N"
for curr_branch in $BRANCH_LIST; do
  if [ $BB_BRANCH == $curr_branch ]; then
    good_branch="Y"
  fi
done

if [ $good_branch = "N" ]; then
  echo "Invalid Branch passed ($BB_BRANCH), should be one of ($BRANCH_LIST)"
  exit 0
fi

#Make sure there are instances with the tag bb_branch = the $BB_BRANCH passed
#If there is no instances, then just quit
#This command will find the instance ids for all intances with the bb_branch = $BB_BRANCH
OUTPUT=$(aws ec2 describe-instances --filters "Name=tag:bb_branch,Values=$BB_BRANCH" --region $REGION | grep $BB_BRANCH 2>&1)
#If we did not find any instances then exit zero with a nice message
if [ "$?" != 0 ]; then
  echo "No instances with a Bitbucket Branch = $BB_BRANCH"
  echo "nothing to do here"
  exit 0
fi

COMMAND_ID=$(aws ssm send-command --document-name "AWS-RunShellScript" --targets '{"Key":"tag:bb_branch","Values":["'$BB_BRANCH'"]}' --max-concurrency "10" --parameters $'{"commands":["CONTAINER_ID=$(docker ps|grep app-batch|awk \'{print $1}\')","[ -z \\\"${CONTAINER_ID}\\\" ] || for this_container in $CONTAINER_ID; do docker exec $this_container sudo -u psadm1 -E /bin/bash /home/psadm1/psadm1_install.sh; done"],"executionTimeout":["3600"]}' --timeout-seconds 600 --region $REGION --output text --query "Command.CommandId" 2>&1)

if [ $? != 0 ]; then
  error_abort "Error with asm ssm send-command ($COMMAND_ID)"
fi

#Loop to check status
while true
do
  STATUS=$(aws ssm list-commands --command-id $COMMAND_ID --region $REGION --output text --query "Commands[*].StatusDetails" 2>&1)
  if [ $? != 0 ]; then
    error_abort "Error checking status of command ($STATUS)"
  fi

  case $STATUS in
    "Pending")
      sleep 5
      ;;
    "InProgress")
      sleep 5
      ;;
    "Success")
      break
      ;;
    *)
      error_abort "Command did not succeed ($STATUS)"
      ;;
  esac
done

# -- Send a message to the #hr-messages channel
if ! [ -z "$SLACK_URL" ]; then
  SLACK_CHANNEL=$(aws ssm get-parameter --name "SLACK_MESSAGE_CHANNEL" --with-decryption --output text --region $REGION --query 'Parameter.[Value]' 2>&1)
  SLACK_MSG="The $BB_BRANCH branch has been synced on all app servers in $AWS_ACCOUNT"
  JSON="{ \"channel\": \"$SLACK_CHANNEL\", \"text\": \"$SLACK_MSG\" }"
  OUTPUT=$(curl -s -d "payload=$JSON" "$SLACK_URL" 2>&1)
fi

