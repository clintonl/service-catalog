#!/bin/bash
#*******************************************************************************
#
# TITLE......: sync_prod_copy.sh
# PARAMETER..:
#    /INPUTS.:
#              -p  Pillar (HR,EL,SA)
#
#*******************************************************************************

#*******************************************************************************
#
# function: print_usage
#
# This function will print the usage arguments for the program.
#
# INPUTS:  none
#
# RETURNS: none
#
#*******************************************************************************
function print_usage
{
echo "Usage: sync_prod_copy.sh"
echo "Options:"
echo "       -p (required) = Pillar valid values are HR, SA, EL"
}

#*******************************************************************************
#
# function: error_abort
#
# This function serves as the "bootstrap" error handler that is called during
# the initial setup, configuration, and initialization of the script, at which
# time the standard logging and handling facilities are not yet on-line.  When
# this functino is called, it simply echoes the messages passed to it to STDOUT
# and then immediate terminates program execution (unconditionally).  It
# should be called to terminate the program when a key setup step cannot be
# performed and processing cannot normally continue.
#
# INPUTS:  first argument - the message text to pass to STDOUT
#
# RETURNS: exists the program with a return code of 255
#
#*******************************************************************************
function error_abort
{
typeset MSG=$1

# -- Send a message to the #met-alerts channel
SLACK_CHANNEL="met-errors"
SLACK_MSG="Error in sync_prod_copy.sh - $MSG"
JSON="{ \"channel\": \"$SLACK_CHANNEL\", \"text\": \"$SLACK_MSG\" }"
OUTPUT=$(curl -s -d "payload=$JSON" "$SLACK_URL" 2>&1)

echo
echo "ERROR: ${MSG}"
echo "aborting . . ."
echo
exit 255
}

#*******************************************************************************
#
# ---------- MAIN ----------
#
#*******************************************************************************
# -- set environment
#set_environment
START_TS=`${DATE}`

# -- parse command-line options
while getopts :p: arguments
do
  case $arguments in
    p)
       LOWER_PILLAR=${OPTARG,,}
       ;;
   \?)
       print_usage
       echo
       error_abort "option -${OPTARG} is not a valid argument"
       exit 255
  esac
done

# -- make sure pillar is specified and is either el, hr, or sa
pillar_list="el hr sa"
good_pillar="N"
for curr_pillar in $pillar_list; do
  if [ "${LOWER_PILLAR}" = "$curr_pillar" ]; then
    good_pillar="Y"
  fi
done

if [ $good_pillar = "N" ]; then
  error_abort "Either a pillar wasn't passed or was invalid please use -p with the values el, hr, or sa (${LOWER_PILLAR})"
fi

# -- Get Slack URL for messages
SLACK_URL=$(aws ssm get-parameter --name "SlackURL" --with-decryption --output text --query 'Parameter.[Value]' 2>&1)
if ! [ "$?" = "0" ]; then
  unset SLACK_URL
fi

# -- Git directory where development repo exists
GIT_DIR="$HOME/git/${LOWER_PILLAR}-dev"
PROD_REMOTE="prod"
PROD_REPO="git@bitbucket.org:uapeoplesoft/${LOWER_PILLAR}-prd.git"

# -- Make sure GIT_DIR exists
if [ ! -d "$GIT_DIR" ]; then
  error_abort "The git directory $GIT_DIR does not exist"
fi

# -- Check to see if there is a remote set up called prod
# -- that is pointing to prd repo in bitbucket
OUTPUT=$(git -C $GIT_DIR remote -v|grep ${PROD_REMOTE}|grep ${PROD_REPO} 2>&1)
if ! [ "$?" = "0" ]; then
  OUTPUT=$(git -C $GIT_DIR remote add $PROD_REMOTE $PROD_REPO 2>&1)
  # -- If the remote could not be created then throw an error
  if ! [ "$?" = "0" ]; then
    error_abort "Error occurred attempting to create remote $PROD_REMOTE ($OUTPUT)"
  fi
fi

# -- Checkout the production-copy branch of the dev repo
OUTPUT=$(git -C $GIT_DIR checkout production-copy 2>&1)
if ! [ "$?" = "0" ]; then
  error_abort "Error occurred attempting to checkout production-copy branch ($OUTPUT)"
fi

# -- Perform a pull on the dev repo to ensure it's in sync with bitbucket
OUTPUT=$(git -C $GIT_DIR pull 2>&1)
if ! [ "$?" = "0" ]; then
  error_abort "Error occurred attempting to pull from the $GIT_DIR directory ($OUTPUT)"
fi

# -- Now pull the production branch of the prod repo into production-copy
OUTPUT=$(git -C $GIT_DIR pull $PROD_REMOTE production --no-edit 2>&1)
if ! [ "$?" = "0" ]; then
  error_abort "Error occurred attempting to pull production branch into production-copy ($OUTPUT)"
fi

# -- Final step is to push the changes into bitbucket
OUTPUT=$(git -C $GIT_DIR push 2>&1)
if ! [ "$?" = "0" ]; then
  error_abort "Error occurred attempting to push production-copy into bitbucket ($OUTPUT)"
fi

# -- Send a message to the #met-alerts channel
SLACK_CHANNEL="met-notices"
SLACK_MSG="Successfully synced production-copy branch in $GIT_DIR"
JSON="{ \"channel\": \"$SLACK_CHANNEL\", \"text\": \"$SLACK_MSG\" }"
OUTPUT=$(curl -s -d "payload=$JSON" "$SLACK_URL" 2>&1)
