#!/bin/bash


# -- Get Account Name
AWS_ACCOUNT=`aws iam list-account-aliases --output text | awk '{print $2}'`

case ${AWS_ACCOUNT} in
  ua-uits-peoplesoft-nonprod)
    ROLE_ARN="arn:aws:iam::415418166582:role/fdn-iam-CloudFormationAdminDeployerRole-HG66CRA6T5RF"
    ;;
  ua-uits-peoplesoft-prod)
    ROLE_ARN="arn:aws:iam::242275737326:role/fdn-iam-CloudFormationAdminDeployerRole-IP2KC5OSU7DS"
    ;;
  *)
    error_abort "Could not determine Role ARN based on the account (${AWS_ACCOUNT})"
    ;;
esac

aws cloudformation update-stack \
   --region us-west-2 \
   --stack-name "PeopleSoftSG"  \
   --capabilities "CAPABILITY_IAM" \
   --template-body "file://../templates/peoplesoft_security_groups.yaml" \
   --role-arn "${ROLE_ARN}" 

