# Route53 Hosted Zone CloudFormation Template

This template creates a Route53 Hosted zone.

## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
