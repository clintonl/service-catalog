# Static S3 Website

The Simple Static Web Site or SSWS is a blueprint that lays the foundation for deploying a static website or 
content to S3 and making it publicly available. This will fit the use case when you want to publish static 
content quickly and easily.

This will be made available via the AWS Service Catalog.

## Goals
* Provide a standard way to deploy a public S3 backed website to host static content
* Provide IAM identities that allow the management of the bucket
* Provide a standard url that can be used to access the site
* Allow individual implementations to be deployed via CloudFormation stacks

## Foundation/Installation

To deploy the foundation for this blueprint:

1. Create a CloudFormation stack using the ssws-foundation.json template. 

This will create an IAM Role that can be assumed by Service Catalog to deploy the S3 bucket and related IAM User, 
Policy, and access credentials to manage bucket contents. The foundation CloudFormation stack will also create an IAM 
Group SSWSServiceGroupUsers.

2. Create the default hosted Zone. 

Note: This has already been done in the pilots account and ua-erp account. If this blueprint is being deployed in a
different account, the cf templates will need to be updated to add the default hosted zone for the account.

3. Make sure IAM Policy is correct for users to get into Service Catalog. 

There is a default end user Service Catalog policy that can be assigned to the users or groups who you would like 
to give access to Service Catalog.

## Usage

Users that have access to Service Catalog and are assigned to the SSWSServiceGroupUsers group will be able to search for
Static S3 Website product and launch it via the Service Catalog in the AWS Console. At the time when this product was
created, there is a limitation where CloudFormation does not propagate tags to nested stacks. For this reason, the
owner, netid, and projectname must be entered in the Parameters screen and manually added to the tags screen. These tags
are required and used for tracking and cost management.

Here is a detailed step by step to launch this product:

1. Log into the AWS Console and go to Service Catalog
2. Search for Static S3 Website, select it, and click launch.
3. Add a stack name that will allow you to identify it and click next.
4. Add a site name (lowercase alpha characters only). This will end up being used to generate the URL and DNS records
for your site.
5. Fill in the owner, netid, and project name tags under cost management and click next.
6. Manually add the tags owner, netid, and projectname on the tags screen and populate the same values as the Parameters
screen.
7. Review the information and click Launch. 
8. Once the stack if finished launching, you will be presented with the site URL, the bucket name, and access
credentials in the output of the stack. This can be used to access and manage content on the site.

That's it. You are done!

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
