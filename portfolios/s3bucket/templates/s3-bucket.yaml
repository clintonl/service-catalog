---
# S3 Bucket CloudFormation Deployment
# -----------------------------------------
# 
# This CloudFormation template will deploy an S3 bucket with it's own IAM user.

AWSTemplateFormatVersion: '2010-09-09'

# Parameters
# ----------
#
# These are the input parameters for this template. All of these parameters
# must be supplied for this template to be deployed.
Parameters:
  # The name of the bucket.
  BucketName:
    Type: String
    Description: The name of the S3 Bucket.

  # #### Tags
  #
  # The following tags are applied to all resources created by this template.
  TagService:
    Description: Service name (from the service catalog) that is utilizing this resource
    Type: String
  TagEnvironment:
    Description: Type of environment that is using this resource, such as 'dev', 'tst', 'prd'. 
    Type: String
  TagContactNetId:
    Description: NetID of the person to contact for information about this resource
    Type: String
  TagAccountNumber:
    Description: Financial system account number for the service utilizing this resource
    Type: String
  TagSubAccount:
    Description: Financial system subaccount number for the service utilizing this resource
    Type: String
  TagTicketNumber:
    Description: Ticket number that this resource is for
    Type: String

# Metadata
# --------
#
# Metadata is mostly for organizing and presenting Parameters in a better way
# when using CloudFormation in the AWS Web UI.
Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
    - Label:
        default: S3 Bucket Configuration
      Parameters:
      - BucketName
    - Label:
        default: Tagging and Cost Management
      Parameters:
      - TagService
      - TagEnvironment
      - TagCreatedBy
      - TagContactNetId
      - TagAccountNumber
      - TagSubAccount
      - TagTicketNumber
    ParameterLabels:
      BucketName:
        default: 'Bucket Name:'

# Resources
# ---------
#
# These are all of the resources deployed by this template.
#
Resources:

  # #### S3 Bucket    
  #
  # This deploys the S3 bucket with some tags.
  S3Bucket:
    Type: AWS::S3::Bucket
    Properties:
      BucketName: !Ref "BucketName"
      AccessControl: Private
      Tags:
      - Key: Name
        Value: !Sub "${BucketName}-s3"
      - Key: service
        Value: !Ref TagService
      - Key: environment
        Value: !Ref TagEnvironment
      - Key: contactnetid
        Value: !Ref TagContactNetId
      - Key: accountnumber
        Value: !Ref TagAccountNumber
      - Key: subaccount
        Value: !Ref TagSubAccount
      - Key: ticketnumber
        Value: !Ref TagTicketNumber

  # #### S3 Bukcet User
  #
  # Creates an IAM user that can only connect to the S3 bucket specified.
  S3BucketUser:
    Type: AWS::IAM::User
    Properties:
      Path: "/"
      Policies:
      - PolicyName: giveaccesstobucketonly
        PolicyDocument:
          Version: '2012-10-17'
          Statement:
          - Effect: Allow
            Action:
            - s3:List*
            Resource:
            - "*"
          - Effect: Allow
            Action:
            - s3:*
            Resource: !Sub "arn:aws:s3:::${S3Bucket}/*"

# Outputs
# ---------
#
# Output values that can be viewed from the AWS CloudFormation console.
#
Outputs:
  BucketName:
    Value: !Ref S3Bucket
