# SFTP Hub OpsWorks Stack

This CloudFormation template creates an SFTP Hub Application.

The basic idea is that a single EC2 instance will mount a set of EFS volumes, and those
volumes will then be available within a docker container running an SFTP server.

## Pre-Requisites

This template assumes it will be deployed into a standard UITS VPC with Output Exports
available for the VPC ID, public and private subnets.

## Parameters

* VPC stack name (for importing Exported values from)

## Outputs



## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
