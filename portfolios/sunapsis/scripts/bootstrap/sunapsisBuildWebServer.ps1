# dbaty, 08/17/2017
# Sunapsis - WebServer bootstrap
# sunapsisBuildWebServer.ps1
# Written for PowerShell 4.0
# Bootstrap script for a Sunapsis Web Server

##### Variables

# Root folders for various items
$rootAppZips = "D:\AppZips"
$rootBatchFiles = "D:\BatchFiles"
$rootColdFusion = "C:\ColdFusion2016"
$rootCustomizations = "D:\Customizations"
$rootCustomizationsScripts = $rootCustomizations + "\scripts"
$rootLogFiles = "D:\LogFiles"
$rootScripts = "D:\Scripts"
$rootShib = "D:\opt\shibboleth-sp\etc\shibboleth"
$rootShib = "D:\opt\shibboleth-sp\etc\shibboleth"
$rootSunapsis = "D:\Sunapsis\wwwroot"

# S3 keys (bucket name comes from user data sctipt vis-a-vis a CloudFormation parameter)
$s3keyColdFusionCustomizations = "ColdFusionCustomizations\"
$s3keyAppEncryption = "AppEncryption\"
$s3keyAppZips = "AppZips\"
$s3keyCFConfig = "ColdFusionConfig\"
$s3keyCerts = "Certs\"
$s3keyCustomizations = "Customizations\"
$s3keySSM = "SSM\"
$s3keyScripts = "Scripts\"
$s3keySevisCerts = "SEVIS\Certs\"
$s3keySftp = "SftpKeys\"
$s3keyShib = "Shibboleth\"
$s3keySoftware = "Software\"

# Retrieve EC2 instance data from instance metadata
$ec2instanceAZ = Invoke-RestMethod -Uri "http://169.254.169.254/latest/meta-data/placement/availability-zone"
$ec2instanceId = Invoke-RestMethod -Uri "http://169.254.169.254/latest/meta-data/instance-id"
$ec2instanceIP = Invoke-RestMethod -Uri "http://169.254.169.254/latest/meta-data/local-ipv4"

# Update S3 key for ColdFusion config files  as we need them to be different depending on the instance's AZ
$s3keyCFConfig += $ec2instanceAZ + "\"

# Retrieve Route53 hosted zone name from CFN stack export then look up ID
$hostedZoneName = ((Get-CFNStack -StackName "sunapsis-$($environmentName)-s3").Outputs).Where({$_.OutputKey -Eq "HostedDNSZoneNameExport"}).OutputValue
$hostedZoneId = (Get-R53HostedZones | Where Name -Eq $hostedZoneName).Id

# Build file share UNC root
$rootFileShare = "\\sunapsis-$($environmentName)-files.$($hostedZoneName)\sunapsis"

# Build host header value for website (dynamic based on environment type)
$hostHeaderRoot = "uainternational.arizona.edu"
$hostHeader = Switch ($environmentName)
	{
		"prd" {"$($hostheaderRoot)"; break}
		"tst" {"test.$($hostheaderRoot)"; break}
		default {"$($environmentName).$($hostheaderRoot)"; break}
	}

# Folder for IIS logs
$iisLogFolder = "D:\IISLogs"

# Array containing Windows features we want installed
$windowsFeatures = @("Web-WebServer", # Internet Information Services (Web Server)
                     "Web-Mgmt-Console", # IIS Management Console
                     "Web-ISAPI-Ext", # ISAPI Extensions
                     "Web-ISAPI-Filter", # ISAPI Filters
                     "Web-Http-Redirect", # HTTP Redirection
                     "Web-CGI", # CGI
                     "Web-Net-Ext45", # .NET 4.5 Extensibility
                     "Web-Asp-Net45") # ASP.NET 4.5

# Hash table of MSI packages we want to install
$msiInstalls = @{}
$msiInstalls.Add("Shibboleth", "http://shibboleth.net/downloads/service-provider/2.6.0/win64/shibboleth-sp-2.6.0.1-win64.msi")
$msiInstalls.Add("AWSCLI64", "https://s3.amazonaws.com/aws-cli/AWSCLI64.msi")
if ($ec2instanceAZ -eq "us-west-2a") # MSIs we only want installed on the "task" server
{
	$msiInstalls.Add("1-SQLSysClrTypes-x64-13.1", "https://download.microsoft.com/download/8/7/2/872BCECA-C849-4B40-8EBE-21D48CDF1456/ENU/x64/SQLSysClrTypes.msi")
	$msiInstalls.Add("2-SqlSharedManagementObjects-x64-13.1", "https://download.microsoft.com/download/8/7/2/872BCECA-C849-4B40-8EBE-21D48CDF1456/ENU/x64/SharedManagementObjects.msi")
	$msiInstalls.Add("3-SqlPowerShellTools-x64-13.1", "https://download.microsoft.com/download/8/7/2/872BCECA-C849-4B40-8EBE-21D48CDF1456/ENU/x64/PowerShellTools.msi")
	$msiInstalls.Add("4-msodbcsql-x64-13.1", "http://download.microsoft.com/download/D/5/E/D5EEF288-A277-45C8-855B-8E2CB7E25B96/13.1.811.168/amd64/msodbcsql.msi")
	$msiInstalls.Add("5-MsSqlCmdLnUtils-x64-13.1", "http://download.microsoft.com/download/C/8/8/C88C2E51-8D23-4301-9F4B-64C8E2F163C5/Command%20Line%20Utilities%20MSI%20files/amd64/MsSqlCmdLnUtils.msi")
}

# Hash table of IIS sites to create
$iisSites = @{}
$iisSites.Add("Sunapsis", $rootSunapsis)

# File/folder names for CloudWatch (logs) configuration
$cwLogJsonPath = "C:\Program Files\Amazon\SSM\Plugins\awsCloudWatch"
$cwLogJsonFile = "AWS.EC2.Windows.CloudWatch.json"

# Create an alias for the Sunapsis delete_files.bat (included in some upgrade) so that we can use the call operator with parameters
Set-Alias delete_files (Join-Path -Path $rootSunapsis -ChildPath "delete_files.bat")

### Hash table of the s3 sync scheduled tasks we'll create and their configuration

$s3syncTasks = @{}

# to backup fileshare
$s3syncTasks.Add("fileshare", (@{
	"StartTime" = "12:00am"
	"RepetitionInterval" = "PT1H" # every 1 hour
	"SourceFolder" = $rootFileShare + "\content"
	"CustomArguments" = ""
}))

# to backup SEVIS transaction logs
$s3syncTasks.Add("sevis", (@{
	"StartTime" = "10:00am"
	"RepetitionInterval" = "PT12H" # every 12 hours
	"SourceFolder" = $rootSunapsis + "\ioffice\batch\sevis"
	"CustomArguments" = "--exclude `"curl/*`" --exclude `"certs/*`" --exclude `"*.config`""
}))

# to backup customization archives
$s3syncTasks.Add("customization_archives", (@{
	"StartTime" = "6:00am"
	"RepetitionInterval" = "PT6H" # every 6 hours
	"SourceFolder" = $rootCustomizations + "\archive"
	"CustomArguments" = ""
}))

# to backup ColdFusion logs (to avoid overwrites, appending the EC2 instanceId for uniqueness)
$s3syncTasks.Add("coldfusion_$($ec2instanceId)", (@{
	"StartTime" = "12:10am"
	"RepetitionInterval" = "PT15M" # every 15 minutes
	"SourceFolder" = $rootColdFusion + "\cfusion\logs"
	"CustomArguments" = ""
	"CreateOnBothServers" = $true
}))

# to backup the logs of Sunapsis jobs
$s3syncTasks.Add("sunapsis_jobs", (@{
	"StartTime" = "12:15am"
	"RepetitionInterval" = "PT15M" # every 15 minutes
	"SourceFolder" = $rootSunapsis + "\ioffice\batch\bat"
	"CustomArguments" = ""
}))

# to backup the logs of the other s3 sync jobs (to avoid overwrites, appending the EC2 instanceId for uniqueness)
$s3syncTasks.Add("s3synclogs_$($ec2instanceId)", (@{
	"StartTime" = "12:05am"
	"RepetitionInterval" = "PT15M" # every 15 minutes
	"SourceFolder" = $rootLogFiles
	"CustomArguments" = ""
	"CreateOnBothServers" = $true
}))

### Hash table of scheduled tasks for customizations we'll create and their configuration
  # NOTE: assumes PowerShell (.ps1) scripts named with the task name in the $rootCustomizations folder

$tasks = @{}

$tasks.Add("runDatabaseBackerUpper", (@{
	"Frequency" = "Daily"
	"StartTime" = "2am"
	"StartTimeNonProd" = "6:00am"
}))

$tasks.Add("runDataFeedEmailMapper", (@{
	"Frequency" = "Daily"
	"StartTime" = "6pm"
	"StartTimeNonProd" = "3:10am"
}))

$tasks.Add("runDataFeederMathPlacement", (@{
	"Frequency" = "Daily"
	"StartTime" = "6:30pm"
	"StartTimeNonProd" = "2:55am"
}))

$tasks.Add("runDataFeederStudent", (@{
	"Frequency" = "Daily"
	"StartTime" = "8:15pm"
	"StartTimeNonProd" = "3:00am"
}))

$tasks.Add("runDataFeederEmployee", (@{
	"Frequency" = "Daily"
	"StartTime" = "8:00pm"
	"StartTimeNonProd" = "3:05am"
}))

$tasks.Add("runLawfulPresenceExtract", (@{
	"Frequency" = "Daily"
	"StartTime" = "12:15am"
}))

$tasks.Add("runMathExtract", (@{
	"Frequency" = "Daily"
	"StartTime" = "5:30pm"
}))

$tasks.Add("runPostDataFeedTasks", (@{
	"Frequency" = "Daily"
	"StartTime" = "5:30am"
	"StartTimeNonProd" = "7:30am"
}))

$tasks.Add("runReportDuplicateMinors", (@{
	"Frequency" = "Weekly"
	"StartTime" = "6:00am"
	"DaysOfWeek" = "Monday"
}))

$tasks.Add("runReportProgramExtend", (@{
	"Frequency" = "Monthly"
	"StartTime" = "06:35"
	"DaysOfMonth" = "1"
}))

$tasks.Add("runReportProgramShorten", (@{
	"Frequency" = "Monthly"
	"StartTime" = "06:30"
	"DaysOfMonth" = "1"
}))

<# samples for syntax reference

$tasks.Add("sampleDaily", (@{
	"Frequency" = "Daily"
	"StartTime" = "2am"
	"RepetitionInterval" = "PT6H"
	"RepetitionDuration" = "PT12H"
}))

$tasks.Add("sampleWeekly", (@{
	"Frequency" = "Weekly"
	"StartTime" = "6:00am"
	"DaysOfWeek" = "Monday"
}))

$tasks.Add("sampleMonthly", (@{
	"Frequency" = "Monthly" # Monthly requires different syntax
	"StartTime" = "12:10" # HH:mm (24-hour time format)
	"DaysOfMonth" = "1" # 1-31 (days of the month), the wildcard character (*) specifies all days
}))

#>

##### Misc Windows config

# Set time zone (Arizona, MST, UTC-7)
& tzutil.exe /s "US Mountain Standard Time" | Out-Null

# Change to use campus DNS (req'd for using the Isilon)
Set-DnsClientServerAddress -InterfaceIndex (Get-NetAdapter | where Status -eq "Up").ifIndex -ServerAddresses ("128.196.11.233", "128.196.11.234")

# Add entry to local hosts file to have site FQDN resolve to 127.0.0.1
# NOTE: this is req'd as Sunapsis makes calls to itself expecting to hit the same server making the requests -- with a load balancer this is not guaranteed
Add-Content -Encoding UTF8 -Path "$($env:windir)\system32\drivers\etc\hosts" -Value "127.0.0.1 $($hostHeader)"

##### Install software, add Windows features

# Download and install MSIs
# NOTE: forcing sort order to ensure installs take place in desired order (due to dependencies)
Read-S3Object -BucketName $s3bucketApp -Folder $rootBootstrap -KeyPrefix $s3keySoftware

foreach ($msi in $msiInstalls.GetEnumerator() | Sort-Object Name)
{
	if ($msi.Name -Like "*msodbcsql*")
	{
		& msiexec /i (Join-Path -Path $rootBootstrap -ChildPath "$($msi.Key).msi") /qn /log (Join-Path -Path $rootBootstrap -ChildPath "install_$($msi.Key)-log.txt") IACCEPTMSODBCSQLLICENSETERMS=YES | Out-Null
	}
	elseif ($msi.Name -Like "*MsSqlCmdLnUtils*")
	{
		& msiexec /i (Join-Path -Path $rootBootstrap -ChildPath "$($msi.Key).msi") /qn /log (Join-Path -Path $rootBootstrap -ChildPath "install_$($msi.Key)-log.txt") IACCEPTMSSQLCMDLNUTILSLICENSETERMS=YES | Out-Null
	}
	if ($msi.Name -Like "*Shibboleth*")
	{
		& msiexec /i (Join-Path -Path $rootBootstrap -ChildPath "$($msi.Key).msi") /qn /log (Join-Path -Path $rootBootstrap -ChildPath "install_$($msi.Key)-log.txt") REBOOT=ReallySupress INSTALL_ISAPI_FILTER=FALSE INSTALL_32BIT=TRUE INSTALLDIR=D:\opt\shibboleth-sp\ | Out-Null
	}
	else
	{
		& msiexec /i (Join-Path -Path $rootBootstrap -ChildPath "$($msi.Key).msi") /qn /log (Join-Path -Path $rootBootstrap -ChildPath "install_$($msi.Key)-log.txt") | Out-Null
	}
}

# Install Windows features
Install-WindowsFeature -Name $windowsFeatures -LogPath (Join-Path -Path $rootBootstrap -ChildPath "windows_features-log.txt")

# Download SFTP keys from S3
mkdir D:\SftpKeys
Read-S3Object -BucketName $s3bucketApp -Folder "D:\SftpKeys" -KeyPrefix $s3keySftp

# Install Windows Management Framework 5.1 (WMF)
Start-Process (Join-Path -Path $rootBootstrap -ChildPath "WMF5-x64.msu") -Wait -ArgumentList "/quiet /norestart"

# UnZip the WinSCP PowerShell module to a location in the PSModulePath
Add-Type -As System.IO.Compression.FileSystem
$ZipFile = Get-Item (Join-Path -Path $rootBootstrap -ChildPath "WinSCP.zip")
$Archive = [System.IO.Compression.ZipFile]::Open( $ZipFile, "Read" )
[System.IO.Compression.ZipFileExtensions]::ExtractToDirectory( $Archive, "C:\Program Files\WindowsPowerShell\Modules" )
$Archive.Dispose()

##### ColdFusion

# Install VC++ 2012 redistributables, both x86 & x64 (ColdFusion 2016 requirement)
& (Join-Path -Path $rootBootstrap -ChildPath "vc2012redist_x64.exe") /q /norestart | Out-Null
& (Join-Path -Path $rootBootstrap -ChildPath "vc2012redist_x86.exe") /q /norestart | Out-Null

# The 2018-01 Windows Updates caused the ColdFusion 2016 install to error with:
# Activation context generation failed for "C:\ColdFusion2016\cfusion\lib\TypeViewer.dll". Dependent Assembly Microsoft.VC90.MFC,processorArchitecture="amd64",publicKeyToken="1fc8b3b9a1e18e3b",type="win32",version="9.0.21022.8" could not be found. Please use sxstrace.exe for detailed diagnosis.
# As Microsoft.VC90 refers to Visual C++ 2008 also installing the VC++ redistributables which resolves this error
& (Join-Path -Path $rootBootstrap -ChildPath "vc2008redist_x64.exe") /q /norestart | Out-Null

# Install ColdFusion 2016
& (Join-Path -Path $rootBootstrap -ChildPath "ColdFusion_2016_WWEJ_win64.exe") -f (Join-Path -Path $rootBootstrap -ChildPath "cf_silent.properties") | Out-Null

# Stop ColdFusion Application Server service
Stop-Service -Name "ColdFusion 2016 Application Server"

# Kill java.exe (if running)
Stop-Process -Name "java" -Force -ErrorAction SilentlyContinue

# Remove the JRE that comes with ColdFusion
Remove-Item -Path (Join-Path -Path $rootColdFusion -ChildPath "jre") -Recurse

# Sometimes files in the ColdFusion JRE folder still have handles on them so rename the folder if that's the case so we can install a new one
Rename-Item (Join-Path -Path $rootColdFusion -ChildPath "jre") (Join-Path -Path $rootColdFusion -ChildPath "jreold") -ErrorAction SilentlyContinue 

# Install JRE 8 (hopefully a newer version than the one shipped with CF2016!)
& (Join-Path -Path $rootBootstrap -ChildPath "jre-8-windows-x64.exe") /s INSTALLDIR=$(Join-Path -Path $rootColdFusion -ChildPath "jre") | Out-Null

# Download ColdFusion configs from S3 and overwrite existing configs
Read-S3Object -BucketName $s3bucketApp -Folder (Join-Path -Path $rootColdFusion -ChildPath "cfusion") -KeyPrefix $s3keyCFConfig

# Update ColdFusion with latest update (will result in ColdFusion 2016 Application Server service starting)
& (Join-Path -Path $rootColdFusion -ChildPath "jre\bin\java.exe") -jar (Join-Path -Path $rootBootstrap -ChildPath "cf2016hotfix.jar") -i silent -f (Join-Path -Path $rootBootstrap -ChildPath "cf_update.properties") | Out-Null

# After the update Kill the coldfusion.exe process (we restart the CF Application Server service later, after we deploy the app)
# After the 2018-01 Windows Updates coldfusion.exe remains running after the update, using port 8500 and blocking the CF Application service from starting
Stop-Process -Name "coldfusion" -Force -ErrorAction SilentlyContinue

##### IIS

# Make folders for web content
mkdir D:\IISLogs
foreach ($site in $iisSites.Keys)
{
	mkdir $iisSites.Item($site)
}

# Download SSL cert, retrive cert pwd from parameter store, import to cert store and get cert hash
Read-S3Object -BucketName $s3bucketApp -Folder $rootBootstrap -KeyPrefix $s3keyCerts
$certPwdClear = (Get-SSMParameterValue -Name "sunapsisCertPassword-$($environmentName)" -WithDecryption $True).Parameters.Value
$certPwdSecure =  ConvertTo-SecureString -String $certPwdClear -Force -AsPlainText
Import-PfxCertificate -FilePath (Join-Path -Path $rootBootstrap -ChildPath "ssl_cert.pfx") -Password $certPwdSecure -CertStoreLocation Cert:\LocalMachine\My
$certHash = Get-ChildItem -Path Cert:\LocalMachine\My | Where-Object {$_.subject -like "*$($hostHeaderRoot)*"} | Select-Object -ExpandProperty Thumbprint

# Create the web site, app pool & SSL bindings
# NOTE: forcing sort to get a consistent IIS site Id for use with Shibboleth SP
foreach ($site in $iisSites.GetEnumerator() | Sort-Object Name)
{
	New-WebAppPool -Name $site.Key
	New-Website -Name $site.Key -ApplicationPool $site.Key -Port 80 -HostHeader $hostHeader -PhysicalPath $site.Value
	New-WebBinding -Name $site.Key -Protocol "https" -Port 443 -HostHeader $hostHeader -SslFlags 0
	New-Item -Path "IIS:\SslBindings\*!443!$($hostHeader)" -Thumbprint $certHash -SSLFlags 0
}

# Set default IIS logging folder 
Set-WebConfigurationProperty -Filter "/system.applicationHost/sites/siteDefaults" -Name "logfile.directory" -Value $iisLogFolder      

# Increse maxAllowedContentLength to 100MB (for file uploads)
Set-WebConfigurationProperty -Filter "/system.webserver/security/requestfiltering/requestLimits" -Name "maxAllowedContentLength" -Value 104857600

# ELB health checks do not use a host header and end up at the IIS default web site
# In order to avoid errors we need to create an HTTPS binding and create a default document (403.14 error without)
New-WebBinding -Name "Default Web Site" -Protocol "https" -Port 443 -SslFlags 0
Set-Content C:\inetpub\wwwroot\default.htm -Value "<html><body>up</body></html>"

# Run the ColdFusion web configuration tool
& C:\ColdFusion2016\cfusion\runtime\bin\wsconfig.exe -ws IIS -site 0 | Out-Null

# Download Shibboleth configuration from S3
Read-S3Object -BucketName $s3bucketApp -Folder $rootShib -KeyPrefix $s3keyShib	

# Override lock & set config for an ISAPI filter on Sunapsis site for Shibboleth
Set-WebConfiguration -Filter "//isapiFilters" -PSPath "IIS:\" -Metadata overrideMode -Value Allow
Add-WebConfiguration -Filter "//isapiFilters" -PSPath "IIS:\sites\Sunapsis" -Value @{
    name = 'Shibboleth';
    path = 'D:\opt\shibboleth-sp\lib64\shibboleth\isapi_shib.dll'
	}

# Override lock & set config for an ISAPI & CGI Restrictions for Shibboleth
Set-WebConfiguration -Filter "//isapiCgiRestriction" -PSPath "IIS:\" -Metadata "overrideMode" -Value "Allow"
Add-WebConfiguration -Filter "//isapiCgiRestriction" -PSPath "IIS:\" -Value @{
		description = 'Shibboleth';
		path = 'D:\opt\shibboleth-sp\lib64\shibboleth\isapi_shib.dll';
		allowed = 'True'
	}

# Override lock & add a Handler Mapping for Shibboleth
Set-WebConfiguration -Filter "//handlers" -PSPath "IIS:\" -Metadata "overrideMode" -Value "Allow"
New-WebHandler -PSPath "IIS:\sites\Sunapsis" -Name "Shibboleth" -Path "*.sso" -ScriptProcessor "D:\opt\shibboleth-sp\lib64\shibboleth\isapi_shib.dll" -Verb "*" -Modules "IsapiModule" -ResourceType "Unspecified"

# Restart Shibboleth service (so that config changes take effect)                
Restart-Service "shibd_default"        

##### Application

# Download & extract application zip files, copy extracted files to Sunapsis root
# NOTE 1: vendor provides a full application with a major release and all minor releases are deltas
# NOTE 2: using naming convention of sunapsis_0.0.0.zip and relying in alphanumeric sort of filenames for order
mkdir $rootAppZips
Read-S3Object -BucketName $s3bucketApp -Folder $rootAppZips -KeyPrefix $s3keyAppZips
foreach ($zip in (Get-ChildItem -Path "$($rootAppZips)\*" -Include "*.zip" | Sort-Object -Property Name).Name)
{
	$ZipFile = Get-Item (Join-Path -Path $rootAppZips -ChildPath $zip)
	$extractPath = Join-Path -Path $rootAppZips -ChildPath ([io.path]::GetFileNameWithoutExtension("$($ZipFile)"))
	$Archive = [System.IO.Compression.ZipFile]::Open( $ZipFile, "Read" )
	[System.IO.Compression.ZipFileExtensions]::ExtractToDirectory( $Archive, $extractPath )
	$Archive.Dispose()
	Copy-Item -Path (Join-Path -Path $extractPath -ChildPath "*") -Destination $rootSunapsis -Recurse -Force
	# Check if a delete_files.bat exists in the Sunapsis root and, if so, run it (via the alias), then delete the file
	if (Test-Path -Path (Join-Path -Path $rootSunapsis -ChildPath "delete_files.bat"))
	{
		Set-Location -Path $rootSunapsis
		& delete_files $($rootSunapsis + "\") $($rootSunapsis + "\")
		Remove-Item (Join-Path -Path $rootSunapsis -ChildPath "delete_files.bat")
	}

}
Remove-Item -Path $rootAppZips -Recurse

# Perform an IISReset (else the Shibboleth ISAPI filter won't load)
& iisreset                

# Add entry to local hosts file to have site FQDN resolve to 127.0.0.1
# NOTE this is req'd as Sunapsis makes calls to itself expecting to hit the same server making the requests -- with a load balancer this is not guaranteed
Add-Content -Encoding UTF8 -Path "$($env:windir)\system32\drivers\etc\hosts" "127.0.0.1 $($hostHeader)"

# Download Sunapsis customizations from S3 to target destination
Read-S3Object -BucketName $s3bucketApp -Folder $rootSunapsis -KeyPrefix $s3keyColdFusionCustomizations

# Download encryption-related files from S3 to their target destinations
Read-S3Object -BucketName $s3bucketApp -Folder (Join-Path -Path $rootSunapsis -ChildPath "ioffice\batch\contego") -KeyPrefix ($s3keyAppEncryption + "Key_Part1\")
Read-S3Object -BucketName $s3bucketApp -Folder (Join-Path -Path $rootSunapsis -ChildPath "ioffice\contego") -KeyPrefix ($s3keyAppEncryption + "Password\")

# Check if file share has its part of the encryption key, if not, download it from S3
mkdir (Join-Path -Path $rootFileShare -ChildPath "content\000\contego") -Force
if (-Not (Test-Path -Path (Join-Path -Path $rootFileShare -ChildPath "content\000\contego\contego2.pdf")))
{
    # Check if folder exists and create it if it does not
    Read-S3Object -BucketName $s3bucketApp -Folder (Join-Path -Path $rootFileShare -ChildPath "content\000\contego") -KeyPrefix ($s3keyAppEncryption + "Key_Part2\")
}

# If UpdateFiles.cfm exists, call it to perform post-upgrade tasks, then delete it
if (Test-Path -Path (Join-Path -Path $rootSunapsis -ChildPath "UpdateFiles.cfm"))
{
	Invoke-RestMethod -Uri "http://localhost/UpdateFiles.cfm"
	Remove-Item (Join-Path -Path $rootSunapsis -ChildPath "UpdateFiles.cfm")
}

##### Instance-specific tasks

### The ASG is configured in two AZs and has a min/max value of 2 meaning we'll always have one web server in each AZ
### We'll leverage this and configure the server in AZ us-west-2a to be our "task" server (where scheduled tasks are initiated)
if ($ec2instanceAZ -eq "us-west-2a")
{
	# Download SEVIS batch certificate from S3 into target destination
	mkdir (Join-Path -Path $rootSunapsis -ChildPath "ioffice\batch\sevis\certs")
	Read-S3Object -BucketName $s3bucketApp -Folder (Join-Path -Path $rootSunapsis -ChildPath "ioffice\batch\sevis\certs") -KeyPrefix $s3keySevisCerts
	
	# Copy SQLPS PowerShell module to a location in the PSModulePath
	Copy-Item "C:\Program Files\Microsoft SQL Server\130\Tools\PowerShell\Modules\SQLPS" "C:\Program Files\WindowsPowerShell\Modules" -Recurse
	
	# Download zipped customizations from S3
	mkdir $rootCustomizations
	Read-S3Object -BucketName $s3bucketApp -Folder $rootCustomizations -KeyPrefix $s3keyCustomizations
	
	# Unzip zipped customizations & delete zip when done
	Add-Type -As System.IO.Compression.FileSystem
	$ZipFile = Get-Item (Join-Path -Path $rootCustomizations -ChildPath "sunapsisCustomizations.zip")
	$Archive = [System.IO.Compression.ZipFile]::Open( $ZipFile, "Read" )
	[System.IO.Compression.ZipFileExtensions]::ExtractToDirectory( $Archive, $rootCustomizations )
	$Archive.Dispose()
	Remove-Item -Path $ZipFile
}

##### Scheduled Tasks (all instances)

### IIS logs cleanup, uses external PowerShell script
mkdir $rootScripts
Read-S3Object -BucketName $s3bucketApp -Folder $rootScripts -KeyPrefix $s3keyScripts
$taskAction = New-ScheduledTaskAction -Execute "powershell.exe" -Argument "-ExecutionPolicy bypass -NonInteractive -NoLogo -NoProfile $(Join-Path -Path $rootScripts -ChildPath "Remove-IISLogsOlderThan3Days.ps1")"
$taskUser = New-ScheduledTaskPrincipal -UserID "NT AUTHORITY\SYSTEM" -LogonType ServiceAccount # -RunLevel Highest
$taskTrigger  = New-ScheduledTaskTrigger -Daily -At "10:30pm"
$task = New-ScheduledTask -Action $taskAction -Principal $taskUser -Trigger $taskTrigger
Register-ScheduledTask "Remove-IISLogsOlderThan3Days" -InputObject $task -Force

##### Scheduled Tasks for s3 sync

### s3 sync jobs

mkdir $rootLogFiles
mkdir $rootBatchFiles

$s3syncTaskNamePrefix = "s3sync_"

# loop through the hash table and create the scheduled tasks
foreach ($s3syncTask in $s3syncTasks.GetEnumerator())
{
	# create task if AZ is us-west2a or if CreateBothServers is set to $true
	if ($ec2instanceAZ -eq "us-west-2a" -or $s3syncTasks.($s3syncTask.Key).CreateOnBothServers)
	{
		$s3syncTaskName = $s3syncTaskNamePrefix + $s3syncTask.Key
		# Create batch file (necessary to redirect output to a file as it can't be done directly through a scheduled task and we want logs of the s3 sync runs)
		if ($s3syncTask.Key -Like "coldfusion*")
		{
			# For ColdFusion server logs we add an extra prefix to the key for organizational purposes
			Set-Content -Path (Join-Path -Path $rootBatchFiles -ChildPath "$($s3syncTaskName).bat") -Value "echo Job Started: %date% %time% >> $(Join-Path -Path $rootLogFiles -ChildPath "$($s3syncTaskName)_log.txt")`r`n`"C:\Program Files\Amazon\AWSCLI\aws.exe`" s3 sync $($s3syncTasks.($s3syncTask.Key).SourceFolder) s3://$($s3bucketArchive)/coldfusion/$($s3syncTask.Key) $($s3syncTasks.($s3syncTask.Key).CustomArguments) >> $(Join-Path -Path $rootLogFiles -ChildPath "$($s3syncTaskName)_log.txt")`r`necho Job Ended: %date% %time% >> $(Join-Path -Path $rootLogFiles -ChildPath "$($s3syncTaskName)_log.txt")`r`necho: >> $(Join-Path -Path $rootLogFiles -ChildPath "$($s3syncTaskName)_log.txt")"
		}
		elseif ($s3syncTask.Key -Like "s3synclogs*")
		{
			# For s3 sync job logs we add an extra prefix to the key for organizational purposes
			Set-Content -Path (Join-Path -Path $rootBatchFiles -ChildPath "$($s3syncTaskName).bat") -Value "echo Job Started: %date% %time% >> $(Join-Path -Path $rootLogFiles -ChildPath "$($s3syncTaskName)_log.txt")`r`n`"C:\Program Files\Amazon\AWSCLI\aws.exe`" s3 sync $($s3syncTasks.($s3syncTask.Key).SourceFolder) s3://$($s3bucketArchive)/s3synclogs/$($s3syncTask.Key) $($s3syncTasks.($s3syncTask.Key).CustomArguments) >> $(Join-Path -Path $rootLogFiles -ChildPath "$($s3syncTaskName)_log.txt")`r`necho Job Ended: %date% %time% >> $(Join-Path -Path $rootLogFiles -ChildPath "$($s3syncTaskName)_log.txt")`r`necho: >> $(Join-Path -Path $rootLogFiles -ChildPath "$($s3syncTaskName)_log.txt")"
		}
		else
		{
			Set-Content -Path (Join-Path -Path $rootBatchFiles -ChildPath "$($s3syncTaskName).bat") -Value "echo Job Started: %date% %time% >> $(Join-Path -Path $rootLogFiles -ChildPath "$($s3syncTaskName)_log.txt")`r`n`"C:\Program Files\Amazon\AWSCLI\aws.exe`" s3 sync $($s3syncTasks.($s3syncTask.Key).SourceFolder) s3://$($s3bucketArchive)/$($s3syncTask.Key) $($s3syncTasks.($s3syncTask.Key).CustomArguments) >> $(Join-Path -Path $rootLogFiles -ChildPath "$($s3syncTaskName)_log.txt")`r`necho Job Ended: %date% %time% >> $(Join-Path -Path $rootLogFiles -ChildPath "$($s3syncTaskName)_log.txt")`r`necho: >> $(Join-Path -Path $rootLogFiles -ChildPath "$($s3syncTaskName)_log.txt")"
		}
		# Create the task
		$s3syncTaskAction = New-ScheduledTaskAction -Execute (Join-Path -Path $rootBatchFiles -ChildPath "$($s3syncTaskName).bat")
		$s3syncTaskUser = New-ScheduledTaskPrincipal -UserID "NT AUTHORITY\SYSTEM" -LogonType ServiceAccount
		$s3syncTaskTrigger  = New-ScheduledTaskTrigger -Daily -At $s3syncTasks.($s3syncTask.Key).StartTime
		$s3syncTaskSettings = New-ScheduledTaskSettingsSet -MultipleInstances IgnoreNew
		$s3syncTaskObject = New-ScheduledTask -Action $s3syncTaskAction -Principal $s3syncTaskUser -Trigger $s3syncTaskTrigger -Settings $s3syncTaskSettings
		Register-ScheduledTask -TaskName $s3syncTaskName -InputObject $s3syncTaskObject -Force
		# Update the task for recurrence (can't do this initially on the trigger as -RepetitionInterval isn't available when using -At)
		$s3syncTaskObject = Get-ScheduledTask -TaskName $s3syncTaskName
		$s3syncTaskObject.Triggers.Repetition.Interval = $s3syncTasks.($s3syncTask.Key).RepetitionInterval
		$s3syncTaskObject | Set-ScheduledTask -User "NT AUTHORITY\SYSTEM"
	}
}

##### Other Scheduled Tasks (only the us-west-2a instance)

if ($ec2instanceAZ -eq "us-west-2a")
{
	# Create the scheduled tasks
	foreach ($task in $tasks.GetEnumerator())
	{
		# We use different schedules in PRD/non-PRD so handle that
		if ($environmentName -Eq "prd")
		{
			$taskStartTime = $tasks.($task.Key).StartTime
		}
		else
		{
			# Check if there is a non-PRD start time value
			if ($tasks.($task.Key).StartTimeNonProd)
			{
				$taskStartTime = $tasks.($task.Key).StartTimeNonProd
			}
			else
			{
				$taskStartTime = $tasks.($task.Key).StartTime
			}
		}
		
		# PowerShell doesn't allow for monthly frequency so need to handle that differently using the schtasks.exe CLI utility
		if ($tasks.($task.Key).Frequency -Eq "Monthly")
		{
			& C:\Windows\system32\schtasks.exe /Create /SC MONTHLY /D "$($tasks.($task.Key).DaysOfMonth)" /TN "$($task.Key)" /ST "$($taskStartTime)" /TR "powershell.exe -ExecutionPolicy bypass -NonInteractive -NoLogo -NoProfile $(Join-Path -Path $rootCustomizationsScripts -ChildPath "$($task.Key).ps1")" | Out-Null
		}
		else
		{
			if ($tasks.($task.Key).Frequency -Eq "Daily")
			{
				$taskTrigger  = New-ScheduledTaskTrigger -Daily -At $($taskStartTime)
			}
			if ($tasks.($task.Key).Frequency -Eq "Weekly")
			{
				$taskTrigger  = New-ScheduledTaskTrigger -Weekly -DaysOfWeek $($tasks.($task.Key).DaysOfWeek) -At $($taskStartTime)
			}
			$taskUser = New-ScheduledTaskPrincipal -UserID "NT AUTHORITY\SYSTEM" -LogonType ServiceAccount
			$taskAction = New-ScheduledTaskAction -Execute "powershell.exe" -Argument "-ExecutionPolicy bypass -NonInteractive -NoLogo -NoProfile $(Join-Path -Path $rootCustomizationsScripts -ChildPath "$($task.Key).ps1")"
			$taskObject = New-ScheduledTask -Action $taskAction -Principal $taskUser -Trigger $taskTrigger
			Register-ScheduledTask "$($task.Key)" -InputObject $taskObject -Force

			# Configure Repetition Interval (if value exists)
			if ($tasks.($task.Key).RepetitionInterval)
			{
				$taskObject = Get-ScheduledTask -TaskName "$($task.Key)"
				$taskObject.Triggers.Repetition.Interval = "$($tasks.($task.Key).RepetitionInterval)"
				$taskObject | Set-ScheduledTask -User "NT AUTHORITY\SYSTEM"
			}

			# Configure Repetition Duration (if value exists)
			if ($tasks.($task.Key).RepetitionDuration)
			{
				$taskObject = Get-ScheduledTask -TaskName "$($task.Key)"
				$taskObject.Triggers.Repetition.Duration = "$($tasks.($task.Key).RepetitionDuration)"
				$taskObject | Set-ScheduledTask -User "NT AUTHORITY\SYSTEM"
			}

			# Disable Lawful Presence extract task in non-PRD enviromments
			if ($environmentName -Ne "PRD" -And $task.Key -Eq "runLawfulPresenceExtract")
			{
				Disable-ScheduledTask -TaskName "$($task.Key)"
			}
		}
	}
}

##### ColdFusion finalization #####

# Start the ColdFusion Application Server service
Start-Service -Name "ColdFusion 2016 Application Server"

##### AWS-specific tasks

# CloudFormation does not currently (as of April 2018) allow setting T2 Unlimited in a launch configuration so we need to do it here
$SuperUnlimiter5000 = New-Object Amazon.EC2.Model.InstanceCreditSpecificationRequest
$SuperUnlimiter5000.CpuCredits = "unlimited"
$SuperUnlimiter5000.InstanceId = $ec2instanceId
Edit-EC2InstanceCreditSpecification -InstanceCreditSpecification $SuperUnlimiter5000

# Download CloudWatch Logs JSON configuration file, modify to update target Log Group, stop SSM Agent to take effect (service auto-restarts)
Read-S3Object -BucketName $s3bucketApp -Folder $cwLogJsonPath -KeyPrefix $s3keySSM
$cwLogJson = Get-Content (Join-Path -Path $cwLogJsonPath -ChildPath $cwLogJsonFile) -Raw | ConvertFrom-Json
$cwLogJson.EngineConfiguration.Components | ForEach-Object { If ($_.Id -Eq "CloudWatchLogs") { $_.Parameters.LogGroup = "sunapsis-$($environmentName)-webserver" }}
$cwLogJson | ConvertTo-Json -Depth 10 | Set-Content (Join-Path -Path $cwLogJsonPath -ChildPath $cwLogJsonFile)
Stop-Service AmazonSSMAgent

# Update Route53 record set to point server alias to current EC2 instance private IP
$route53recordSet = New-Object Amazon.Route53.Model.ResourceRecordSet
$route53recordSet.Name = "sunapsis-$($environmentName)-web-$($ec2instanceAZ.TrimStart("us-west12")).$($hostedZoneName)"
$route53recordSet.Type = "A"
$route53recordSet.TTL = "0"
$route53recordSet.ResourceRecords = New-Object Amazon.Route53.Model.ResourceRecord ($ec2instanceIP)
$route53action = [Amazon.Route53.ChangeAction]::UPSERT
$route53change = New-Object Amazon.Route53.Model.Change ($route53action, $route53recordSet)
Edit-R53ResourceRecordSet -HostedZoneId $hostedZoneId -ChangeBatch_Change $route53change

# Get the EC2 instanceID
$ec2instanceId = Invoke-RestMethod -Uri "http://169.254.169.254/latest/meta-data/instance-id"

# Assign a "Name" tag to the instance to be friendly to the console UI
$awsTags = @()
$awsNameTag = New-Object Amazon.EC2.Model.Tag
$awsNameTag.Key = "Name"
$awsNameTag.Value = "sunapsis-$($environmentName)-web-$($ec2instanceAZ.TrimStart("us-west12"))"
$awsTags += $awsNameTag
New-EC2Tag -ResourceId $ec2instanceId -Tags $awsTags

# Reboot (required by WMF 5.1 installation)
Restart-Computer -Force