#!/bin/bash

# source environment vars
. ./stack-env.sh

# upload SSL certificate for ELB
aws iam upload-server-certificate --server-certificate-name ${SERVER_CERT_NAME} --certificate-body file://${SSL_CERT_PATH} --private-key file://${SSL_KEY_PATH} --certificate-chain file://${SSL_CERT_CHAIN} >/tmp/$$.out 2>/tmp/$$.err
if [ $? -ne 0 ]; then
  grep -q "The Server Certificate with name ${SERVER_CERT_NAME} already exists" /tmp/$$.err
  if [ $? -ne 0 ]; then
    echo "iam upload-server-certificate failed: `cat /tmp/$$.err`"
    exit 1
  fi
fi

# get ARN of SSL cert
cert_arn=`aws iam get-server-certificate --server-certificate-name ${SERVER_CERT_NAME} | grep '"Arn": ' | awk -F': ' '{print $2}' | tr -d '," '`

# append SSL ARN param to parameters JSON file
cat uits-incident-alerts-params.json | sed -e "s,SSL-CERT-ARN-REPLACE-ME,${cert_arn},g" >uits-incident-alerts-params.$$.json

# create CloudFormation stack
aws cloudformation create-stack --stack-name uits-incident-alerts-stack \
 --template-body file://`pwd`/templates/uits-incident-alerts-stack.json \
 --parameters file://`pwd`/uits-incident-alerts-params.$$.json \
 --capabilities CAPABILITY_IAM >/tmp/$$.out 2>/tmp/$$.err

 if [ $? -ne 0 ]; then
   echo "cloudformation create-stack filed: `cat /tmp/$$.err`"
   exit 1
 fi
