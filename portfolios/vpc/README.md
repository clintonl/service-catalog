# VPC with 2 Availability Zones and VPN connections

This template sets up an AWS Virtual Private Cloud (VPC) with subnets in two 
availability zones.  There are two public subnets, and two private subnets.

    +--------+--------+
    | Public | Public |
    |  AZ A  |  AZ B  |
    +========+========+
    | Private| Private|
    |  AZ A  |  AZ B  |
    +--------+--------+

Before deploying this VPC, you will need to contact Network Operations and obtain
the following information:

* VPC Cider Range in our 10.x IP Space
* ASR IP Address and Port Number
    If this is a Non-Production VPC, only a single VPN connection is needed.  For 
    a Production VPC, two are needed, so you should get two sets of ASR numbers.

## Splitting up the subnets

Partition your allocated VPC CIDR block so that you have enough space in each 
subnet and each availability zone.  For example if we asked for a small /24 range:

Purpose           | CIDR             | # of IP Addresses
-------------------------------------------- 
VPC               | 10.0.200.0/24    | 256
Public Subnet A   | 10.0.200.0/27    | 32
Public Subnet B   | 10.0.200.32/27   | 32
Private Subnet A  | 10.0.200.64/26   | 64
Private Subnet B  | 10.0.200.128/26  | 64
Un-Allocated      | 10.0.200.192/26  | 64

Here we start with a /24 block of 256 IPs.  Since public subnets usually have fewer
resources in them (mostly just load balancers, NAT, bastion hosts) we give that a 
little less space, a /27 of 32 IPs.  For the private subnets we have two blocks of
/26 for 64 IPs each.  This leaves us with a block of 64 un-allocated which could be
used in the future. Unfortunately due to CIDR math, you can't split that remaining
64 in half and give it to the existing private subnets, but you could create two new
private subnets if you wanted.

## Parameters

VPC Settings
- VPCName
- VPCcidr
- VPCType

Public Subnets
- SubnetPublicAcidr
- SubnetPublicBcidr

Private Subnets
- SubnetPrivateAcidr
- SubnetPrivateBcidr

VPN Settings
- VpnASR1IpAddress
- VpnASR1BgpAsn
- VpnASR2IpAddress
- VpnASR2BgpAsn

Tagging and Cost Management
- ServiceTag
- EnvironmentTag
- ContactNetidTag
- AccountNumberTag
- TicketNumberTag


## Outputs

Subnet IDs
* public-subnet-a
* public-subnet-b
* private-subnet-a
* private-subnet-b

## Credits

Developed and maintained by University of Arizona Enterprise Cloud Services. 

For questions, contact:

uits-ecs@list.arizona.edu
